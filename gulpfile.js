// TODO: Add minification
var gulp = require('gulp'),
sass = require('gulp-sass'),
webpack = require('webpack-stream'),
livereload = require('gulp-livereload');

gulp.task('scripts', function() {
  return gulp.src('src/js/main.js')
    .pipe(webpack( require('./webpack.config.js') ))
    .pipe(gulp.dest('app/scripts'))
    .pipe(livereload());
});

gulp.task('styles', function () {
    gulp.src('src/css/*.scss')
      .pipe(sass())
      .on('error', swallowError)
      .pipe(gulp.dest('app/styles'))
      .pipe(livereload());
});

gulp.task('default',['scripts','styles'], function(){});

gulp.task('watch', function() {
  livereload.listen();
  gulp.watch('src/css/**/*.scss', ['styles']);
  gulp.watch('src/js/**/*.js', ['scripts']);
  // Added this to manage the templates
  gulp.watch('src/js/**/*.monk', ['scripts']);
});

// Custom error function to stop emit(end);
var swallowError = function(error) {
    console.log(error.toString());
}