'use strict';

var path = require('path');


var devMode = (process.argv || []).indexOf('--dev') !== -1;

if (devMode) {
  // load the app dependencies
  var PATH_APP_NODE_MODULES = path.join(__dirname, '..', '..', 'app', 'node_modules');
  require('module').globalPaths.push(PATH_APP_NODE_MODULES);
}

const electron = require('electron');
const {Menu, MenuItem} = electron;
const menubar = require('menubar');
// Module to control application life.
const app = electron.app;
let mb = menubar({
  'preload-window': true
});
mb.on('ready', function ready () {
  // console.log('app is ready');
  // your app code here
});
mb.on('right-click', function(){
	const menu = Menu.buildFromTemplate(template);
	Menu.setApplicationMenu(menu);
});
