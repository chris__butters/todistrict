/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _monkberry = __webpack_require__(1);
	
	var _monkberry2 = _interopRequireDefault(_monkberry);
	
	__webpack_require__(2);
	
	var _todo = __webpack_require__(5);
	
	var _todo2 = _interopRequireDefault(_todo);
	
	var _dexie = __webpack_require__(6);
	
	var _dexie2 = _interopRequireDefault(_dexie);
	
	var _animejs = __webpack_require__(9);
	
	var _animejs2 = _interopRequireDefault(_animejs);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	// TODO: Quit
	// TODO: random messages for task
	
	var db = new _dexie2.default('data');
	db.version(1).stores({
	  todo: '++id, text, complete'
	});
	db.open();
	
	var state = {
	  todos: []
	};
	
	var view = _monkberry2.default.render(_todo2.default, document.body);
	view.update(state);
	
	db.todo.toArray().then(function (response) {
	  state.todos = response;
	  view.update(state);
	});
	
	view.on('submit', 'form', function (event) {
	  event.preventDefault();
	  var input = view.querySelector('input[type="text"]');
	  if (input.value == '') {
	    alert('Cannot add a blank item');
	    return;
	  }
	  db.todo.put({ text: input.value, complete: false }).then(function (res) {
	    return db.todo.toArray();
	  }).then(function (db) {
	    state.todos = db;
	    view.update(state);
	    input.value = '';
	  });
	});
	
	view.on('click', 'input[type="checkbox"]', function (event) {
	  var i = parseInt(event.target.dataset.index);
	  db.todo.where('id').equals(i).modify(function (item) {
	    item.complete = !item.complete;
	  }).then(function (response) {
	    db.todo.toArray().then(function (response) {
	      return response;
	    }).then(function (response) {
	      state.todos = response;
	      view.update(state);
	    });
	  });
	});
	
	view.on('click', '.delete', function (event) {
	  event.preventDefault();
	  var i = parseInt(event.target.parentNode.dataset.index);
	
	  db.todo.where('id').equals(i).delete().then(function (response) {
	    db.todo.toArray().then(function (response) {
	      return response;
	    }).then(function (response) {
	      state.todos = response;
	      view.update(state);
	    });
	  });
	});
	
	view.on('click', '.delete *', function (event) {
	  event.stopPropogation();
	});
	
	view.on('mouseover', '.label', function (event) {
	  ;
	  var el = this;
	  var true_width = function () {
	    // jQuery code to rewrite as vanilla
	    var tempobj = el.cloneNode(true);
	    tempobj.style.left = '-1000px';
	    tempobj.style.position = 'absolute';
	    document.body.appendChild(tempobj);
	    var result = tempobj.offsetWidth;
	    tempobj.parentNode.removeChild(tempobj);
	
	    return result;
	  }();
	
	  if (true_width > el.offsetWidth) {
	    var shift_distance = true_width - el.offsetWidth; // how far to move
	    (0, _animejs2.default)({
	      targets: el.querySelector('*'),
	      left: shift_distance * -1 + 'px',
	      duration: 1500,
	      loop: false,
	      easing: 'linear'
	    });
	  }
	});
	view.on('mouseout', '.label', function (event) {
	  var el = this;
	  var anim = (0, _animejs2.default)({
	    targets: el.querySelector('*'),
	    left: '0px',
	    duration: 1500,
	    loop: false,
	    easing: 'linear'
	  });
	});
	
	document.querySelector('.quit button').addEventListener('click', function () {
	  window.close();
	});

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	/**                                      _    _
	 *                     /\/\   ___  _ __ | | _| |__   ___ _ __ _ __ _   _
	 *                    /    \ / _ \| '_ \| |/ / '_ \ / _ \ '__| '__| | | |
	 *                   / /\/\ \ (_) | | | |   <| |_) |  __/ |  | |  | |_| |
	 *                   \/    \/\___/|_| |_|_|\_\_.__/ \___|_|  |_|   \__, |
	 *                                                                 |___/
	 *
	 *        +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
	 *  Enter ->  |       |                                   |           |           |       |
	 *        +   +   +   +---+   +---+---+   +---+---+   +   +   +---+   +   +---+   +   +   +
	 *        |       |           |                   |   |       |       |   |   |       |   |
	 *        +---+---+---+---+---+   +---+---+---+---+   +---+---+   +---+   +   +---+---+   +
	 *        |       |               |       |           |       |       |   |           |   |
	 *        +   +   +   +---+---+---+   +   +   +---+---+   +   +---+   +   +---+---+   +   +
	 *        |   |       |           |   |   |       |       |               |   |           |
	 *        +   +---+---+   +---+   +   +   +---+   +   +---+---+---+---+---+   +   +   +---+
	 *        |   |       |       |       |       |   |   |       |       |   |       |   |   |
	 *        +   +---+   +---+   +---+---+---+   +   +   +   +   +   +   +   +---+---+   +   +
	 *        |           |       |       |   |       |       |   |   |   |           |   |   |
	 *        +---+---+---+   +---+   +   +   +   +---+---+---+   +---+   +---+---+   +   +   +
	 *        |   |       |           |       |   |       |       |       |               |   |
	 *        +   +   +   +---+---+---+   +---+   +   +   +   +---+   +---+---+   +---+---+   +
	 *        |   |   |           |           |   |   |   |       |   |       |   |           |
	 *        +   +   +---+---+   +---+---+---+   +---+   +---+   +   +   +   +   +   +---+   +
	 *        |       |                           |       |   |       |   |       |   |       |
	 *        +---+---+   +   +   +---+---+---+---+   +---+   +---+   +   +---+---+   +   +---+
	 *        |           |   |                               |       |               |       -> Exit
	 *        +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
	 */
	(function (document) {
	  /**
	   * Monkberry
	   * @class
	   */
	  function Monkberry() {
	    this.parent = null;
	    this.nested = [];
	    this.nodes = [];
	    this.filters = null;
	    this.directives = null;
	    this.context = null;
	    this.unbind = null;
	    this.onRender = null;
	    this.onUpdate = null;
	    this.onRemove = null;
	  }
	
	  /**
	   * Render template and attach it to node.
	   * @param {Monkberry} template
	   * @param {Element} node
	   * @param {Object=} options
	   * @return {Monkberry}
	   */
	  Monkberry.render = function (template, node, options) {
	    var view;
	
	    if (options && options.noCache) {
	      view = new template();
	    } else {
	      view = template.pool.pop() || new template();
	    }
	
	    if (node.nodeType == 8) {
	      view.insertBefore(node);
	    } else {
	      view.appendTo(node);
	    }
	
	    if (options) {
	      if (options.parent) {
	        view.parent = options.parent;
	      }
	
	      if (options.context) {
	        view.context = options.context;
	      }
	
	      if (options.filters) {
	        view.filters = options.filters;
	      }
	
	      if (options.directives) {
	        view.directives = options.directives;
	      }
	    }
	
	    if (view.onRender) {
	      view.onRender();
	    }
	
	    return view;
	  };
	
	  /**
	   * Prerepder template for future usage.
	   * @param {Monkberry} template - Template name.
	   * @param {Number} times - Times of prerender.
	   */
	  Monkberry.prerender = function (template, times) {
	    while (times--) {
	      template.pool.push(new template());
	    }
	  };
	
	  /**
	   * Main loops processor.
	   */
	  Monkberry.loop = function (parent, node, map, template, array, options) {
	    var i, j, len, keys, transform, arrayLength, childrenSize = map.length;
	
	    // Get array length, and convert object to array if needed.
	    if (Array.isArray(array)) {
	      transform = transformArray;
	      arrayLength = array.length;
	    } else {
	      transform = transformObject;
	      keys = Object.keys(array);
	      arrayLength = keys.length;
	    }
	
	    // If new array contains less items what before, remove surpluses.
	    len = childrenSize - arrayLength;
	    for (i in map.items) {
	      if (len-- > 0) {
	        map.items[i].remove();
	      } else {
	        break;
	      }
	    }
	
	    // If there is already some views, update there loop state.
	    j = 0;
	    for (i in map.items) {
	      map.items[i].__state__ = transform(array, keys, j, options);
	      j++;
	    }
	
	    // If new array contains more items when previous, render new views and append them.
	    for (j = childrenSize, len = arrayLength; j < len; j++) {
	      // Render new view.
	      var view = Monkberry.render(template, node, {parent: parent, context: parent.context, filters: parent.filters, directives: parent.directives});
	
	      // Set view hierarchy.
	      parent.nested.push(view);
	
	      // Remember to remove from children map on view remove.
	      i = map.push(view);
	      view.unbind = (function (i) {
	        return function () {
	          map.remove(i);
	        };
	      })(i);
	
	      // Set view state for later update in onUpdate.
	      view.__state__ = transform(array, keys, j, options);
	    }
	  };
	
	  /**
	   * Main if processor.
	   */
	  Monkberry.cond = function (parent, node, child/*.ref*/, template, test) {
	    if (child.ref) { // If view was already inserted, update or remove it.
	      if (!test) {
	        child.ref.remove();
	      }
	    } else if (test) {
	      // Render new view.
	      var view = Monkberry.render(template, node, {parent: parent, context: parent.context, filters: parent.filters, directives: parent.directives});
	
	      // Set view hierarchy.
	      parent.nested.push(view);
	
	      // Remember to remove child ref on remove of view.
	      child.ref = view;
	      view.unbind = function () {
	        child.ref = null;
	      };
	    }
	
	    return test;
	  };
	
	  /**
	   * Main custom tags processor.
	   */
	  Monkberry.insert = function (parent, node, child/*.ref*/, template, data) {
	    if (child.ref) { // If view was already inserted, update or remove it.
	      child.ref.update(data);
	    } else {
	      // Render new view.
	      var view = Monkberry.render(template, node, {parent: parent, context: parent.context, filters: parent.filters, directives: parent.directives});
	
	      // Set view hierarchy.
	      parent.nested.push(view);
	
	      // Remember to remove child ref on remove of view.
	      child.ref = view;
	      view.unbind = function () {
	        child.ref = null;
	      };
	
	      // Set view data (note what it must be after adding nodes to DOM).
	      view.update(data);
	    }
	  };
	
	  /**
	   * Remove view from DOM.
	   */
	  Monkberry.prototype.remove = function () {
	    // Remove appended nodes.
	    var i = this.nodes.length;
	    while (i--) {
	      this.nodes[i].parentNode.removeChild(this.nodes[i]);
	    }
	
	    // Remove self from parent's children map or child ref.
	    if (this.unbind) {
	      this.unbind();
	    }
	
	    // Remove all nested views.
	    i = this.nested.length;
	    while (i--) {
	      this.nested[i].remove();
	    }
	
	    // Remove this view from parent's nested views.
	    if (this.parent) {
	      i = this.parent.nested.indexOf(this);
	      this.parent.nested.splice(i, 1);
	      this.parent = null;
	    }
	
	    // Call on remove callback.
	    if (this.onRemove) {
	      this.onRemove();
	    }
	
	    // Store view in pool for reuse in future.
	    this.constructor.pool.push(this);
	  };
	
	  /**
	   * @param {Element} toNode
	   */
	  Monkberry.prototype.appendTo = function (toNode) {
	    for (var i = 0, len = this.nodes.length; i < len; i++) {
	      toNode.appendChild(this.nodes[i]);
	    }
	  };
	
	  /**
	   * @param {Element} toNode
	   */
	  Monkberry.prototype.insertBefore = function (toNode) {
	    if (toNode.parentNode) {
	      for (var i = 0, len = this.nodes.length; i < len; i++) {
	        toNode.parentNode.insertBefore(this.nodes[i], toNode);
	      }
	    } else {
	      throw new Error(
	        "Can not insert child view into parent node. " +
	        "You need append your view first and then update."
	      );
	    }
	  };
	
	  /**
	   * Return rendered node, or DocumentFragment of rendered nodes if more then one root node in template.
	   * @returns {Element|DocumentFragment}
	   */
	  Monkberry.prototype.createDocument = function () {
	    if (this.nodes.length == 1) {
	      return this.nodes[0];
	    } else {
	      var fragment = document.createDocumentFragment();
	      for (var i = 0, len = this.nodes.length; i < len; i++) {
	        fragment.appendChild(this.nodes[i]);
	      }
	      return fragment;
	    }
	  };
	
	  /**
	   * @param {string} query
	   * @returns {Element}
	   */
	  Monkberry.prototype.querySelector = function (query) {
	    for (var i = 0; i < this.nodes.length; i++) {
	      if (this.nodes[i].matches && this.nodes[i].matches(query)) {
	        return this.nodes[i];
	      }
	
	      if (this.nodes[i].nodeType === 8) {
	        throw new Error('Can not use querySelector with non-element nodes on first level.');
	      }
	
	      if (this.nodes[i].querySelector) {
	        var element = this.nodes[i].querySelector(query);
	        if (element) {
	          return element;
	        }
	      }
	    }
	    return null;
	  };
	
	
	  /**
	   * Simple Map implementation with length property.
	   */
	  function Map() {
	    this.items = Object.create(null);
	    this.length = 0;
	    this.next = 0;
	  }
	
	  Map.prototype.push = function (element) {
	    this.items[this.next] = element;
	    this.length += 1;
	    this.next += 1;
	    return this.next - 1;
	  };
	
	  Map.prototype.remove = function (i) {
	    if (i in this.items) {
	      delete this.items[i];
	      this.length -= 1;
	    } else {
	      throw new Error('You are trying to delete not existing element "' + i + '" form map.');
	    }
	  };
	
	  Map.prototype.forEach = function (callback) {
	    for (var i in this.items) {
	      callback(this.items[i]);
	    }
	  };
	
	  Monkberry.Map = Map;
	
	  //
	  // Helper function for working with foreach loops data.
	  // Will transform data for "key, value of array" constructions.
	  //
	
	  function transformArray(array, keys, i, options) {
	    if (options) {
	      var t = {__index__: i};
	      t[options.value] = array[i];
	
	      if (options.key) {
	        t[options.key] = i;
	      }
	
	      return t;
	    } else {
	      return array[i];
	    }
	  }
	
	  function transformObject(array, keys, i, options) {
	    if (options) {
	      var t = {__index__: i};
	      t[options.value] = array[keys[i]];
	
	      if (options.key) {
	        t[options.key] = keys[i];
	      }
	
	      return t;
	    } else {
	      return array[keys[i]];
	    }
	  }
	
	  if (true) {
	    module.exports = Monkberry;
	  } else {
	    window.Monkberry = Monkberry;
	  }
	})(window.document);


/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _monkberry = __webpack_require__(1);
	
	var _monkberry2 = _interopRequireDefault(_monkberry);
	
	var _domDelegate = __webpack_require__(3);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	_monkberry2.default.prototype.on = function (eventType, selector, handler) {
	  var _this = this;
	
	  var useCapture = arguments.length <= 3 || arguments[3] === undefined ? undefined : arguments[3];
	
	  if (!this.delegates) {
	    this.delegates = [];
	    this.nodes.forEach(function (node, i) {
	      if (node.nodeType === 8) {
	        throw 'Can not use event delegating with non-element nodes on first level.';
	      }
	
	      _this.delegates[i] = new _domDelegate.Delegate(node);
	    });
	  }
	
	  this.delegates.forEach(function (delegate) {
	    return delegate.on(eventType, selector, handler, useCapture);
	  });
	};
	
	_monkberry2.default.prototype.off = function () {
	  var eventType = arguments.length <= 0 || arguments[0] === undefined ? undefined : arguments[0];
	  var selector = arguments.length <= 1 || arguments[1] === undefined ? undefined : arguments[1];
	  var handler = arguments.length <= 2 || arguments[2] === undefined ? undefined : arguments[2];
	  var useCapture = arguments.length <= 3 || arguments[3] === undefined ? undefined : arguments[3];
	
	  this.delegates.forEach(function (delegate) {
	    return delegate.off(eventType, selector, handler, useCapture);
	  });
	};

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	/*jshint browser:true, node:true*/
	
	'use strict';
	
	/**
	 * @preserve Create and manage a DOM event delegator.
	 *
	 * @version 0.3.0
	 * @codingstandard ftlabs-jsv2
	 * @copyright The Financial Times Limited [All Rights Reserved]
	 * @license MIT License (see LICENSE.txt)
	 */
	var Delegate = __webpack_require__(4);
	
	module.exports = function(root) {
	  return new Delegate(root);
	};
	
	module.exports.Delegate = Delegate;


/***/ },
/* 4 */
/***/ function(module, exports) {

	/*jshint browser:true, node:true*/
	
	'use strict';
	
	module.exports = Delegate;
	
	/**
	 * DOM event delegator
	 *
	 * The delegator will listen
	 * for events that bubble up
	 * to the root node.
	 *
	 * @constructor
	 * @param {Node|string} [root] The root node or a selector string matching the root node
	 */
	function Delegate(root) {
	
	  /**
	   * Maintain a map of listener
	   * lists, keyed by event name.
	   *
	   * @type Object
	   */
	  this.listenerMap = [{}, {}];
	  if (root) {
	    this.root(root);
	  }
	
	  /** @type function() */
	  this.handle = Delegate.prototype.handle.bind(this);
	}
	
	/**
	 * Start listening for events
	 * on the provided DOM element
	 *
	 * @param  {Node|string} [root] The root node or a selector string matching the root node
	 * @returns {Delegate} This method is chainable
	 */
	Delegate.prototype.root = function(root) {
	  var listenerMap = this.listenerMap;
	  var eventType;
	
	  // Remove master event listeners
	  if (this.rootElement) {
	    for (eventType in listenerMap[1]) {
	      if (listenerMap[1].hasOwnProperty(eventType)) {
	        this.rootElement.removeEventListener(eventType, this.handle, true);
	      }
	    }
	    for (eventType in listenerMap[0]) {
	      if (listenerMap[0].hasOwnProperty(eventType)) {
	        this.rootElement.removeEventListener(eventType, this.handle, false);
	      }
	    }
	  }
	
	  // If no root or root is not
	  // a dom node, then remove internal
	  // root reference and exit here
	  if (!root || !root.addEventListener) {
	    if (this.rootElement) {
	      delete this.rootElement;
	    }
	    return this;
	  }
	
	  /**
	   * The root node at which
	   * listeners are attached.
	   *
	   * @type Node
	   */
	  this.rootElement = root;
	
	  // Set up master event listeners
	  for (eventType in listenerMap[1]) {
	    if (listenerMap[1].hasOwnProperty(eventType)) {
	      this.rootElement.addEventListener(eventType, this.handle, true);
	    }
	  }
	  for (eventType in listenerMap[0]) {
	    if (listenerMap[0].hasOwnProperty(eventType)) {
	      this.rootElement.addEventListener(eventType, this.handle, false);
	    }
	  }
	
	  return this;
	};
	
	/**
	 * @param {string} eventType
	 * @returns boolean
	 */
	Delegate.prototype.captureForType = function(eventType) {
	  return ['blur', 'error', 'focus', 'load', 'resize', 'scroll'].indexOf(eventType) !== -1;
	};
	
	/**
	 * Attach a handler to one
	 * event for all elements
	 * that match the selector,
	 * now or in the future
	 *
	 * The handler function receives
	 * three arguments: the DOM event
	 * object, the node that matched
	 * the selector while the event
	 * was bubbling and a reference
	 * to itself. Within the handler,
	 * 'this' is equal to the second
	 * argument.
	 *
	 * The node that actually received
	 * the event can be accessed via
	 * 'event.target'.
	 *
	 * @param {string} eventType Listen for these events
	 * @param {string|undefined} selector Only handle events on elements matching this selector, if undefined match root element
	 * @param {function()} handler Handler function - event data passed here will be in event.data
	 * @param {Object} [eventData] Data to pass in event.data
	 * @returns {Delegate} This method is chainable
	 */
	Delegate.prototype.on = function(eventType, selector, handler, useCapture) {
	  var root, listenerMap, matcher, matcherParam;
	
	  if (!eventType) {
	    throw new TypeError('Invalid event type: ' + eventType);
	  }
	
	  // handler can be passed as
	  // the second or third argument
	  if (typeof selector === 'function') {
	    useCapture = handler;
	    handler = selector;
	    selector = null;
	  }
	
	  // Fallback to sensible defaults
	  // if useCapture not set
	  if (useCapture === undefined) {
	    useCapture = this.captureForType(eventType);
	  }
	
	  if (typeof handler !== 'function') {
	    throw new TypeError('Handler must be a type of Function');
	  }
	
	  root = this.rootElement;
	  listenerMap = this.listenerMap[useCapture ? 1 : 0];
	
	  // Add master handler for type if not created yet
	  if (!listenerMap[eventType]) {
	    if (root) {
	      root.addEventListener(eventType, this.handle, useCapture);
	    }
	    listenerMap[eventType] = [];
	  }
	
	  if (!selector) {
	    matcherParam = null;
	
	    // COMPLEX - matchesRoot needs to have access to
	    // this.rootElement, so bind the function to this.
	    matcher = matchesRoot.bind(this);
	
	  // Compile a matcher for the given selector
	  } else if (/^[a-z]+$/i.test(selector)) {
	    matcherParam = selector;
	    matcher = matchesTag;
	  } else if (/^#[a-z0-9\-_]+$/i.test(selector)) {
	    matcherParam = selector.slice(1);
	    matcher = matchesId;
	  } else {
	    matcherParam = selector;
	    matcher = matches;
	  }
	
	  // Add to the list of listeners
	  listenerMap[eventType].push({
	    selector: selector,
	    handler: handler,
	    matcher: matcher,
	    matcherParam: matcherParam
	  });
	
	  return this;
	};
	
	/**
	 * Remove an event handler
	 * for elements that match
	 * the selector, forever
	 *
	 * @param {string} [eventType] Remove handlers for events matching this type, considering the other parameters
	 * @param {string} [selector] If this parameter is omitted, only handlers which match the other two will be removed
	 * @param {function()} [handler] If this parameter is omitted, only handlers which match the previous two will be removed
	 * @returns {Delegate} This method is chainable
	 */
	Delegate.prototype.off = function(eventType, selector, handler, useCapture) {
	  var i, listener, listenerMap, listenerList, singleEventType;
	
	  // Handler can be passed as
	  // the second or third argument
	  if (typeof selector === 'function') {
	    useCapture = handler;
	    handler = selector;
	    selector = null;
	  }
	
	  // If useCapture not set, remove
	  // all event listeners
	  if (useCapture === undefined) {
	    this.off(eventType, selector, handler, true);
	    this.off(eventType, selector, handler, false);
	    return this;
	  }
	
	  listenerMap = this.listenerMap[useCapture ? 1 : 0];
	  if (!eventType) {
	    for (singleEventType in listenerMap) {
	      if (listenerMap.hasOwnProperty(singleEventType)) {
	        this.off(singleEventType, selector, handler);
	      }
	    }
	
	    return this;
	  }
	
	  listenerList = listenerMap[eventType];
	  if (!listenerList || !listenerList.length) {
	    return this;
	  }
	
	  // Remove only parameter matches
	  // if specified
	  for (i = listenerList.length - 1; i >= 0; i--) {
	    listener = listenerList[i];
	
	    if ((!selector || selector === listener.selector) && (!handler || handler === listener.handler)) {
	      listenerList.splice(i, 1);
	    }
	  }
	
	  // All listeners removed
	  if (!listenerList.length) {
	    delete listenerMap[eventType];
	
	    // Remove the main handler
	    if (this.rootElement) {
	      this.rootElement.removeEventListener(eventType, this.handle, useCapture);
	    }
	  }
	
	  return this;
	};
	
	
	/**
	 * Handle an arbitrary event.
	 *
	 * @param {Event} event
	 */
	Delegate.prototype.handle = function(event) {
	  var i, l, type = event.type, root, phase, listener, returned, listenerList = [], target, /** @const */ EVENTIGNORE = 'ftLabsDelegateIgnore';
	
	  if (event[EVENTIGNORE] === true) {
	    return;
	  }
	
	  target = event.target;
	
	  // Hardcode value of Node.TEXT_NODE
	  // as not defined in IE8
	  if (target.nodeType === 3) {
	    target = target.parentNode;
	  }
	
	  root = this.rootElement;
	
	  phase = event.eventPhase || ( event.target !== event.currentTarget ? 3 : 2 );
	  
	  switch (phase) {
	    case 1: //Event.CAPTURING_PHASE:
	      listenerList = this.listenerMap[1][type];
	    break;
	    case 2: //Event.AT_TARGET:
	      if (this.listenerMap[0] && this.listenerMap[0][type]) listenerList = listenerList.concat(this.listenerMap[0][type]);
	      if (this.listenerMap[1] && this.listenerMap[1][type]) listenerList = listenerList.concat(this.listenerMap[1][type]);
	    break;
	    case 3: //Event.BUBBLING_PHASE:
	      listenerList = this.listenerMap[0][type];
	    break;
	  }
	
	  // Need to continuously check
	  // that the specific list is
	  // still populated in case one
	  // of the callbacks actually
	  // causes the list to be destroyed.
	  l = listenerList.length;
	  while (target && l) {
	    for (i = 0; i < l; i++) {
	      listener = listenerList[i];
	
	      // Bail from this loop if
	      // the length changed and
	      // no more listeners are
	      // defined between i and l.
	      if (!listener) {
	        break;
	      }
	
	      // Check for match and fire
	      // the event if there's one
	      //
	      // TODO:MCG:20120117: Need a way
	      // to check if event#stopImmediatePropagation
	      // was called. If so, break both loops.
	      if (listener.matcher.call(target, listener.matcherParam, target)) {
	        returned = this.fire(event, target, listener);
	      }
	
	      // Stop propagation to subsequent
	      // callbacks if the callback returned
	      // false
	      if (returned === false) {
	        event[EVENTIGNORE] = true;
	        event.preventDefault();
	        return;
	      }
	    }
	
	    // TODO:MCG:20120117: Need a way to
	    // check if event#stopPropagation
	    // was called. If so, break looping
	    // through the DOM. Stop if the
	    // delegation root has been reached
	    if (target === root) {
	      break;
	    }
	
	    l = listenerList.length;
	    target = target.parentElement;
	  }
	};
	
	/**
	 * Fire a listener on a target.
	 *
	 * @param {Event} event
	 * @param {Node} target
	 * @param {Object} listener
	 * @returns {boolean}
	 */
	Delegate.prototype.fire = function(event, target, listener) {
	  return listener.handler.call(target, event, target);
	};
	
	/**
	 * Check whether an element
	 * matches a generic selector.
	 *
	 * @type function()
	 * @param {string} selector A CSS selector
	 */
	var matches = (function(el) {
	  if (!el) return;
	  var p = el.prototype;
	  return (p.matches || p.matchesSelector || p.webkitMatchesSelector || p.mozMatchesSelector || p.msMatchesSelector || p.oMatchesSelector);
	}(Element));
	
	/**
	 * Check whether an element
	 * matches a tag selector.
	 *
	 * Tags are NOT case-sensitive,
	 * except in XML (and XML-based
	 * languages such as XHTML).
	 *
	 * @param {string} tagName The tag name to test against
	 * @param {Element} element The element to test with
	 * @returns boolean
	 */
	function matchesTag(tagName, element) {
	  return tagName.toLowerCase() === element.tagName.toLowerCase();
	}
	
	/**
	 * Check whether an element
	 * matches the root.
	 *
	 * @param {?String} selector In this case this is always passed through as null and not used
	 * @param {Element} element The element to test with
	 * @returns boolean
	 */
	function matchesRoot(selector, element) {
	  /*jshint validthis:true*/
	  if (this.rootElement === window) return element === document;
	  return this.rootElement === element;
	}
	
	/**
	 * Check whether the ID of
	 * the element in 'this'
	 * matches the given ID.
	 *
	 * IDs are case-sensitive.
	 *
	 * @param {string} id The ID to test against
	 * @param {Element} element The element to test with
	 * @returns boolean
	 */
	function matchesId(id, element) {
	  return id === element.id;
	}
	
	/**
	 * Short hand for off()
	 * and root(), ie both
	 * with no parameters
	 *
	 * @return void
	 */
	Delegate.prototype.destroy = function() {
	  this.off();
	  this.root();
	};


/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	var Monkberry = __webpack_require__(1);
	
	/**
	 * @class
	 */
	function todo() {
	  Monkberry.call(this);
	  var _this = this;
	
	  // Create elements
	  var div0 = document.createElement('div');
	  var ol1 = document.createElement('ol');
	  var children0 = new Monkberry.Map();
	  var form2 = document.createElement('form');
	  var input3 = document.createElement('input');
	  var button4 = document.createElement('button');
	  var div5 = document.createElement('div');
	  var button6 = document.createElement('button');
	
	  // Construct dom
	  input3.setAttribute("type", "text");
	  input3.setAttribute("placeholder", "#");
	  button4.appendChild(document.createTextNode("Add"));
	  button4.setAttribute("type", "submit");
	  form2.appendChild(input3);
	  form2.appendChild(button4);
	  div0.appendChild(ol1);
	  div0.appendChild(form2);
	  div0.setAttribute("class", "todo");
	  button6.appendChild(document.createTextNode("Quit"));
	  div5.appendChild(button6);
	  div5.setAttribute("class", "quit");
	
	  // Update functions
	  this.__update__ = {
	    todos: function (todos) {
	      Monkberry.loop(_this, ol1, children0, todo_for0, todos, {"key":"i","value":"todo"});
	      input3.setAttribute("placeholder", ("#") + ((todos.length) + (1)));;
	    }
	  };
	
	  // On update actions
	  this.onUpdate = function (__data__) {
	    children0.forEach(function (view) {
	      view.update(__data__);
	      view.update(view.__state__);
	    });
	  };
	
	  // Set root nodes
	  this.nodes = [div0, div5];
	}
	todo.prototype = Object.create(Monkberry.prototype);
	todo.prototype.constructor = todo;
	todo.pool = [];
	todo.prototype.update = function (__data__) {
	  if (__data__.todos !== undefined) {
	    this.__update__.todos(__data__.todos);
	  }
	  this.onUpdate(__data__);
	};
	
	/**
	 * @class
	 */
	function todo_for0() {
	  Monkberry.call(this);
	  this.__state__ = {};
	  var _this = this;
	
	  // Create elements
	  var li0 = document.createElement('li');
	  var label1 = document.createElement('label');
	  var span2 = document.createElement('span');
	  var input3 = document.createElement('input');
	  var svg4 = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
	  var path5 = document.createElementNS('http://www.w3.org/2000/svg', 'path');
	  var path6 = document.createElementNS('http://www.w3.org/2000/svg', 'path');
	  var span7 = document.createElement('span');
	  var child0 = {};
	  var child1 = {};
	  var span8 = document.createElement('span');
	  var svg9 = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
	  var path10 = document.createElementNS('http://www.w3.org/2000/svg', 'path');
	
	  // Construct dom
	  input3.setAttribute("type", "checkbox");
	  path5.setAttribute("d", "M400,200c0,110.457-89.543,200-200,200l0,0C89.543,400,0,310.457,0,200l0,0C0,89.543,89.543,0,200,0l0,0\n          C310.457,0,400,89.543,400,200L400,200z");
	  path6.setAttribute("class", "tick");
	  path6.setAttribute("d", "M290.994,133.994c-6.001-6-15.5-6-21.502,0l-95.499,98l-42.501-44c-6-6-15.5-6-21.5,0\n            c-6,6.002-6,16.001,0,22l53.499,55.001c6,6,15.501,6,21.501,0l106.501-109C296.994,149.996,296.994,139.994,290.994,133.994z");
	  svg4.appendChild(path5);
	  svg4.appendChild(path6);
	  svg4.setAttribute("version", "1.1");
	  svg4.id = "Layer_1";
	  svg4.setAttribute("xmlns", "http://www.w3.org/2000/svg");
	  svg4.setAttribute("width", "400px");
	  svg4.setAttribute("height", "400px");
	  svg4.setAttribute("viewBox", "0 0 400 400");
	  svg4.setAttribute("preserveAspectRatio", "xMidYMid");
	  span2.appendChild(input3);
	  span2.appendChild(svg4);
	  span2.setAttribute("class", "checkbox");
	  span7.setAttribute("class", "label");
	  label1.appendChild(span2);
	  label1.appendChild(span7);
	  path10.setAttribute("d", "M29.88 2.322L27.583.026l-12.63 12.63L2.323.027.026 2.324l12.63 12.63-12.63 12.63 2.297 2.297 12.63-12.63 12.63 12.63 2.297-2.296-12.63-12.63");
	  svg9.appendChild(path10);
	  svg9.setAttribute("xmlns", "http://www.w3.org/2000/svg");
	  svg9.setAttribute("width", "30");
	  svg9.setAttribute("height", "30");
	  svg9.setAttribute("viewBox", "0 0 30 30");
	  svg9.setAttribute("preserveAspectRatio", "xMidYMid");
	  span8.appendChild(svg9);
	  span8.setAttribute("class", "delete");
	  li0.appendChild(label1);
	  li0.appendChild(span8);
	
	  // Update functions
	  this.__update__ = {
	    todo: function (todo) {
	      var result;
	      input3.checked = todo.complete;;
	      input3.setAttribute("data-index", todo.id);;
	      result = Monkberry.cond(_this, span7, child0, todo_for0_if0, todo.complete);
	      Monkberry.cond(_this, span7, child1, todo_for0_else1, !result);
	      li0.setAttribute("data-index", todo.id);;
	      li0.setAttribute("data-complete", todo.complete);;
	    }
	  };
	
	  // On update actions
	  this.onUpdate = function (__data__) {
	    if (child0.ref) {
	      child0.ref.update(__data__);
	    }
	    if (child1.ref) {
	      child1.ref.update(__data__);
	    }
	  };
	
	  // Set root nodes
	  this.nodes = [li0];
	}
	todo_for0.prototype = Object.create(Monkberry.prototype);
	todo_for0.prototype.constructor = todo_for0;
	todo_for0.pool = [];
	todo_for0.prototype.update = function (__data__) {
	  if (__data__.todo !== undefined && __data__.__index__ !== undefined) {
	    this.__update__.todo(__data__.todo);
	  }
	  if (__data__.i !== undefined && __data__.__index__ !== undefined) {
	  }
	  this.onUpdate(__data__);
	};
	
	/**
	 * @class
	 */
	function todo_for0_if0() {
	  Monkberry.call(this);
	
	  // Create elements
	  var del0 = document.createElement('del');
	  var text1 = document.createTextNode('');
	
	  // Construct dom
	  del0.appendChild(text1);
	
	  // Update functions
	  this.__update__ = {
	    todo: function (todo) {
	      text1.textContent = todo.text;
	    }
	  };
	
	  // Set root nodes
	  this.nodes = [del0];
	}
	todo_for0_if0.prototype = Object.create(Monkberry.prototype);
	todo_for0_if0.prototype.constructor = todo_for0_if0;
	todo_for0_if0.pool = [];
	todo_for0_if0.prototype.update = function (__data__) {
	  if (__data__.todo !== undefined) {
	    this.__update__.todo(__data__.todo);
	  }
	};
	
	/**
	 * @class
	 */
	function todo_for0_else1() {
	  Monkberry.call(this);
	
	  // Create elements
	  var em0 = document.createElement('em');
	  var text1 = document.createTextNode('');
	
	  // Construct dom
	  em0.appendChild(text1);
	
	  // Update functions
	  this.__update__ = {
	    todo: function (todo) {
	      text1.textContent = todo.text;
	    }
	  };
	
	  // Set root nodes
	  this.nodes = [em0];
	}
	todo_for0_else1.prototype = Object.create(Monkberry.prototype);
	todo_for0_else1.prototype.constructor = todo_for0_else1;
	todo_for0_else1.pool = [];
	todo_for0_else1.prototype.update = function (__data__) {
	  if (__data__.todo !== undefined) {
	    this.__update__.todo(__data__.todo);
	  }
	};
	
	module.exports = todo;


/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global, setImmediate) {(function (global, factory) {
	    true ? module.exports = factory() :
	   typeof define === 'function' && define.amd ? define(factory) :
	   global.Dexie = factory();
	}(this, function () { 'use strict';
	
	   // By default, debug will be true only if platform is a web platform and its page is served from localhost.
	   // When debug = true, error's stacks will contain asyncronic long stacks.
	   var debug = typeof location !== 'undefined' &&
	   // By default, use debug mode if served from localhost.
	   /^(http|https):\/\/(localhost|127\.0\.0\.1)/.test(location.href);
	
	   function setDebug(value, filter) {
	       debug = value;
	       libraryFilter = filter;
	   }
	
	   var libraryFilter = function () {
	       return true;
	   };
	
	   var NEEDS_THROW_FOR_STACK = !new Error("").stack;
	
	   function getErrorWithStack() {
	       "use strict";
	
	       if (NEEDS_THROW_FOR_STACK) try {
	           // Doing something naughty in strict mode here to trigger a specific error
	           // that can be explicitely ignored in debugger's exception settings.
	           // If we'd just throw new Error() here, IE's debugger's exception settings
	           // will just consider it as "exception thrown by javascript code" which is
	           // something you wouldn't want it to ignore.
	           getErrorWithStack.arguments;
	           throw new Error(); // Fallback if above line don't throw.
	       } catch (e) {
	           return e;
	       }
	       return new Error();
	   }
	
	   function prettyStack(exception, numIgnoredFrames) {
	       var stack = exception.stack;
	       if (!stack) return "";
	       numIgnoredFrames = numIgnoredFrames || 0;
	       if (stack.indexOf(exception.name) === 0) numIgnoredFrames += (exception.name + exception.message).split('\n').length;
	       return stack.split('\n').slice(numIgnoredFrames).filter(libraryFilter).map(function (frame) {
	           return "\n" + frame;
	       }).join('');
	   }
	
	   function nop() {}
	   function mirror(val) {
	       return val;
	   }
	   function pureFunctionChain(f1, f2) {
	       // Enables chained events that takes ONE argument and returns it to the next function in chain.
	       // This pattern is used in the hook("reading") event.
	       if (f1 == null || f1 === mirror) return f2;
	       return function (val) {
	           return f2(f1(val));
	       };
	   }
	
	   function callBoth(on1, on2) {
	       return function () {
	           on1.apply(this, arguments);
	           on2.apply(this, arguments);
	       };
	   }
	
	   function hookCreatingChain(f1, f2) {
	       // Enables chained events that takes several arguments and may modify first argument by making a modification and then returning the same instance.
	       // This pattern is used in the hook("creating") event.
	       if (f1 === nop) return f2;
	       return function () {
	           var res = f1.apply(this, arguments);
	           if (res !== undefined) arguments[0] = res;
	           var onsuccess = this.onsuccess,
	               // In case event listener has set this.onsuccess
	           onerror = this.onerror; // In case event listener has set this.onerror
	           this.onsuccess = null;
	           this.onerror = null;
	           var res2 = f2.apply(this, arguments);
	           if (onsuccess) this.onsuccess = this.onsuccess ? callBoth(onsuccess, this.onsuccess) : onsuccess;
	           if (onerror) this.onerror = this.onerror ? callBoth(onerror, this.onerror) : onerror;
	           return res2 !== undefined ? res2 : res;
	       };
	   }
	
	   function hookDeletingChain(f1, f2) {
	       if (f1 === nop) return f2;
	       return function () {
	           f1.apply(this, arguments);
	           var onsuccess = this.onsuccess,
	               // In case event listener has set this.onsuccess
	           onerror = this.onerror; // In case event listener has set this.onerror
	           this.onsuccess = this.onerror = null;
	           f2.apply(this, arguments);
	           if (onsuccess) this.onsuccess = this.onsuccess ? callBoth(onsuccess, this.onsuccess) : onsuccess;
	           if (onerror) this.onerror = this.onerror ? callBoth(onerror, this.onerror) : onerror;
	       };
	   }
	
	   function hookUpdatingChain(f1, f2) {
	       if (f1 === nop) return f2;
	       return function (modifications) {
	           var res = f1.apply(this, arguments);
	           extend(modifications, res); // If f1 returns new modifications, extend caller's modifications with the result before calling next in chain.
	           var onsuccess = this.onsuccess,
	               // In case event listener has set this.onsuccess
	           onerror = this.onerror; // In case event listener has set this.onerror
	           this.onsuccess = null;
	           this.onerror = null;
	           var res2 = f2.apply(this, arguments);
	           if (onsuccess) this.onsuccess = this.onsuccess ? callBoth(onsuccess, this.onsuccess) : onsuccess;
	           if (onerror) this.onerror = this.onerror ? callBoth(onerror, this.onerror) : onerror;
	           return res === undefined ? res2 === undefined ? undefined : res2 : extend(res, res2);
	       };
	   }
	
	   function reverseStoppableEventChain(f1, f2) {
	       if (f1 === nop) return f2;
	       return function () {
	           if (f2.apply(this, arguments) === false) return false;
	           return f1.apply(this, arguments);
	       };
	   }
	
	   function promisableChain(f1, f2) {
	       if (f1 === nop) return f2;
	       return function () {
	           var res = f1.apply(this, arguments);
	           if (res && typeof res.then === 'function') {
	               var thiz = this,
	                   i = arguments.length,
	                   args = new Array(i);
	               while (i--) {
	                   args[i] = arguments[i];
	               }return res.then(function () {
	                   return f2.apply(thiz, args);
	               });
	           }
	           return f2.apply(this, arguments);
	       };
	   }
	
	   var keys = Object.keys;
	   var isArray = Array.isArray;
	   var _global = typeof self !== 'undefined' ? self : typeof window !== 'undefined' ? window : global;
	
	   function extend(obj, extension) {
	       if (typeof extension !== 'object') return obj;
	       keys(extension).forEach(function (key) {
	           obj[key] = extension[key];
	       });
	       return obj;
	   }
	
	   var getProto = Object.getPrototypeOf;
	   var _hasOwn = {}.hasOwnProperty;
	   function hasOwn(obj, prop) {
	       return _hasOwn.call(obj, prop);
	   }
	
	   function props(proto, extension) {
	       if (typeof extension === 'function') extension = extension(getProto(proto));
	       keys(extension).forEach(function (key) {
	           setProp(proto, key, extension[key]);
	       });
	   }
	
	   function setProp(obj, prop, functionOrGetSet, options) {
	       Object.defineProperty(obj, prop, extend(functionOrGetSet && hasOwn(functionOrGetSet, "get") && typeof functionOrGetSet.get === 'function' ? { get: functionOrGetSet.get, set: functionOrGetSet.set, configurable: true } : { value: functionOrGetSet, configurable: true, writable: true }, options));
	   }
	
	   function derive(Child) {
	       return {
	           from: function (Parent) {
	               Child.prototype = Object.create(Parent.prototype);
	               setProp(Child.prototype, "constructor", Child);
	               return {
	                   extend: props.bind(null, Child.prototype)
	               };
	           }
	       };
	   }
	
	   var getOwnPropertyDescriptor = Object.getOwnPropertyDescriptor;
	
	   function getPropertyDescriptor(obj, prop) {
	       var pd = getOwnPropertyDescriptor(obj, prop),
	           proto;
	       return pd || (proto = getProto(obj)) && getPropertyDescriptor(proto, prop);
	   }
	
	   var _slice = [].slice;
	   function slice(args, start, end) {
	       return _slice.call(args, start, end);
	   }
	
	   function override(origFunc, overridedFactory) {
	       return overridedFactory(origFunc);
	   }
	
	   function doFakeAutoComplete(fn) {
	       var to = setTimeout(fn, 1000);
	       clearTimeout(to);
	   }
	
	   function assert(b) {
	       if (!b) throw new exceptions.Internal("Assertion failed");
	   }
	
	   function asap(fn) {
	       if (_global.setImmediate) setImmediate(fn);else setTimeout(fn, 0);
	   }
	
	   /** Generate an object (hash map) based on given array.
	    * @param extractor Function taking an array item and its index and returning an array of 2 items ([key, value]) to
	    *        instert on the resulting object for each item in the array. If this function returns a falsy value, the
	    *        current item wont affect the resulting object.
	    */
	   function arrayToObject(array, extractor) {
	       return array.reduce(function (result, item, i) {
	           var nameAndValue = extractor(item, i);
	           if (nameAndValue) result[nameAndValue[0]] = nameAndValue[1];
	           return result;
	       }, {});
	   }
	
	   function trycatcher(fn, reject) {
	       return function () {
	           try {
	               fn.apply(this, arguments);
	           } catch (e) {
	               reject(e);
	           }
	       };
	   }
	
	   function tryCatch(fn, onerror, args) {
	       try {
	           fn.apply(null, args);
	       } catch (ex) {
	           onerror && onerror(ex);
	       }
	   }
	
	   function rejection(err, uncaughtHandler) {
	       // Get the call stack and return a rejected promise.
	       var rv = Promise.reject(err);
	       return uncaughtHandler ? rv.uncaught(uncaughtHandler) : rv;
	   }
	
	   function getByKeyPath(obj, keyPath) {
	       // http://www.w3.org/TR/IndexedDB/#steps-for-extracting-a-key-from-a-value-using-a-key-path
	       if (hasOwn(obj, keyPath)) return obj[keyPath]; // This line is moved from last to first for optimization purpose.
	       if (!keyPath) return obj;
	       if (typeof keyPath !== 'string') {
	           var rv = [];
	           for (var i = 0, l = keyPath.length; i < l; ++i) {
	               var val = getByKeyPath(obj, keyPath[i]);
	               rv.push(val);
	           }
	           return rv;
	       }
	       var period = keyPath.indexOf('.');
	       if (period !== -1) {
	           var innerObj = obj[keyPath.substr(0, period)];
	           return innerObj === undefined ? undefined : getByKeyPath(innerObj, keyPath.substr(period + 1));
	       }
	       return undefined;
	   }
	
	   function setByKeyPath(obj, keyPath, value) {
	       if (!obj || keyPath === undefined) return;
	       if ('isFrozen' in Object && Object.isFrozen(obj)) return;
	       if (typeof keyPath !== 'string' && 'length' in keyPath) {
	           assert(typeof value !== 'string' && 'length' in value);
	           for (var i = 0, l = keyPath.length; i < l; ++i) {
	               setByKeyPath(obj, keyPath[i], value[i]);
	           }
	       } else {
	           var period = keyPath.indexOf('.');
	           if (period !== -1) {
	               var currentKeyPath = keyPath.substr(0, period);
	               var remainingKeyPath = keyPath.substr(period + 1);
	               if (remainingKeyPath === "") {
	                   if (value === undefined) delete obj[currentKeyPath];else obj[currentKeyPath] = value;
	               } else {
	                   var innerObj = obj[currentKeyPath];
	                   if (!innerObj) innerObj = obj[currentKeyPath] = {};
	                   setByKeyPath(innerObj, remainingKeyPath, value);
	               }
	           } else {
	               if (value === undefined) delete obj[keyPath];else obj[keyPath] = value;
	           }
	       }
	   }
	
	   function delByKeyPath(obj, keyPath) {
	       if (typeof keyPath === 'string') setByKeyPath(obj, keyPath, undefined);else if ('length' in keyPath) [].map.call(keyPath, function (kp) {
	           setByKeyPath(obj, kp, undefined);
	       });
	   }
	
	   function shallowClone(obj) {
	       var rv = {};
	       for (var m in obj) {
	           if (hasOwn(obj, m)) rv[m] = obj[m];
	       }
	       return rv;
	   }
	
	   function deepClone(any) {
	       if (!any || typeof any !== 'object') return any;
	       var rv;
	       if (isArray(any)) {
	           rv = [];
	           for (var i = 0, l = any.length; i < l; ++i) {
	               rv.push(deepClone(any[i]));
	           }
	       } else if (any instanceof Date) {
	           rv = new Date();
	           rv.setTime(any.getTime());
	       } else {
	           rv = any.constructor ? Object.create(any.constructor.prototype) : {};
	           for (var prop in any) {
	               if (hasOwn(any, prop)) {
	                   rv[prop] = deepClone(any[prop]);
	               }
	           }
	       }
	       return rv;
	   }
	
	   function getObjectDiff(a, b, rv, prfx) {
	       // Compares objects a and b and produces a diff object.
	       rv = rv || {};
	       prfx = prfx || '';
	       keys(a).forEach(function (prop) {
	           if (!hasOwn(b, prop)) rv[prfx + prop] = undefined; // Property removed
	           else {
	                   var ap = a[prop],
	                       bp = b[prop];
	                   if (typeof ap === 'object' && typeof bp === 'object' && ap && bp && ap.constructor === bp.constructor)
	                       // Same type of object but its properties may have changed
	                       getObjectDiff(ap, bp, rv, prfx + prop + ".");else if (ap !== bp) rv[prfx + prop] = b[prop]; // Primitive value changed
	               }
	       });
	       keys(b).forEach(function (prop) {
	           if (!hasOwn(a, prop)) {
	               rv[prfx + prop] = b[prop]; // Property added
	           }
	       });
	       return rv;
	   }
	
	   // If first argument is iterable or array-like, return it as an array
	   var iteratorSymbol = typeof Symbol !== 'undefined' && Symbol.iterator;
	   var getIteratorOf = iteratorSymbol ? function (x) {
	       var i;
	       return x != null && (i = x[iteratorSymbol]) && i.apply(x);
	   } : function () {
	       return null;
	   };
	
	   var NO_CHAR_ARRAY = {};
	   // Takes one or several arguments and returns an array based on the following criteras:
	   // * If several arguments provided, return arguments converted to an array in a way that
	   //   still allows javascript engine to optimize the code.
	   // * If single argument is an array, return a clone of it.
	   // * If this-pointer equals NO_CHAR_ARRAY, don't accept strings as valid iterables as a special
	   //   case to the two bullets below.
	   // * If single argument is an iterable, convert it to an array and return the resulting array.
	   // * If single argument is array-like (has length of type number), convert it to an array.
	   function getArrayOf(arrayLike) {
	       var i, a, x, it;
	       if (arguments.length === 1) {
	           if (isArray(arrayLike)) return arrayLike.slice();
	           if (this === NO_CHAR_ARRAY && typeof arrayLike === 'string') return [arrayLike];
	           if (it = getIteratorOf(arrayLike)) {
	               a = [];
	               while (x = it.next(), !x.done) {
	                   a.push(x.value);
	               }return a;
	           }
	           if (arrayLike == null) return [arrayLike];
	           i = arrayLike.length;
	           if (typeof i === 'number') {
	               a = new Array(i);
	               while (i--) {
	                   a[i] = arrayLike[i];
	               }return a;
	           }
	           return [arrayLike];
	       }
	       i = arguments.length;
	       a = new Array(i);
	       while (i--) {
	           a[i] = arguments[i];
	       }return a;
	   }
	
	   var concat = [].concat;
	   function flatten(a) {
	       return concat.apply([], a);
	   }
	
	   var dexieErrorNames = ['Modify', 'Bulk', 'OpenFailed', 'VersionChange', 'Schema', 'Upgrade', 'InvalidTable', 'MissingAPI', 'NoSuchDatabase', 'InvalidArgument', 'SubTransaction', 'Unsupported', 'Internal', 'DatabaseClosed', 'IncompatiblePromise'];
	
	   var idbDomErrorNames = ['Unknown', 'Constraint', 'Data', 'TransactionInactive', 'ReadOnly', 'Version', 'NotFound', 'InvalidState', 'InvalidAccess', 'Abort', 'Timeout', 'QuotaExceeded', 'Syntax', 'DataClone'];
	
	   var errorList = dexieErrorNames.concat(idbDomErrorNames);
	
	   var defaultTexts = {
	       VersionChanged: "Database version changed by other database connection",
	       DatabaseClosed: "Database has been closed",
	       Abort: "Transaction aborted",
	       TransactionInactive: "Transaction has already completed or failed"
	   };
	
	   //
	   // DexieError - base class of all out exceptions.
	   //
	   function DexieError(name, msg) {
	       // Reason we don't use ES6 classes is because:
	       // 1. It bloats transpiled code and increases size of minified code.
	       // 2. It doesn't give us much in this case.
	       // 3. It would require sub classes to call super(), which
	       //    is not needed when deriving from Error.
	       this._e = getErrorWithStack();
	       this.name = name;
	       this.message = msg;
	   }
	
	   derive(DexieError).from(Error).extend({
	       stack: {
	           get: function () {
	               return this._stack || (this._stack = this.name + ": " + this.message + prettyStack(this._e, 2));
	           }
	       },
	       toString: function () {
	           return this.name + ": " + this.message;
	       }
	   });
	
	   function getMultiErrorMessage(msg, failures) {
	       return msg + ". Errors: " + failures.map(function (f) {
	           return f.toString();
	       }).filter(function (v, i, s) {
	           return s.indexOf(v) === i;
	       }) // Only unique error strings
	       .join('\n');
	   }
	
	   //
	   // ModifyError - thrown in WriteableCollection.modify()
	   // Specific constructor because it contains members failures and failedKeys.
	   //
	   function ModifyError(msg, failures, successCount, failedKeys) {
	       this._e = getErrorWithStack();
	       this.failures = failures;
	       this.failedKeys = failedKeys;
	       this.successCount = successCount;
	   }
	   derive(ModifyError).from(DexieError);
	
	   function BulkError(msg, failures) {
	       this._e = getErrorWithStack();
	       this.name = "BulkError";
	       this.failures = failures;
	       this.message = getMultiErrorMessage(msg, failures);
	   }
	   derive(BulkError).from(DexieError);
	
	   //
	   //
	   // Dynamically generate error names and exception classes based
	   // on the names in errorList.
	   //
	   //
	
	   // Map of {ErrorName -> ErrorName + "Error"}
	   var errnames = errorList.reduce(function (obj, name) {
	       return obj[name] = name + "Error", obj;
	   }, {});
	
	   // Need an alias for DexieError because we're gonna create subclasses with the same name.
	   var BaseException = DexieError;
	   // Map of {ErrorName -> exception constructor}
	   var exceptions = errorList.reduce(function (obj, name) {
	       // Let the name be "DexieError" because this name may
	       // be shown in call stack and when debugging. DexieError is
	       // the most true name because it derives from DexieError,
	       // and we cannot change Function.name programatically without
	       // dynamically create a Function object, which would be considered
	       // 'eval-evil'.
	       var fullName = name + "Error";
	       function DexieError(msgOrInner, inner) {
	           this._e = getErrorWithStack();
	           this.name = fullName;
	           if (!msgOrInner) {
	               this.message = defaultTexts[name] || fullName;
	               this.inner = null;
	           } else if (typeof msgOrInner === 'string') {
	               this.message = msgOrInner;
	               this.inner = inner || null;
	           } else if (typeof msgOrInner === 'object') {
	               this.message = msgOrInner.name + ' ' + msgOrInner.message;
	               this.inner = msgOrInner;
	           }
	       }
	       derive(DexieError).from(BaseException);
	       obj[name] = DexieError;
	       return obj;
	   }, {});
	
	   // Use ECMASCRIPT standard exceptions where applicable:
	   exceptions.Syntax = SyntaxError;
	   exceptions.Type = TypeError;
	   exceptions.Range = RangeError;
	
	   var exceptionMap = idbDomErrorNames.reduce(function (obj, name) {
	       obj[name + "Error"] = exceptions[name];
	       return obj;
	   }, {});
	
	   function mapError(domError, message) {
	       if (!domError || domError instanceof DexieError || domError instanceof TypeError || domError instanceof SyntaxError || !domError.name || !exceptionMap[domError.name]) return domError;
	       var rv = new exceptionMap[domError.name](message || domError.message, domError);
	       if ("stack" in domError) {
	           // Derive stack from inner exception if it has a stack
	           setProp(rv, "stack", { get: function () {
	                   return this.inner.stack;
	               } });
	       }
	       return rv;
	   }
	
	   var fullNameExceptions = errorList.reduce(function (obj, name) {
	       if (["Syntax", "Type", "Range"].indexOf(name) === -1) obj[name + "Error"] = exceptions[name];
	       return obj;
	   }, {});
	
	   fullNameExceptions.ModifyError = ModifyError;
	   fullNameExceptions.DexieError = DexieError;
	   fullNameExceptions.BulkError = BulkError;
	
	   function Events(ctx) {
	       var evs = {};
	       var rv = function (eventName, subscriber) {
	           if (subscriber) {
	               // Subscribe. If additional arguments than just the subscriber was provided, forward them as well.
	               var i = arguments.length,
	                   args = new Array(i - 1);
	               while (--i) {
	                   args[i - 1] = arguments[i];
	               }evs[eventName].subscribe.apply(null, args);
	               return ctx;
	           } else if (typeof eventName === 'string') {
	               // Return interface allowing to fire or unsubscribe from event
	               return evs[eventName];
	           }
	       };
	       rv.addEventType = add;
	
	       for (var i = 1, l = arguments.length; i < l; ++i) {
	           add(arguments[i]);
	       }
	
	       return rv;
	
	       function add(eventName, chainFunction, defaultFunction) {
	           if (typeof eventName === 'object') return addConfiguredEvents(eventName);
	           if (!chainFunction) chainFunction = reverseStoppableEventChain;
	           if (!defaultFunction) defaultFunction = nop;
	
	           var context = {
	               subscribers: [],
	               fire: defaultFunction,
	               subscribe: function (cb) {
	                   if (context.subscribers.indexOf(cb) === -1) {
	                       context.subscribers.push(cb);
	                       context.fire = chainFunction(context.fire, cb);
	                   }
	               },
	               unsubscribe: function (cb) {
	                   context.subscribers = context.subscribers.filter(function (fn) {
	                       return fn !== cb;
	                   });
	                   context.fire = context.subscribers.reduce(chainFunction, defaultFunction);
	               }
	           };
	           evs[eventName] = rv[eventName] = context;
	           return context;
	       }
	
	       function addConfiguredEvents(cfg) {
	           // events(this, {reading: [functionChain, nop]});
	           keys(cfg).forEach(function (eventName) {
	               var args = cfg[eventName];
	               if (isArray(args)) {
	                   add(eventName, cfg[eventName][0], cfg[eventName][1]);
	               } else if (args === 'asap') {
	                   // Rather than approaching event subscription using a functional approach, we here do it in a for-loop where subscriber is executed in its own stack
	                   // enabling that any exception that occur wont disturb the initiator and also not nescessary be catched and forgotten.
	                   var context = add(eventName, mirror, function fire() {
	                       // Optimazation-safe cloning of arguments into args.
	                       var i = arguments.length,
	                           args = new Array(i);
	                       while (i--) {
	                           args[i] = arguments[i];
	                       } // All each subscriber:
	                       context.subscribers.forEach(function (fn) {
	                           asap(function fireEvent() {
	                               fn.apply(null, args);
	                           });
	                       });
	                   });
	               } else throw new exceptions.InvalidArgument("Invalid event config");
	           });
	       }
	   }
	
	   //
	   // Promise Class for Dexie library
	   //
	   // I started out writing this Promise class by copying promise-light (https://github.com/taylorhakes/promise-light) by
	   // https://github.com/taylorhakes - an A+ and ECMASCRIPT 6 compliant Promise implementation.
	   //
	   // Modifications needed to be done to support indexedDB because it wont accept setTimeout()
	   // (See discussion: https://github.com/promises-aplus/promises-spec/issues/45) .
	   // This topic was also discussed in the following thread: https://github.com/promises-aplus/promises-spec/issues/45
	   //
	   // This implementation will not use setTimeout or setImmediate when it's not needed. The behavior is 100% Promise/A+ compliant since
	   // the caller of new Promise() can be certain that the promise wont be triggered the lines after constructing the promise.
	   //
	   // In previous versions this was fixed by not calling setTimeout when knowing that the resolve() or reject() came from another
	   // tick. In Dexie v1.4.0, I've rewritten the Promise class entirely. Just some fragments of promise-light is left. I use
	   // another strategy now that simplifies everything a lot: to always execute callbacks in a new tick, but have an own microTick
	   // engine that is used instead of setImmediate() or setTimeout().
	   // Promise class has also been optimized a lot with inspiration from bluebird - to avoid closures as much as possible.
	   // Also with inspiration from bluebird, asyncronic stacks in debug mode.
	   //
	   // Specific non-standard features of this Promise class:
	   // * Async static context support (Promise.PSD)
	   // * Promise.follow() method built upon PSD, that allows user to track all promises created from current stack frame
	   //   and below + all promises that those promises creates or awaits.
	   // * Detect any unhandled promise in a PSD-scope (PSD.onunhandled).
	   //
	   // David Fahlander, https://github.com/dfahlander
	   //
	
	   // Just a pointer that only this module knows about.
	   // Used in Promise constructor to emulate a private constructor.
	   var INTERNAL = {};
	
	   // Async stacks (long stacks) must not grow infinitely.
	   var LONG_STACKS_CLIP_LIMIT = 100;
	   var MAX_LONG_STACKS = 20;
	   var stack_being_generated = false;
	   /* The default "nextTick" function used only for the very first promise in a promise chain.
	      As soon as then promise is resolved or rejected, all next tasks will be executed in micro ticks
	      emulated in this module. For indexedDB compatibility, this means that every method needs to 
	      execute at least one promise before doing an indexedDB operation. Dexie will always call 
	      db.ready().then() for every operation to make sure the indexedDB event is started in an
	      emulated micro tick.
	   */
	   var schedulePhysicalTick = typeof setImmediate === 'undefined' ?
	   // No support for setImmediate. No worry, setTimeout is only called
	   // once time. Every tick that follows will be our emulated micro tick.
	   // Could have uses setTimeout.bind(null, 0, physicalTick) if it wasnt for that FF13 and below has a bug
	   function () {
	       setTimeout(physicalTick, 0);
	   } :
	   // setImmediate supported. Modern platform. Also supports Function.bind().
	   setImmediate.bind(null, physicalTick);
	
	   // Confifurable through Promise.scheduler.
	   // Don't export because it would be unsafe to let unknown
	   // code call it unless they do try..catch within their callback.
	   // This function can be retrieved through getter of Promise.scheduler though,
	   // but users must not do Promise.scheduler (myFuncThatThrows exception)!
	   var asap$1 = function (callback, args) {
	       microtickQueue.push([callback, args]);
	       if (needsNewPhysicalTick) {
	           schedulePhysicalTick();
	           needsNewPhysicalTick = false;
	       }
	   };
	
	   var isOutsideMicroTick = true;
	   var needsNewPhysicalTick = true;
	   var unhandledErrors = [];
	   var rejectingErrors = [];
	   var currentFulfiller = null;
	   var rejectionMapper = mirror;
	   // Remove in next major when removing error mapping of DOMErrors and DOMExceptions
	
	   var globalPSD = {
	       global: true,
	       ref: 0,
	       unhandleds: [],
	       onunhandled: globalError,
	       //env: null, // Will be set whenever leaving a scope using wrappers.snapshot()
	       finalize: function () {
	           this.unhandleds.forEach(function (uh) {
	               try {
	                   globalError(uh[0], uh[1]);
	               } catch (e) {}
	           });
	       }
	   };
	
	   var PSD = globalPSD;
	
	   var microtickQueue = []; // Callbacks to call in this or next physical tick.
	   var numScheduledCalls = 0; // Number of listener-calls left to do in this physical tick.
	   var tickFinalizers = []; // Finalizers to call when there are no more async calls scheduled within current physical tick.
	
	   // Wrappers are not being used yet. Their framework is functioning and can be used
	   // to replace environment during a PSD scope (a.k.a. 'zone').
	   /* **KEEP** export var wrappers = (() => {
	       var wrappers = [];
	
	       return {
	           snapshot: () => {
	               var i = wrappers.length,
	                   result = new Array(i);
	               while (i--) result[i] = wrappers[i].snapshot();
	               return result;
	           },
	           restore: values => {
	               var i = wrappers.length;
	               while (i--) wrappers[i].restore(values[i]);
	           },
	           wrap: () => wrappers.map(w => w.wrap()),
	           add: wrapper => {
	               wrappers.push(wrapper);
	           }
	       };
	   })();
	   */
	
	   function Promise(fn) {
	       if (typeof this !== 'object') throw new TypeError('Promises must be constructed via new');
	       this._listeners = [];
	       this.onuncatched = nop; // Deprecate in next major. Not needed. Better to use global error handler.
	
	       // A library may set `promise._lib = true;` after promise is created to make resolve() or reject()
	       // execute the microtask engine implicitely within the call to resolve() or reject().
	       // To remain A+ compliant, a library must only set `_lib=true` if it can guarantee that the stack
	       // only contains library code when calling resolve() or reject().
	       // RULE OF THUMB: ONLY set _lib = true for promises explicitely resolving/rejecting directly from
	       // global scope (event handler, timer etc)!
	       this._lib = false;
	       // Current async scope
	       var psd = this._PSD = PSD;
	
	       if (debug) {
	           this._stackHolder = getErrorWithStack();
	           this._prev = null;
	           this._numPrev = 0; // Number of previous promises (for long stacks)
	           linkToPreviousPromise(this, currentFulfiller);
	       }
	
	       if (typeof fn !== 'function') {
	           if (fn !== INTERNAL) throw new TypeError('Not a function');
	           // Private constructor (INTERNAL, state, value).
	           // Used internally by Promise.resolve() and Promise.reject().
	           this._state = arguments[1];
	           this._value = arguments[2];
	           if (this._state === false) handleRejection(this, this._value); // Map error, set stack and addPossiblyUnhandledError().
	           return;
	       }
	
	       this._state = null; // null (=pending), false (=rejected) or true (=resolved)
	       this._value = null; // error or result
	       ++psd.ref; // Refcounting current scope
	       executePromiseTask(this, fn);
	   }
	
	   props(Promise.prototype, {
	
	       then: function (onFulfilled, onRejected) {
	           var _this = this;
	
	           var rv = new Promise(function (resolve, reject) {
	               propagateToListener(_this, new Listener(onFulfilled, onRejected, resolve, reject));
	           });
	           debug && (!this._prev || this._state === null) && linkToPreviousPromise(rv, this);
	           return rv;
	       },
	
	       _then: function (onFulfilled, onRejected) {
	           // A little tinier version of then() that don't have to create a resulting promise.
	           propagateToListener(this, new Listener(null, null, onFulfilled, onRejected));
	       },
	
	       catch: function (onRejected) {
	           if (arguments.length === 1) return this.then(null, onRejected);
	           // First argument is the Error type to catch
	           var type = arguments[0],
	               handler = arguments[1];
	           return typeof type === 'function' ? this.then(null, function (err) {
	               return(
	                   // Catching errors by its constructor type (similar to java / c++ / c#)
	                   // Sample: promise.catch(TypeError, function (e) { ... });
	                   err instanceof type ? handler(err) : PromiseReject(err)
	               );
	           }) : this.then(null, function (err) {
	               return(
	                   // Catching errors by the error.name property. Makes sense for indexedDB where error type
	                   // is always DOMError but where e.name tells the actual error type.
	                   // Sample: promise.catch('ConstraintError', function (e) { ... });
	                   err && err.name === type ? handler(err) : PromiseReject(err)
	               );
	           });
	       },
	
	       finally: function (onFinally) {
	           return this.then(function (value) {
	               onFinally();
	               return value;
	           }, function (err) {
	               onFinally();
	               return PromiseReject(err);
	           });
	       },
	
	       // Deprecate in next major. Needed only for db.on.error.
	       uncaught: function (uncaughtHandler) {
	           var _this2 = this;
	
	           // Be backward compatible and use "onuncatched" as the event name on this.
	           // Handle multiple subscribers through reverseStoppableEventChain(). If a handler returns `false`, bubbling stops.
	           this.onuncatched = reverseStoppableEventChain(this.onuncatched, uncaughtHandler);
	           // In case caller does this on an already rejected promise, assume caller wants to point out the error to this promise and not
	           // a previous promise. Reason: the prevous promise may lack onuncatched handler.
	           if (this._state === false && unhandledErrors.indexOf(this) === -1) {
	               // Replace unhandled error's destinaion promise with this one!
	               unhandledErrors.some(function (p, i, l) {
	                   return p._value === _this2._value && (l[i] = _this2);
	               });
	               // Actually we do this shit because we need to support db.on.error() correctly during db.open(). If we deprecate db.on.error, we could
	               // take away this piece of code as well as the onuncatched and uncaught() method.
	           }
	           return this;
	       },
	
	       stack: {
	           get: function () {
	               if (this._stack) return this._stack;
	               try {
	                   stack_being_generated = true;
	                   var stacks = getStack(this, [], MAX_LONG_STACKS);
	                   var stack = stacks.join("\nFrom previous: ");
	                   if (this._state !== null) this._stack = stack; // Stack may be updated on reject.
	                   return stack;
	               } finally {
	                   stack_being_generated = false;
	               }
	           }
	       }
	   });
	
	   function Listener(onFulfilled, onRejected, resolve, reject) {
	       this.onFulfilled = typeof onFulfilled === 'function' ? onFulfilled : null;
	       this.onRejected = typeof onRejected === 'function' ? onRejected : null;
	       this.resolve = resolve;
	       this.reject = reject;
	       this.psd = PSD;
	   }
	
	   // Promise Static Properties
	   props(Promise, {
	       all: function () {
	           var values = getArrayOf.apply(null, arguments); // Supports iterables, implicit arguments and array-like.
	           return new Promise(function (resolve, reject) {
	               if (values.length === 0) resolve([]);
	               var remaining = values.length;
	               values.forEach(function (a, i) {
	                   return Promise.resolve(a).then(function (x) {
	                       values[i] = x;
	                       if (! --remaining) resolve(values);
	                   }, reject);
	               });
	           });
	       },
	
	       resolve: function (value) {
	           if (value && typeof value.then === 'function') return value;
	           return new Promise(INTERNAL, true, value);
	       },
	
	       reject: PromiseReject,
	
	       race: function () {
	           var values = getArrayOf.apply(null, arguments);
	           return new Promise(function (resolve, reject) {
	               values.map(function (value) {
	                   return Promise.resolve(value).then(resolve, reject);
	               });
	           });
	       },
	
	       PSD: {
	           get: function () {
	               return PSD;
	           },
	           set: function (value) {
	               return PSD = value;
	           }
	       },
	
	       newPSD: newScope,
	
	       usePSD: usePSD,
	
	       scheduler: {
	           get: function () {
	               return asap$1;
	           },
	           set: function (value) {
	               asap$1 = value;
	           }
	       },
	
	       rejectionMapper: {
	           get: function () {
	               return rejectionMapper;
	           },
	           set: function (value) {
	               rejectionMapper = value;
	           } // Map reject failures
	       },
	
	       follow: function (fn) {
	           return new Promise(function (resolve, reject) {
	               return newScope(function (resolve, reject) {
	                   var psd = PSD;
	                   psd.unhandleds = []; // For unhandled standard- or 3rd party Promises. Checked at psd.finalize()
	                   psd.onunhandled = reject; // Triggered directly on unhandled promises of this library.
	                   psd.finalize = callBoth(function () {
	                       var _this3 = this;
	
	                       // Unhandled standard or 3rd part promises are put in PSD.unhandleds and
	                       // examined upon scope completion while unhandled rejections in this Promise
	                       // will trigger directly through psd.onunhandled
	                       run_at_end_of_this_or_next_physical_tick(function () {
	                           _this3.unhandleds.length === 0 ? resolve() : reject(_this3.unhandleds[0]);
	                       });
	                   }, psd.finalize);
	                   fn();
	               }, resolve, reject);
	           });
	       },
	
	       on: Events(null, { "error": [reverseStoppableEventChain, defaultErrorHandler] // Default to defaultErrorHandler
	       })
	
	   });
	
	   /**
	   * Take a potentially misbehaving resolver function and make sure
	   * onFulfilled and onRejected are only called once.
	   *
	   * Makes no guarantees about asynchrony.
	   */
	   function executePromiseTask(promise, fn) {
	       // Promise Resolution Procedure:
	       // https://github.com/promises-aplus/promises-spec#the-promise-resolution-procedure
	       try {
	           fn(function (value) {
	               if (promise._state !== null) return;
	               if (value === promise) throw new TypeError('A promise cannot be resolved with itself.');
	               var shouldExecuteTick = promise._lib && beginMicroTickScope();
	               if (value && typeof value.then === 'function') {
	                   executePromiseTask(promise, function (resolve, reject) {
	                       value instanceof Promise ? value._then(resolve, reject) : value.then(resolve, reject);
	                   });
	               } else {
	                   promise._state = true;
	                   promise._value = value;
	                   propagateAllListeners(promise);
	               }
	               if (shouldExecuteTick) endMicroTickScope();
	           }, handleRejection.bind(null, promise)); // If Function.bind is not supported. Exception is handled in catch below
	       } catch (ex) {
	           handleRejection(promise, ex);
	       }
	   }
	
	   function handleRejection(promise, reason) {
	       rejectingErrors.push(reason);
	       if (promise._state !== null) return;
	       var shouldExecuteTick = promise._lib && beginMicroTickScope();
	       reason = rejectionMapper(reason);
	       promise._state = false;
	       promise._value = reason;
	       debug && reason !== null && typeof reason === 'object' && !reason._promise && tryCatch(function () {
	           var origProp = getPropertyDescriptor(reason, "stack");
	           reason._promise = promise;
	           setProp(reason, "stack", {
	               get: function () {
	                   return stack_being_generated ? origProp && (origProp.get ? origProp.get.apply(reason) : origProp.value) : promise.stack;
	               }
	           });
	       });
	       // Add the failure to a list of possibly uncaught errors
	       addPossiblyUnhandledError(promise);
	       propagateAllListeners(promise);
	       if (shouldExecuteTick) endMicroTickScope();
	   }
	
	   function propagateAllListeners(promise) {
	       //debug && linkToPreviousPromise(promise);
	       var listeners = promise._listeners;
	       promise._listeners = [];
	       for (var i = 0, len = listeners.length; i < len; ++i) {
	           propagateToListener(promise, listeners[i]);
	       }
	       var psd = promise._PSD;
	       --psd.ref || psd.finalize(); // if psd.ref reaches zero, call psd.finalize();
	       if (numScheduledCalls === 0) {
	           // If numScheduledCalls is 0, it means that our stack is not in a callback of a scheduled call,
	           // and that no deferreds where listening to this rejection or success.
	           // Since there is a risk that our stack can contain application code that may
	           // do stuff after this code is finished that may generate new calls, we cannot
	           // call finalizers here.
	           ++numScheduledCalls;
	           asap$1(function () {
	               if (--numScheduledCalls === 0) finalizePhysicalTick(); // Will detect unhandled errors
	           }, []);
	       }
	   }
	
	   function propagateToListener(promise, listener) {
	       if (promise._state === null) {
	           promise._listeners.push(listener);
	           return;
	       }
	
	       var cb = promise._state ? listener.onFulfilled : listener.onRejected;
	       if (cb === null) {
	           // This Listener doesnt have a listener for the event being triggered (onFulfilled or onReject) so lets forward the event to any eventual listeners on the Promise instance returned by then() or catch()
	           return (promise._state ? listener.resolve : listener.reject)(promise._value);
	       }
	       var psd = listener.psd;
	       ++psd.ref;
	       ++numScheduledCalls;
	       asap$1(callListener, [cb, promise, listener]);
	   }
	
	   function callListener(cb, promise, listener) {
	       var outerScope = PSD;
	       var psd = listener.psd;
	       try {
	           if (psd !== outerScope) {
	               // **KEEP** outerScope.env = wrappers.snapshot(); // Snapshot outerScope's environment.
	               PSD = psd;
	               // **KEEP** wrappers.restore(psd.env); // Restore PSD's environment.
	           }
	
	           // Set static variable currentFulfiller to the promise that is being fullfilled,
	           // so that we connect the chain of promises (for long stacks support)
	           currentFulfiller = promise;
	
	           // Call callback and resolve our listener with it's return value.
	           var value = promise._value,
	               ret;
	           if (promise._state) {
	               ret = cb(value);
	           } else {
	               if (rejectingErrors.length) rejectingErrors = [];
	               ret = cb(value);
	               if (rejectingErrors.indexOf(value) === -1) markErrorAsHandled(promise); // Callback didnt do Promise.reject(err) nor reject(err) onto another promise.
	           }
	           listener.resolve(ret);
	       } catch (e) {
	           // Exception thrown in callback. Reject our listener.
	           listener.reject(e);
	       } finally {
	           // Restore PSD, env and currentFulfiller.
	           if (psd !== outerScope) {
	               PSD = outerScope;
	               // **KEEP** wrappers.restore(outerScope.env); // Restore outerScope's environment
	           }
	           currentFulfiller = null;
	           if (--numScheduledCalls === 0) finalizePhysicalTick();
	           --psd.ref || psd.finalize();
	       }
	   }
	
	   function getStack(promise, stacks, limit) {
	       if (stacks.length === limit) return stacks;
	       var stack = "";
	       if (promise._state === false) {
	           var failure = promise._value,
	               errorName,
	               message;
	
	           if (failure != null) {
	               errorName = failure.name || "Error";
	               message = failure.message || failure;
	               stack = prettyStack(failure, 0);
	           } else {
	               errorName = failure; // If error is undefined or null, show that.
	               message = "";
	           }
	           stacks.push(errorName + (message ? ": " + message : "") + stack);
	       }
	       if (debug) {
	           stack = prettyStack(promise._stackHolder, 2);
	           if (stack && stacks.indexOf(stack) === -1) stacks.push(stack);
	           if (promise._prev) getStack(promise._prev, stacks, limit);
	       }
	       return stacks;
	   }
	
	   function linkToPreviousPromise(promise, prev) {
	       // Support long stacks by linking to previous completed promise.
	       var numPrev = prev ? prev._numPrev + 1 : 0;
	       if (numPrev < LONG_STACKS_CLIP_LIMIT) {
	           // Prohibit infinite Promise loops to get an infinite long memory consuming "tail".
	           promise._prev = prev;
	           promise._numPrev = numPrev;
	       }
	   }
	
	   /* The callback to schedule with setImmediate() or setTimeout().
	      It runs a virtual microtick and executes any callback registered in microtickQueue.
	    */
	   function physicalTick() {
	       beginMicroTickScope() && endMicroTickScope();
	   }
	
	   function beginMicroTickScope() {
	       var wasRootExec = isOutsideMicroTick;
	       isOutsideMicroTick = false;
	       needsNewPhysicalTick = false;
	       return wasRootExec;
	   }
	
	   /* Executes micro-ticks without doing try..catch.
	      This can be possible because we only use this internally and
	      the registered functions are exception-safe (they do try..catch
	      internally before calling any external method). If registering
	      functions in the microtickQueue that are not exception-safe, this
	      would destroy the framework and make it instable. So we don't export
	      our asap method.
	   */
	   function endMicroTickScope() {
	       var callbacks, i, l;
	       do {
	           while (microtickQueue.length > 0) {
	               callbacks = microtickQueue;
	               microtickQueue = [];
	               l = callbacks.length;
	               for (i = 0; i < l; ++i) {
	                   var item = callbacks[i];
	                   item[0].apply(null, item[1]);
	               }
	           }
	       } while (microtickQueue.length > 0);
	       isOutsideMicroTick = true;
	       needsNewPhysicalTick = true;
	   }
	
	   function finalizePhysicalTick() {
	       var unhandledErrs = unhandledErrors;
	       unhandledErrors = [];
	       unhandledErrs.forEach(function (p) {
	           p._PSD.onunhandled.call(null, p._value, p);
	       });
	       var finalizers = tickFinalizers.slice(0); // Clone first because finalizer may remove itself from list.
	       var i = finalizers.length;
	       while (i) {
	           finalizers[--i]();
	       }
	   }
	
	   function run_at_end_of_this_or_next_physical_tick(fn) {
	       function finalizer() {
	           fn();
	           tickFinalizers.splice(tickFinalizers.indexOf(finalizer), 1);
	       }
	       tickFinalizers.push(finalizer);
	       ++numScheduledCalls;
	       asap$1(function () {
	           if (--numScheduledCalls === 0) finalizePhysicalTick();
	       }, []);
	   }
	
	   function addPossiblyUnhandledError(promise) {
	       // Only add to unhandledErrors if not already there. The first one to add to this list
	       // will be upon the first rejection so that the root cause (first promise in the
	       // rejection chain) is the one listed.
	       if (!unhandledErrors.some(function (p) {
	           return p._value === promise._value;
	       })) unhandledErrors.push(promise);
	   }
	
	   function markErrorAsHandled(promise) {
	       // Called when a reject handled is actually being called.
	       // Search in unhandledErrors for any promise whos _value is this promise_value (list
	       // contains only rejected promises, and only one item per error)
	       var i = unhandledErrors.length;
	       while (i) {
	           if (unhandledErrors[--i]._value === promise._value) {
	               // Found a promise that failed with this same error object pointer,
	               // Remove that since there is a listener that actually takes care of it.
	               unhandledErrors.splice(i, 1);
	               return;
	           }
	       }
	   }
	
	   // By default, log uncaught errors to the console
	   function defaultErrorHandler(e) {
	       console.warn('Unhandled rejection: ' + (e.stack || e));
	   }
	
	   function PromiseReject(reason) {
	       return new Promise(INTERNAL, false, reason);
	   }
	
	   function wrap(fn, errorCatcher) {
	       var psd = PSD;
	       return function () {
	           var wasRootExec = beginMicroTickScope(),
	               outerScope = PSD;
	
	           try {
	               if (outerScope !== psd) {
	                   // **KEEP** outerScope.env = wrappers.snapshot(); // Snapshot outerScope's environment
	                   PSD = psd;
	                   // **KEEP** wrappers.restore(psd.env); // Restore PSD's environment.
	               }
	               return fn.apply(this, arguments);
	           } catch (e) {
	               errorCatcher && errorCatcher(e);
	           } finally {
	               if (outerScope !== psd) {
	                   PSD = outerScope;
	                   // **KEEP** wrappers.restore(outerScope.env); // Restore outerScope's environment
	               }
	               if (wasRootExec) endMicroTickScope();
	           }
	       };
	   }
	
	   function newScope(fn, a1, a2, a3) {
	       var parent = PSD,
	           psd = Object.create(parent);
	       psd.parent = parent;
	       psd.ref = 0;
	       psd.global = false;
	       // **KEEP** psd.env = wrappers.wrap(psd);
	
	       // unhandleds and onunhandled should not be specifically set here.
	       // Leave them on parent prototype.
	       // unhandleds.push(err) will push to parent's prototype
	       // onunhandled() will call parents onunhandled (with this scope's this-pointer though!)
	       ++parent.ref;
	       psd.finalize = function () {
	           --this.parent.ref || this.parent.finalize();
	       };
	       var rv = usePSD(psd, fn, a1, a2, a3);
	       if (psd.ref === 0) psd.finalize();
	       return rv;
	   }
	
	   function usePSD(psd, fn, a1, a2, a3) {
	       var outerScope = PSD;
	       try {
	           if (psd !== outerScope) {
	               // **KEEP** outerScope.env = wrappers.snapshot(); // snapshot outerScope's environment.
	               PSD = psd;
	               // **KEEP** wrappers.restore(psd.env); // Restore PSD's environment.
	           }
	           return fn(a1, a2, a3);
	       } finally {
	           if (psd !== outerScope) {
	               PSD = outerScope;
	               // **KEEP** wrappers.restore(outerScope.env); // Restore outerScope's environment.
	           }
	       }
	   }
	
	   function globalError(err, promise) {
	       var rv;
	       try {
	           rv = promise.onuncatched(err);
	       } catch (e) {}
	       if (rv !== false) try {
	           Promise.on.error.fire(err, promise); // TODO: Deprecated and use same global handler as bluebird.
	       } catch (e) {}
	   }
	
	   /* **KEEP** 
	
	   export function wrapPromise(PromiseClass) {
	       var proto = PromiseClass.prototype;
	       var origThen = proto.then;
	       
	       wrappers.add({
	           snapshot: () => proto.then,
	           restore: value => {proto.then = value;},
	           wrap: () => patchedThen
	       });
	
	       function patchedThen (onFulfilled, onRejected) {
	           var promise = this;
	           var onFulfilledProxy = wrap(function(value){
	               var rv = value;
	               if (onFulfilled) {
	                   rv = onFulfilled(rv);
	                   if (rv && typeof rv.then === 'function') rv.then(); // Intercept that promise as well.
	               }
	               --PSD.ref || PSD.finalize();
	               return rv;
	           });
	           var onRejectedProxy = wrap(function(err){
	               promise._$err = err;
	               var unhandleds = PSD.unhandleds;
	               var idx = unhandleds.length,
	                   rv;
	               while (idx--) if (unhandleds[idx]._$err === err) break;
	               if (onRejected) {
	                   if (idx !== -1) unhandleds.splice(idx, 1); // Mark as handled.
	                   rv = onRejected(err);
	                   if (rv && typeof rv.then === 'function') rv.then(); // Intercept that promise as well.
	               } else {
	                   if (idx === -1) unhandleds.push(promise);
	                   rv = PromiseClass.reject(err);
	                   rv._$nointercept = true; // Prohibit eternal loop.
	               }
	               --PSD.ref || PSD.finalize();
	               return rv;
	           });
	           
	           if (this._$nointercept) return origThen.apply(this, arguments);
	           ++PSD.ref;
	           return origThen.call(this, onFulfilledProxy, onRejectedProxy);
	       }
	   }
	
	   // Global Promise wrapper
	   if (_global.Promise) wrapPromise(_global.Promise);
	
	   */
	
	   doFakeAutoComplete(function () {
	       // Simplify the job for VS Intellisense. This piece of code is one of the keys to the new marvellous intellisense support in Dexie.
	       asap$1 = function (fn, args) {
	           setTimeout(function () {
	               fn.apply(null, args);
	           }, 0);
	       };
	   });
	
	   var DEXIE_VERSION = '1.4.1';
	   var maxString = String.fromCharCode(65535);
	   var maxKey = function () {
	       try {
	           IDBKeyRange.only([[]]);return [[]];
	       } catch (e) {
	           return maxString;
	       }
	   }();
	   var INVALID_KEY_ARGUMENT = "Invalid key provided. Keys must be of type string, number, Date or Array<string | number | Date>.";
	   var STRING_EXPECTED = "String expected.";
	   var connections = [];
	   var isIEOrEdge = typeof navigator !== 'undefined' && /(MSIE|Trident|Edge)/.test(navigator.userAgent);
	   var hasIEDeleteObjectStoreBug = isIEOrEdge;
	   var hangsOnDeleteLargeKeyRange = isIEOrEdge;
	   var dexieStackFrameFilter = function (frame) {
	       return !/(dexie\.js|dexie\.min\.js)/.test(frame);
	   };
	   setDebug(debug, dexieStackFrameFilter);
	
	   function Dexie(dbName, options) {
	       /// <param name="options" type="Object" optional="true">Specify only if you wich to control which addons that should run on this instance</param>
	       var deps = Dexie.dependencies;
	       var opts = extend({
	           // Default Options
	           addons: Dexie.addons, // Pick statically registered addons by default
	           autoOpen: true, // Don't require db.open() explicitely.
	           indexedDB: deps.indexedDB, // Backend IndexedDB api. Default to IDBShim or browser env.
	           IDBKeyRange: deps.IDBKeyRange // Backend IDBKeyRange api. Default to IDBShim or browser env.
	       }, options);
	       var addons = opts.addons,
	           autoOpen = opts.autoOpen,
	           indexedDB = opts.indexedDB,
	           IDBKeyRange = opts.IDBKeyRange;
	
	       var globalSchema = this._dbSchema = {};
	       var versions = [];
	       var dbStoreNames = [];
	       var allTables = {};
	       ///<var type="IDBDatabase" />
	       var idbdb = null; // Instance of IDBDatabase
	       var dbOpenError = null;
	       var isBeingOpened = false;
	       var openComplete = false;
	       var READONLY = "readonly",
	           READWRITE = "readwrite";
	       var db = this;
	       var dbReadyResolve,
	           dbReadyPromise = new Promise(function (resolve) {
	           dbReadyResolve = resolve;
	       }),
	           cancelOpen,
	           openCanceller = new Promise(function (_, reject) {
	           cancelOpen = reject;
	       });
	       var autoSchema = true;
	       var hasNativeGetDatabaseNames = !!getNativeGetDatabaseNamesFn(indexedDB),
	           hasGetAll;
	
	       function init() {
	           // Default subscribers to "versionchange" and "blocked".
	           // Can be overridden by custom handlers. If custom handlers return false, these default
	           // behaviours will be prevented.
	           db.on("versionchange", function (ev) {
	               // Default behavior for versionchange event is to close database connection.
	               // Caller can override this behavior by doing db.on("versionchange", function(){ return false; });
	               // Let's not block the other window from making it's delete() or open() call.
	               // NOTE! This event is never fired in IE,Edge or Safari.
	               if (ev.newVersion > 0) console.warn('Another connection wants to upgrade database \'' + db.name + '\'. Closing db now to resume the upgrade.');else console.warn('Another connection wants to delete database \'' + db.name + '\'. Closing db now to resume the delete request.');
	               db.close();
	               // In many web applications, it would be recommended to force window.reload()
	               // when this event occurs. To do that, subscribe to the versionchange event
	               // and call window.location.reload(true) if ev.newVersion > 0 (not a deletion)
	               // The reason for this is that your current web app obviously has old schema code that needs
	               // to be updated. Another window got a newer version of the app and needs to upgrade DB but
	               // your window is blocking it unless we close it here.
	           });
	           db.on("blocked", function (ev) {
	               if (!ev.newVersion || ev.newVersion < ev.oldVersion) console.warn('Dexie.delete(\'' + db.name + '\') was blocked');else console.warn('Upgrade \'' + db.name + '\' blocked by other connection holding version ' + ev.oldVersion / 10);
	           });
	       }
	
	       //
	       //
	       //
	       // ------------------------- Versioning Framework---------------------------
	       //
	       //
	       //
	
	       this.version = function (versionNumber) {
	           /// <param name="versionNumber" type="Number"></param>
	           /// <returns type="Version"></returns>
	           if (idbdb || isBeingOpened) throw new exceptions.Schema("Cannot add version when database is open");
	           this.verno = Math.max(this.verno, versionNumber);
	           var versionInstance = versions.filter(function (v) {
	               return v._cfg.version === versionNumber;
	           })[0];
	           if (versionInstance) return versionInstance;
	           versionInstance = new Version(versionNumber);
	           versions.push(versionInstance);
	           versions.sort(lowerVersionFirst);
	           return versionInstance;
	       };
	
	       function Version(versionNumber) {
	           this._cfg = {
	               version: versionNumber,
	               storesSource: null,
	               dbschema: {},
	               tables: {},
	               contentUpgrade: null
	           };
	           this.stores({}); // Derive earlier schemas by default.
	       }
	
	       extend(Version.prototype, {
	           stores: function (stores) {
	               /// <summary>
	               ///   Defines the schema for a particular version
	               /// </summary>
	               /// <param name="stores" type="Object">
	               /// Example: <br/>
	               ///   {users: "id++,first,last,&amp;username,*email", <br/>
	               ///   passwords: "id++,&amp;username"}<br/>
	               /// <br/>
	               /// Syntax: {Table: "[primaryKey][++],[&amp;][*]index1,[&amp;][*]index2,..."}<br/><br/>
	               /// Special characters:<br/>
	               ///  "&amp;"  means unique key, <br/>
	               ///  "*"  means value is multiEntry, <br/>
	               ///  "++" means auto-increment and only applicable for primary key <br/>
	               /// </param>
	               this._cfg.storesSource = this._cfg.storesSource ? extend(this._cfg.storesSource, stores) : stores;
	
	               // Derive stores from earlier versions if they are not explicitely specified as null or a new syntax.
	               var storesSpec = {};
	               versions.forEach(function (version) {
	                   // 'versions' is always sorted by lowest version first.
	                   extend(storesSpec, version._cfg.storesSource);
	               });
	
	               var dbschema = this._cfg.dbschema = {};
	               this._parseStoresSpec(storesSpec, dbschema);
	               // Update the latest schema to this version
	               // Update API
	               globalSchema = db._dbSchema = dbschema;
	               removeTablesApi([allTables, db, Transaction.prototype]);
	               setApiOnPlace([allTables, db, Transaction.prototype, this._cfg.tables], keys(dbschema), READWRITE, dbschema);
	               dbStoreNames = keys(dbschema);
	               return this;
	           },
	           upgrade: function (upgradeFunction) {
	               /// <param name="upgradeFunction" optional="true">Function that performs upgrading actions.</param>
	               var self = this;
	               fakeAutoComplete(function () {
	                   upgradeFunction(db._createTransaction(READWRITE, keys(self._cfg.dbschema), self._cfg.dbschema)); // BUGBUG: No code completion for prev version's tables wont appear.
	               });
	               this._cfg.contentUpgrade = upgradeFunction;
	               return this;
	           },
	           _parseStoresSpec: function (stores, outSchema) {
	               keys(stores).forEach(function (tableName) {
	                   if (stores[tableName] !== null) {
	                       var instanceTemplate = {};
	                       var indexes = parseIndexSyntax(stores[tableName]);
	                       var primKey = indexes.shift();
	                       if (primKey.multi) throw new exceptions.Schema("Primary key cannot be multi-valued");
	                       if (primKey.keyPath) setByKeyPath(instanceTemplate, primKey.keyPath, primKey.auto ? 0 : primKey.keyPath);
	                       indexes.forEach(function (idx) {
	                           if (idx.auto) throw new exceptions.Schema("Only primary key can be marked as autoIncrement (++)");
	                           if (!idx.keyPath) throw new exceptions.Schema("Index must have a name and cannot be an empty string");
	                           setByKeyPath(instanceTemplate, idx.keyPath, idx.compound ? idx.keyPath.map(function () {
	                               return "";
	                           }) : "");
	                       });
	                       outSchema[tableName] = new TableSchema(tableName, primKey, indexes, instanceTemplate);
	                   }
	               });
	           }
	       });
	
	       function runUpgraders(oldVersion, idbtrans, reject) {
	           var trans = db._createTransaction(READWRITE, dbStoreNames, globalSchema);
	           trans.create(idbtrans);
	           trans._completion.catch(reject);
	           var rejectTransaction = trans._reject.bind(trans);
	           newScope(function () {
	               PSD.trans = trans;
	               if (oldVersion === 0) {
	                   // Create tables:
	                   keys(globalSchema).forEach(function (tableName) {
	                       createTable(idbtrans, tableName, globalSchema[tableName].primKey, globalSchema[tableName].indexes);
	                   });
	                   Promise.follow(function () {
	                       return db.on.populate.fire(trans);
	                   }).catch(rejectTransaction);
	               } else updateTablesAndIndexes(oldVersion, trans, idbtrans).catch(rejectTransaction);
	           });
	       }
	
	       function updateTablesAndIndexes(oldVersion, trans, idbtrans) {
	           // Upgrade version to version, step-by-step from oldest to newest version.
	           // Each transaction object will contain the table set that was current in that version (but also not-yet-deleted tables from its previous version)
	           var queue = [];
	           var oldVersionStruct = versions.filter(function (version) {
	               return version._cfg.version === oldVersion;
	           })[0];
	           if (!oldVersionStruct) throw new exceptions.Upgrade("Dexie specification of currently installed DB version is missing");
	           globalSchema = db._dbSchema = oldVersionStruct._cfg.dbschema;
	           var anyContentUpgraderHasRun = false;
	
	           var versToRun = versions.filter(function (v) {
	               return v._cfg.version > oldVersion;
	           });
	           versToRun.forEach(function (version) {
	               /// <param name="version" type="Version"></param>
	               queue.push(function () {
	                   var oldSchema = globalSchema;
	                   var newSchema = version._cfg.dbschema;
	                   adjustToExistingIndexNames(oldSchema, idbtrans);
	                   adjustToExistingIndexNames(newSchema, idbtrans);
	                   globalSchema = db._dbSchema = newSchema;
	                   var diff = getSchemaDiff(oldSchema, newSchema);
	                   // Add tables          
	                   diff.add.forEach(function (tuple) {
	                       createTable(idbtrans, tuple[0], tuple[1].primKey, tuple[1].indexes);
	                   });
	                   // Change tables
	                   diff.change.forEach(function (change) {
	                       if (change.recreate) {
	                           throw new exceptions.Upgrade("Not yet support for changing primary key");
	                       } else {
	                           var store = idbtrans.objectStore(change.name);
	                           // Add indexes
	                           change.add.forEach(function (idx) {
	                               addIndex(store, idx);
	                           });
	                           // Update indexes
	                           change.change.forEach(function (idx) {
	                               store.deleteIndex(idx.name);
	                               addIndex(store, idx);
	                           });
	                           // Delete indexes
	                           change.del.forEach(function (idxName) {
	                               store.deleteIndex(idxName);
	                           });
	                       }
	                   });
	                   if (version._cfg.contentUpgrade) {
	                       anyContentUpgraderHasRun = true;
	                       return Promise.follow(function () {
	                           version._cfg.contentUpgrade(trans);
	                       });
	                   }
	               });
	               queue.push(function (idbtrans) {
	                   if (anyContentUpgraderHasRun && !hasIEDeleteObjectStoreBug) {
	                       // Dont delete old tables if ieBug is present and a content upgrader has run. Let tables be left in DB so far. This needs to be taken care of.
	                       var newSchema = version._cfg.dbschema;
	                       // Delete old tables
	                       deleteRemovedTables(newSchema, idbtrans);
	                   }
	               });
	           });
	
	           // Now, create a queue execution engine
	           function runQueue() {
	               return queue.length ? Promise.resolve(queue.shift()(trans.idbtrans)).then(runQueue) : Promise.resolve();
	           }
	
	           return runQueue().then(function () {
	               createMissingTables(globalSchema, idbtrans); // At last, make sure to create any missing tables. (Needed by addons that add stores to DB without specifying version)
	           });
	       }
	
	       function getSchemaDiff(oldSchema, newSchema) {
	           var diff = {
	               del: [], // Array of table names
	               add: [], // Array of [tableName, newDefinition]
	               change: [] // Array of {name: tableName, recreate: newDefinition, del: delIndexNames, add: newIndexDefs, change: changedIndexDefs}
	           };
	           for (var table in oldSchema) {
	               if (!newSchema[table]) diff.del.push(table);
	           }
	           for (table in newSchema) {
	               var oldDef = oldSchema[table],
	                   newDef = newSchema[table];
	               if (!oldDef) {
	                   diff.add.push([table, newDef]);
	               } else {
	                   var change = {
	                       name: table,
	                       def: newDef,
	                       recreate: false,
	                       del: [],
	                       add: [],
	                       change: []
	                   };
	                   if (oldDef.primKey.src !== newDef.primKey.src) {
	                       // Primary key has changed. Remove and re-add table.
	                       change.recreate = true;
	                       diff.change.push(change);
	                   } else {
	                       // Same primary key. Just find out what differs:
	                       var oldIndexes = oldDef.idxByName;
	                       var newIndexes = newDef.idxByName;
	                       for (var idxName in oldIndexes) {
	                           if (!newIndexes[idxName]) change.del.push(idxName);
	                       }
	                       for (idxName in newIndexes) {
	                           var oldIdx = oldIndexes[idxName],
	                               newIdx = newIndexes[idxName];
	                           if (!oldIdx) change.add.push(newIdx);else if (oldIdx.src !== newIdx.src) change.change.push(newIdx);
	                       }
	                       if (change.del.length > 0 || change.add.length > 0 || change.change.length > 0) {
	                           diff.change.push(change);
	                       }
	                   }
	               }
	           }
	           return diff;
	       }
	
	       function createTable(idbtrans, tableName, primKey, indexes) {
	           /// <param name="idbtrans" type="IDBTransaction"></param>
	           var store = idbtrans.db.createObjectStore(tableName, primKey.keyPath ? { keyPath: primKey.keyPath, autoIncrement: primKey.auto } : { autoIncrement: primKey.auto });
	           indexes.forEach(function (idx) {
	               addIndex(store, idx);
	           });
	           return store;
	       }
	
	       function createMissingTables(newSchema, idbtrans) {
	           keys(newSchema).forEach(function (tableName) {
	               if (!idbtrans.db.objectStoreNames.contains(tableName)) {
	                   createTable(idbtrans, tableName, newSchema[tableName].primKey, newSchema[tableName].indexes);
	               }
	           });
	       }
	
	       function deleteRemovedTables(newSchema, idbtrans) {
	           for (var i = 0; i < idbtrans.db.objectStoreNames.length; ++i) {
	               var storeName = idbtrans.db.objectStoreNames[i];
	               if (newSchema[storeName] == null) {
	                   idbtrans.db.deleteObjectStore(storeName);
	               }
	           }
	       }
	
	       function addIndex(store, idx) {
	           store.createIndex(idx.name, idx.keyPath, { unique: idx.unique, multiEntry: idx.multi });
	       }
	
	       function dbUncaught(err) {
	           return db.on.error.fire(err);
	       }
	
	       //
	       //
	       //      Dexie Protected API
	       //
	       //
	
	       this._allTables = allTables;
	
	       this._tableFactory = function createTable(mode, tableSchema) {
	           /// <param name="tableSchema" type="TableSchema"></param>
	           if (mode === READONLY) return new Table(tableSchema.name, tableSchema, Collection);else return new WriteableTable(tableSchema.name, tableSchema);
	       };
	
	       this._createTransaction = function (mode, storeNames, dbschema, parentTransaction) {
	           return new Transaction(mode, storeNames, dbschema, parentTransaction);
	       };
	
	       /* Generate a temporary transaction when db operations are done outside a transactino scope.
	       */
	       function tempTransaction(mode, storeNames, fn) {
	           // Last argument is "writeLocked". But this doesnt apply to oneshot direct db operations, so we ignore it.
	           if (!openComplete && !PSD.letThrough) {
	               if (!isBeingOpened) {
	                   if (!autoOpen) return rejection(new exceptions.DatabaseClosed(), dbUncaught);
	                   db.open().catch(nop); // Open in background. If if fails, it will be catched by the final promise anyway.
	               }
	               return dbReadyPromise.then(function () {
	                   return tempTransaction(mode, storeNames, fn);
	               });
	           } else {
	               var trans = db._createTransaction(mode, storeNames, globalSchema);
	               return trans._promise(mode, function (resolve, reject) {
	                   newScope(function () {
	                       // OPTIMIZATION POSSIBLE? newScope() not needed because it's already done in _promise.
	                       PSD.trans = trans;
	                       fn(resolve, reject, trans);
	                   });
	               }).then(function (result) {
	                   // Instead of resolving value directly, wait with resolving it until transaction has completed.
	                   // Otherwise the data would not be in the DB if requesting it in the then() operation.
	                   // Specifically, to ensure that the following expression will work:
	                   //
	                   //   db.friends.put({name: "Arne"}).then(function () {
	                   //       db.friends.where("name").equals("Arne").count(function(count) {
	                   //           assert (count === 1);
	                   //       });
	                   //   });
	                   //
	                   return trans._completion.then(function () {
	                       return result;
	                   });
	               }); /*.catch(err => { // Don't do this as of now. If would affect bulk- and modify methods in a way that could be more intuitive. But wait! Maybe change in next major.
	                    trans._reject(err);
	                    return rejection(err);
	                   });*/
	           }
	       }
	
	       this._whenReady = function (fn) {
	           return new Promise(fake || openComplete || PSD.letThrough ? fn : function (resolve, reject) {
	               if (!isBeingOpened) {
	                   if (!autoOpen) {
	                       reject(new exceptions.DatabaseClosed());
	                       return;
	                   }
	                   db.open().catch(nop); // Open in background. If if fails, it will be catched by the final promise anyway.
	               }
	               dbReadyPromise.then(function () {
	                   fn(resolve, reject);
	               });
	           }).uncaught(dbUncaught);
	       };
	
	       //
	       //
	       //
	       //
	       //      Dexie API
	       //
	       //
	       //
	
	       this.verno = 0;
	
	       this.open = function () {
	           if (isBeingOpened || idbdb) return dbReadyPromise.then(function () {
	               return dbOpenError ? rejection(dbOpenError, dbUncaught) : db;
	           });
	           debug && (openCanceller._stackHolder = getErrorWithStack()); // Let stacks point to when open() was called rather than where new Dexie() was called.
	           isBeingOpened = true;
	           dbOpenError = null;
	           openComplete = false;
	
	           // Function pointers to call when the core opening process completes.
	           var resolveDbReady = dbReadyResolve,
	
	           // upgradeTransaction to abort on failure.
	           upgradeTransaction = null;
	
	           return Promise.race([openCanceller, new Promise(function (resolve, reject) {
	               doFakeAutoComplete(function () {
	                   return resolve();
	               });
	
	               // Make sure caller has specified at least one version
	               if (versions.length > 0) autoSchema = false;
	
	               // Multiply db.verno with 10 will be needed to workaround upgrading bug in IE:
	               // IE fails when deleting objectStore after reading from it.
	               // A future version of Dexie.js will stopover an intermediate version to workaround this.
	               // At that point, we want to be backward compatible. Could have been multiplied with 2, but by using 10, it is easier to map the number to the real version number.
	
	               // If no API, throw!
	               if (!indexedDB) throw new exceptions.MissingAPI("indexedDB API not found. If using IE10+, make sure to run your code on a server URL " + "(not locally). If using old Safari versions, make sure to include indexedDB polyfill.");
	
	               var req = autoSchema ? indexedDB.open(dbName) : indexedDB.open(dbName, Math.round(db.verno * 10));
	               if (!req) throw new exceptions.MissingAPI("IndexedDB API not available"); // May happen in Safari private mode, see https://github.com/dfahlander/Dexie.js/issues/134
	               req.onerror = wrap(eventRejectHandler(reject));
	               req.onblocked = wrap(fireOnBlocked);
	               req.onupgradeneeded = wrap(function (e) {
	                   upgradeTransaction = req.transaction;
	                   if (autoSchema && !db._allowEmptyDB) {
	                       // Unless an addon has specified db._allowEmptyDB, lets make the call fail.
	                       // Caller did not specify a version or schema. Doing that is only acceptable for opening alread existing databases.
	                       // If onupgradeneeded is called it means database did not exist. Reject the open() promise and make sure that we
	                       // do not create a new database by accident here.
	                       req.onerror = preventDefault; // Prohibit onabort error from firing before we're done!
	                       upgradeTransaction.abort(); // Abort transaction (would hope that this would make DB disappear but it doesnt.)
	                       // Close database and delete it.
	                       req.result.close();
	                       var delreq = indexedDB.deleteDatabase(dbName); // The upgrade transaction is atomic, and javascript is single threaded - meaning that there is no risk that we delete someone elses database here!
	                       delreq.onsuccess = delreq.onerror = wrap(function () {
	                           reject(new exceptions.NoSuchDatabase('Database ' + dbName + ' doesnt exist'));
	                       });
	                   } else {
	                       upgradeTransaction.onerror = wrap(eventRejectHandler(reject));
	                       var oldVer = e.oldVersion > Math.pow(2, 62) ? 0 : e.oldVersion; // Safari 8 fix.
	                       runUpgraders(oldVer / 10, upgradeTransaction, reject, req);
	                   }
	               }, reject);
	
	               req.onsuccess = wrap(function () {
	                   // Core opening procedure complete. Now let's just record some stuff.
	                   upgradeTransaction = null;
	                   idbdb = req.result;
	                   connections.push(db); // Used for emulating versionchange event on IE/Edge/Safari.
	
	                   if (autoSchema) readGlobalSchema();else if (idbdb.objectStoreNames.length > 0) {
	                       try {
	                           adjustToExistingIndexNames(globalSchema, idbdb.transaction(safariMultiStoreFix(idbdb.objectStoreNames), READONLY));
	                       } catch (e) {
	                           // Safari may bail out if > 1 store names. However, this shouldnt be a showstopper. Issue #120.
	                       }
	                   }
	
	                   idbdb.onversionchange = wrap(function (ev) {
	                       db._vcFired = true; // detect implementations that not support versionchange (IE/Edge/Safari)
	                       db.on("versionchange").fire(ev);
	                   });
	
	                   if (!hasNativeGetDatabaseNames) {
	                       // Update localStorage with list of database names
	                       globalDatabaseList(function (databaseNames) {
	                           if (databaseNames.indexOf(dbName) === -1) return databaseNames.push(dbName);
	                       });
	                   }
	
	                   resolve();
	               }, reject);
	           })]).then(function () {
	               // Before finally resolving the dbReadyPromise and this promise,
	               // call and await all on('ready') subscribers:
	               // Dexie.vip() makes subscribers able to use the database while being opened.
	               // This is a must since these subscribers take part of the opening procedure.
	               return Dexie.vip(db.on.ready.fire);
	           }).then(function () {
	               // Resolve the db.open() with the db instance.
	               isBeingOpened = false;
	               return db;
	           }).catch(function (err) {
	               try {
	                   // Did we fail within onupgradeneeded? Make sure to abort the upgrade transaction so it doesnt commit.
	                   upgradeTransaction && upgradeTransaction.abort();
	               } catch (e) {}
	               isBeingOpened = false; // Set before calling db.close() so that it doesnt reject openCanceller again (leads to unhandled rejection event).
	               db.close(); // Closes and resets idbdb, removes connections, resets dbReadyPromise and openCanceller so that a later db.open() is fresh.
	               // A call to db.close() may have made on-ready subscribers fail. Use dbOpenError if set, since err could be a follow-up error on that.
	               dbOpenError = err; // Record the error. It will be used to reject further promises of db operations.
	               return rejection(dbOpenError, dbUncaught); // dbUncaught will make sure any error that happened in any operation before will now bubble to db.on.error() thanks to the special handling in Promise.uncaught().
	           }).finally(function () {
	               openComplete = true;
	               resolveDbReady(); // dbReadyPromise is resolved no matter if open() rejects or resolved. It's just to wake up waiters.
	           });
	       };
	
	       this.close = function () {
	           var idx = connections.indexOf(db);
	           if (idx >= 0) connections.splice(idx, 1);
	           if (idbdb) {
	               try {
	                   idbdb.close();
	               } catch (e) {}
	               idbdb = null;
	           }
	           autoOpen = false;
	           dbOpenError = new exceptions.DatabaseClosed();
	           if (isBeingOpened) cancelOpen(dbOpenError);
	           // Reset dbReadyPromise promise:
	           dbReadyPromise = new Promise(function (resolve) {
	               dbReadyResolve = resolve;
	           });
	           openCanceller = new Promise(function (_, reject) {
	               cancelOpen = reject;
	           });
	       };
	
	       this.delete = function () {
	           var hasArguments = arguments.length > 0;
	           return new Promise(function (resolve, reject) {
	               if (hasArguments) throw new exceptions.InvalidArgument("Arguments not allowed in db.delete()");
	               if (isBeingOpened) {
	                   dbReadyPromise.then(doDelete);
	               } else {
	                   doDelete();
	               }
	               function doDelete() {
	                   db.close();
	                   var req = indexedDB.deleteDatabase(dbName);
	                   req.onsuccess = wrap(function () {
	                       if (!hasNativeGetDatabaseNames) {
	                           globalDatabaseList(function (databaseNames) {
	                               var pos = databaseNames.indexOf(dbName);
	                               if (pos >= 0) return databaseNames.splice(pos, 1);
	                           });
	                       }
	                       resolve();
	                   });
	                   req.onerror = wrap(eventRejectHandler(reject));
	                   req.onblocked = fireOnBlocked;
	               }
	           }).uncaught(dbUncaught);
	       };
	
	       this.backendDB = function () {
	           return idbdb;
	       };
	
	       this.isOpen = function () {
	           return idbdb !== null;
	       };
	       this.hasFailed = function () {
	           return dbOpenError !== null;
	       };
	       this.dynamicallyOpened = function () {
	           return autoSchema;
	       };
	
	       //
	       // Properties
	       //
	       this.name = dbName;
	
	       // db.tables - an array of all Table instances.
	       setProp(this, "tables", {
	           get: function () {
	               /// <returns type="Array" elementType="WriteableTable" />
	               return keys(allTables).map(function (name) {
	                   return allTables[name];
	               });
	           }
	       });
	
	       //
	       // Events
	       //
	       this.on = Events(this, "error", "populate", "blocked", "versionchange", { ready: [promisableChain, nop] });
	
	       this.on.ready.subscribe = override(this.on.ready.subscribe, function (subscribe) {
	           return function (subscriber, bSticky) {
	               Dexie.vip(function () {
	                   subscribe(subscriber);
	                   if (!bSticky) subscribe(function unsubscribe() {
	                       db.on.ready.unsubscribe(subscriber);
	                       db.on.ready.unsubscribe(unsubscribe);
	                   });
	               });
	           };
	       });
	
	       fakeAutoComplete(function () {
	           db.on("populate").fire(db._createTransaction(READWRITE, dbStoreNames, globalSchema));
	           db.on("error").fire(new Error());
	       });
	
	       this.transaction = function (mode, tableInstances, scopeFunc) {
	           /// <summary>
	           ///
	           /// </summary>
	           /// <param name="mode" type="String">"r" for readonly, or "rw" for readwrite</param>
	           /// <param name="tableInstances">Table instance, Array of Table instances, String or String Array of object stores to include in the transaction</param>
	           /// <param name="scopeFunc" type="Function">Function to execute with transaction</param>
	
	           // Let table arguments be all arguments between mode and last argument.
	           var i = arguments.length;
	           if (i < 2) throw new exceptions.InvalidArgument("Too few arguments");
	           // Prevent optimzation killer (https://github.com/petkaantonov/bluebird/wiki/Optimization-killers#32-leaking-arguments)
	           // and clone arguments except the first one into local var 'args'.
	           var args = new Array(i - 1);
	           while (--i) {
	               args[i - 1] = arguments[i];
	           } // Let scopeFunc be the last argument and pop it so that args now only contain the table arguments.
	           scopeFunc = args.pop();
	           var tables = flatten(args); // Support using array as middle argument, or a mix of arrays and non-arrays.
	           var parentTransaction = PSD.trans;
	           // Check if parent transactions is bound to this db instance, and if caller wants to reuse it
	           if (!parentTransaction || parentTransaction.db !== db || mode.indexOf('!') !== -1) parentTransaction = null;
	           var onlyIfCompatible = mode.indexOf('?') !== -1;
	           mode = mode.replace('!', '').replace('?', ''); // Ok. Will change arguments[0] as well but we wont touch arguments henceforth.
	
	           try {
	               //
	               // Get storeNames from arguments. Either through given table instances, or through given table names.
	               //
	               var storeNames = tables.map(function (table) {
	                   var storeName = table instanceof Table ? table.name : table;
	                   if (typeof storeName !== 'string') throw new TypeError("Invalid table argument to Dexie.transaction(). Only Table or String are allowed");
	                   return storeName;
	               });
	
	               //
	               // Resolve mode. Allow shortcuts "r" and "rw".
	               //
	               if (mode == "r" || mode == READONLY) mode = READONLY;else if (mode == "rw" || mode == READWRITE) mode = READWRITE;else throw new exceptions.InvalidArgument("Invalid transaction mode: " + mode);
	
	               if (parentTransaction) {
	                   // Basic checks
	                   if (parentTransaction.mode === READONLY && mode === READWRITE) {
	                       if (onlyIfCompatible) {
	                           // Spawn new transaction instead.
	                           parentTransaction = null;
	                       } else throw new exceptions.SubTransaction("Cannot enter a sub-transaction with READWRITE mode when parent transaction is READONLY");
	                   }
	                   if (parentTransaction) {
	                       storeNames.forEach(function (storeName) {
	                           if (!hasOwn(parentTransaction.tables, storeName)) {
	                               if (onlyIfCompatible) {
	                                   // Spawn new transaction instead.
	                                   parentTransaction = null;
	                               } else throw new exceptions.SubTransaction("Table " + storeName + " not included in parent transaction.");
	                           }
	                       });
	                   }
	               }
	           } catch (e) {
	               return parentTransaction ? parentTransaction._promise(null, function (_, reject) {
	                   reject(e);
	               }) : rejection(e, dbUncaught);
	           }
	           // If this is a sub-transaction, lock the parent and then launch the sub-transaction.
	           return parentTransaction ? parentTransaction._promise(mode, enterTransactionScope, "lock") : db._whenReady(enterTransactionScope);
	
	           function enterTransactionScope(resolve) {
	               var parentPSD = PSD;
	               resolve(Promise.resolve().then(function () {
	                   return newScope(function () {
	                       // Keep a pointer to last non-transactional PSD to use if someone calls Dexie.ignoreTransaction().
	                       PSD.transless = PSD.transless || parentPSD;
	                       // Our transaction.
	                       //return new Promise((resolve, reject) => {
	                       var trans = db._createTransaction(mode, storeNames, globalSchema, parentTransaction);
	                       // Let the transaction instance be part of a Promise-specific data (PSD) value.
	                       PSD.trans = trans;
	
	                       if (parentTransaction) {
	                           // Emulate transaction commit awareness for inner transaction (must 'commit' when the inner transaction has no more operations ongoing)
	                           trans.idbtrans = parentTransaction.idbtrans;
	                       } else {
	                           trans.create(); // Create the backend transaction so that complete() or error() will trigger even if no operation is made upon it.
	                       }
	
	                       // Provide arguments to the scope function (for backward compatibility)
	                       var tableArgs = storeNames.map(function (name) {
	                           return trans.tables[name];
	                       });
	                       tableArgs.push(trans);
	
	                       var returnValue;
	                       return Promise.follow(function () {
	                           // Finally, call the scope function with our table and transaction arguments.
	                           returnValue = scopeFunc.apply(trans, tableArgs); // NOTE: returnValue is used in trans.on.complete() not as a returnValue to this func.
	                           if (returnValue) {
	                               if (typeof returnValue.next === 'function' && typeof returnValue.throw === 'function') {
	                                   // scopeFunc returned an iterator with throw-support. Handle yield as await.
	                                   returnValue = awaitIterator(returnValue);
	                               } else if (typeof returnValue.then === 'function' && !hasOwn(returnValue, '_PSD')) {
	                                   throw new exceptions.IncompatiblePromise("Incompatible Promise returned from transaction scope (read more at http://tinyurl.com/znyqjqc). Transaction scope: " + scopeFunc.toString());
	                               }
	                           }
	                       }).uncaught(dbUncaught).then(function () {
	                           if (parentTransaction) trans._resolve(); // sub transactions don't react to idbtrans.oncomplete. We must trigger a acompletion.
	                           return trans._completion; // Even if WE believe everything is fine. Await IDBTransaction's oncomplete or onerror as well.
	                       }).then(function () {
	                           return returnValue;
	                       }).catch(function (e) {
	                           //reject(e);
	                           trans._reject(e); // Yes, above then-handler were maybe not called because of an unhandled rejection in scopeFunc!
	                           return rejection(e);
	                       });
	                       //});
	                   });
	               }));
	           }
	       };
	
	       this.table = function (tableName) {
	           /// <returns type="WriteableTable"></returns>
	           if (fake && autoSchema) return new WriteableTable(tableName);
	           if (!hasOwn(allTables, tableName)) {
	               throw new exceptions.InvalidTable('Table ' + tableName + ' does not exist');
	           }
	           return allTables[tableName];
	       };
	
	       //
	       //
	       //
	       // Table Class
	       //
	       //
	       //
	       function Table(name, tableSchema, collClass) {
	           /// <param name="name" type="String"></param>
	           this.name = name;
	           this.schema = tableSchema;
	           this.hook = allTables[name] ? allTables[name].hook : Events(null, {
	               "creating": [hookCreatingChain, nop],
	               "reading": [pureFunctionChain, mirror],
	               "updating": [hookUpdatingChain, nop],
	               "deleting": [hookDeletingChain, nop]
	           });
	           this._collClass = collClass || Collection;
	       }
	
	       props(Table.prototype, {
	
	           //
	           // Table Protected Methods
	           //
	
	           _trans: function getTransaction(mode, fn, writeLocked) {
	               var trans = PSD.trans;
	               return trans && trans.db === db ? trans._promise(mode, fn, writeLocked) : tempTransaction(mode, [this.name], fn);
	           },
	           _idbstore: function getIDBObjectStore(mode, fn, writeLocked) {
	               if (fake) return new Promise(fn); // Simplify the work for Intellisense/Code completion.
	               var trans = PSD.trans,
	                   tableName = this.name;
	               function supplyIdbStore(resolve, reject, trans) {
	                   fn(resolve, reject, trans.idbtrans.objectStore(tableName), trans);
	               }
	               return trans && trans.db === db ? trans._promise(mode, supplyIdbStore, writeLocked) : tempTransaction(mode, [this.name], supplyIdbStore);
	           },
	
	           //
	           // Table Public Methods
	           //
	           get: function (key, cb) {
	               var self = this;
	               return this._idbstore(READONLY, function (resolve, reject, idbstore) {
	                   fake && resolve(self.schema.instanceTemplate);
	                   var req = idbstore.get(key);
	                   req.onerror = eventRejectHandler(reject);
	                   req.onsuccess = function () {
	                       resolve(self.hook.reading.fire(req.result));
	                   };
	               }).then(cb);
	           },
	           where: function (indexName) {
	               return new WhereClause(this, indexName);
	           },
	           count: function (cb) {
	               return this.toCollection().count(cb);
	           },
	           offset: function (offset) {
	               return this.toCollection().offset(offset);
	           },
	           limit: function (numRows) {
	               return this.toCollection().limit(numRows);
	           },
	           reverse: function () {
	               return this.toCollection().reverse();
	           },
	           filter: function (filterFunction) {
	               return this.toCollection().and(filterFunction);
	           },
	           each: function (fn) {
	               return this.toCollection().each(fn);
	           },
	           toArray: function (cb) {
	               return this.toCollection().toArray(cb);
	           },
	           orderBy: function (index) {
	               return new this._collClass(new WhereClause(this, index));
	           },
	
	           toCollection: function () {
	               return new this._collClass(new WhereClause(this));
	           },
	
	           mapToClass: function (constructor, structure) {
	               /// <summary>
	               ///     Map table to a javascript constructor function. Objects returned from the database will be instances of this class, making
	               ///     it possible to the instanceOf operator as well as extending the class using constructor.prototype.method = function(){...}.
	               /// </summary>
	               /// <param name="constructor">Constructor function representing the class.</param>
	               /// <param name="structure" optional="true">Helps IDE code completion by knowing the members that objects contain and not just the indexes. Also
	               /// know what type each member has. Example: {name: String, emailAddresses: [String], password}</param>
	               this.schema.mappedClass = constructor;
	               var instanceTemplate = Object.create(constructor.prototype);
	               if (structure) {
	                   // structure and instanceTemplate is for IDE code competion only while constructor.prototype is for actual inheritance.
	                   applyStructure(instanceTemplate, structure);
	               }
	               this.schema.instanceTemplate = instanceTemplate;
	
	               // Now, subscribe to the when("reading") event to make all objects that come out from this table inherit from given class
	               // no matter which method to use for reading (Table.get() or Table.where(...)... )
	               var readHook = function (obj) {
	                   if (!obj) return obj; // No valid object. (Value is null). Return as is.
	                   // Create a new object that derives from constructor:
	                   var res = Object.create(constructor.prototype);
	                   // Clone members:
	                   for (var m in obj) {
	                       if (hasOwn(obj, m)) res[m] = obj[m];
	                   }return res;
	               };
	
	               if (this.schema.readHook) {
	                   this.hook.reading.unsubscribe(this.schema.readHook);
	               }
	               this.schema.readHook = readHook;
	               this.hook("reading", readHook);
	               return constructor;
	           },
	           defineClass: function (structure) {
	               /// <summary>
	               ///     Define all members of the class that represents the table. This will help code completion of when objects are read from the database
	               ///     as well as making it possible to extend the prototype of the returned constructor function.
	               /// </summary>
	               /// <param name="structure">Helps IDE code completion by knowing the members that objects contain and not just the indexes. Also
	               /// know what type each member has. Example: {name: String, emailAddresses: [String], properties: {shoeSize: Number}}</param>
	               return this.mapToClass(Dexie.defineClass(structure), structure);
	           }
	       });
	
	       //
	       //
	       //
	       // WriteableTable Class (extends Table)
	       //
	       //
	       //
	       function WriteableTable(name, tableSchema, collClass) {
	           Table.call(this, name, tableSchema, collClass || WriteableCollection);
	       }
	
	       function BulkErrorHandlerCatchAll(errorList, done, supportHooks) {
	           return (supportHooks ? hookedEventRejectHandler : eventRejectHandler)(function (e) {
	               errorList.push(e);
	               done && done();
	           });
	       }
	
	       function bulkDelete(idbstore, trans, keysOrTuples, hasDeleteHook, deletingHook) {
	           // If hasDeleteHook, keysOrTuples must be an array of tuples: [[key1, value2],[key2,value2],...],
	           // else keysOrTuples must be just an array of keys: [key1, key2, ...].
	           return new Promise(function (resolve, reject) {
	               var len = keysOrTuples.length,
	                   lastItem = len - 1;
	               if (len === 0) return resolve();
	               if (!hasDeleteHook) {
	                   for (var i = 0; i < len; ++i) {
	                       var req = idbstore.delete(keysOrTuples[i]);
	                       req.onerror = wrap(eventRejectHandler(reject));
	                       if (i === lastItem) req.onsuccess = wrap(function () {
	                           return resolve();
	                       });
	                   }
	               } else {
	                   var hookCtx,
	                       errorHandler = hookedEventRejectHandler(reject),
	                       successHandler = hookedEventSuccessHandler(null);
	                   tryCatch(function () {
	                       for (var i = 0; i < len; ++i) {
	                           hookCtx = { onsuccess: null, onerror: null };
	                           var tuple = keysOrTuples[i];
	                           deletingHook.call(hookCtx, tuple[0], tuple[1], trans);
	                           var req = idbstore.delete(tuple[0]);
	                           req._hookCtx = hookCtx;
	                           req.onerror = errorHandler;
	                           if (i === lastItem) req.onsuccess = hookedEventSuccessHandler(resolve);else req.onsuccess = successHandler;
	                       }
	                   }, function (err) {
	                       hookCtx.onerror && hookCtx.onerror(err);
	                       throw err;
	                   });
	               }
	           }).uncaught(dbUncaught);
	       }
	
	       derive(WriteableTable).from(Table).extend({
	           bulkDelete: function (keys) {
	               if (this.hook.deleting.fire === nop) {
	                   return this._idbstore(READWRITE, function (resolve, reject, idbstore, trans) {
	                       resolve(bulkDelete(idbstore, trans, keys, false, nop));
	                   });
	               } else {
	                   return this.where(':id').anyOf(keys).delete().then(function () {}); // Resolve with undefined.
	               }
	           },
	           bulkPut: function (objects, keys) {
	               var _this = this;
	
	               return this._idbstore(READWRITE, function (resolve, reject, idbstore) {
	                   if (!idbstore.keyPath && !_this.schema.primKey.auto && !keys) throw new exceptions.InvalidArgument("bulkPut() with non-inbound keys requires keys array in second argument");
	                   if (idbstore.keyPath && keys) throw new exceptions.InvalidArgument("bulkPut(): keys argument invalid on tables with inbound keys");
	                   if (keys && keys.length !== objects.length) throw new exceptions.InvalidArgument("Arguments objects and keys must have the same length");
	                   if (objects.length === 0) return resolve(); // Caller provided empty list.
	                   var done = function (result) {
	                       if (errorList.length === 0) resolve(result);else reject(new BulkError(_this.name + '.bulkPut(): ' + errorList.length + ' of ' + numObjs + ' operations failed', errorList));
	                   };
	                   var req,
	                       errorList = [],
	                       errorHandler,
	                       numObjs = objects.length,
	                       table = _this;
	                   if (_this.hook.creating.fire === nop && _this.hook.updating.fire === nop) {
	                       //
	                       // Standard Bulk (no 'creating' or 'updating' hooks to care about)
	                       //
	                       errorHandler = BulkErrorHandlerCatchAll(errorList);
	                       for (var i = 0, l = objects.length; i < l; ++i) {
	                           req = keys ? idbstore.put(objects[i], keys[i]) : idbstore.put(objects[i]);
	                           req.onerror = errorHandler;
	                       }
	                       // Only need to catch success or error on the last operation
	                       // according to the IDB spec.
	                       req.onerror = BulkErrorHandlerCatchAll(errorList, done);
	                       req.onsuccess = eventSuccessHandler(done);
	                   } else {
	                       var effectiveKeys = keys || idbstore.keyPath && objects.map(function (o) {
	                           return getByKeyPath(o, idbstore.keyPath);
	                       });
	                       // Generate map of {[key]: object}
	                       var objectLookup = effectiveKeys && arrayToObject(effectiveKeys, function (key, i) {
	                           return key != null && [key, objects[i]];
	                       });
	                       var promise = !effectiveKeys ?
	
	                       // Auto-incremented key-less objects only without any keys argument.
	                       table.bulkAdd(objects) :
	
	                       // Keys provided. Either as inbound in provided objects, or as a keys argument.
	                       // Begin with updating those that exists in DB:
	                       table.where(':id').anyOf(effectiveKeys.filter(function (key) {
	                           return key != null;
	                       })).modify(function () {
	                           this.value = objectLookup[this.primKey];
	                           objectLookup[this.primKey] = null; // Mark as "don't add this"
	                       }).catch(ModifyError, function (e) {
	                           errorList = e.failures; // No need to concat here. These are the first errors added.
	                       }).then(function () {
	                           // Now, let's examine which items didnt exist so we can add them:
	                           var objsToAdd = [],
	                               keysToAdd = keys && [];
	                           // Iterate backwards. Why? Because if same key was used twice, just add the last one.
	                           for (var i = effectiveKeys.length - 1; i >= 0; --i) {
	                               var key = effectiveKeys[i];
	                               if (key == null || objectLookup[key]) {
	                                   objsToAdd.push(objects[i]);
	                                   keys && keysToAdd.push(key);
	                                   if (key != null) objectLookup[key] = null; // Mark as "dont add again"
	                               }
	                           }
	                           // The items are in reverse order so reverse them before adding.
	                           // Could be important in order to get auto-incremented keys the way the caller
	                           // would expect. Could have used unshift instead of push()/reverse(),
	                           // but: http://jsperf.com/unshift-vs-reverse
	                           objsToAdd.reverse();
	                           keys && keysToAdd.reverse();
	                           return table.bulkAdd(objsToAdd, keysToAdd);
	                       }).then(function (lastAddedKey) {
	                           // Resolve with key of the last object in given arguments to bulkPut():
	                           var lastEffectiveKey = effectiveKeys[effectiveKeys.length - 1]; // Key was provided.
	                           return lastEffectiveKey != null ? lastEffectiveKey : lastAddedKey;
	                       });
	
	                       promise.then(done).catch(BulkError, function (e) {
	                           // Concat failure from ModifyError and reject using our 'done' method.
	                           errorList = errorList.concat(e.failures);
	                           done();
	                       }).catch(reject);
	                   }
	               }, "locked"); // If called from transaction scope, lock transaction til all steps are done.
	           },
	           bulkAdd: function (objects, keys) {
	               var self = this,
	                   creatingHook = this.hook.creating.fire;
	               return this._idbstore(READWRITE, function (resolve, reject, idbstore, trans) {
	                   if (!idbstore.keyPath && !self.schema.primKey.auto && !keys) throw new exceptions.InvalidArgument("bulkAdd() with non-inbound keys requires keys array in second argument");
	                   if (idbstore.keyPath && keys) throw new exceptions.InvalidArgument("bulkAdd(): keys argument invalid on tables with inbound keys");
	                   if (keys && keys.length !== objects.length) throw new exceptions.InvalidArgument("Arguments objects and keys must have the same length");
	                   if (objects.length === 0) return resolve(); // Caller provided empty list.
	                   function done(result) {
	                       if (errorList.length === 0) resolve(result);else reject(new BulkError(self.name + '.bulkAdd(): ' + errorList.length + ' of ' + numObjs + ' operations failed', errorList));
	                   }
	                   var req,
	                       errorList = [],
	                       errorHandler,
	                       successHandler,
	                       numObjs = objects.length;
	                   if (creatingHook !== nop) {
	                       //
	                       // There are subscribers to hook('creating')
	                       // Must behave as documented.
	                       //
	                       var keyPath = idbstore.keyPath,
	                           hookCtx;
	                       errorHandler = BulkErrorHandlerCatchAll(errorList, null, true);
	                       successHandler = hookedEventSuccessHandler(null);
	
	                       tryCatch(function () {
	                           for (var i = 0, l = objects.length; i < l; ++i) {
	                               hookCtx = { onerror: null, onsuccess: null };
	                               var key = keys && keys[i];
	                               var obj = objects[i],
	                                   effectiveKey = keys ? key : keyPath ? getByKeyPath(obj, keyPath) : undefined,
	                                   keyToUse = creatingHook.call(hookCtx, effectiveKey, obj, trans);
	                               if (effectiveKey == null && keyToUse != null) {
	                                   if (keyPath) {
	                                       obj = deepClone(obj);
	                                       setByKeyPath(obj, keyPath, keyToUse);
	                                   } else {
	                                       key = keyToUse;
	                                   }
	                               }
	                               req = key != null ? idbstore.add(obj, key) : idbstore.add(obj);
	                               req._hookCtx = hookCtx;
	                               if (i < l - 1) {
	                                   req.onerror = errorHandler;
	                                   if (hookCtx.onsuccess) req.onsuccess = successHandler;
	                               }
	                           }
	                       }, function (err) {
	                           hookCtx.onerror && hookCtx.onerror(err);
	                           throw err;
	                       });
	
	                       req.onerror = BulkErrorHandlerCatchAll(errorList, done, true);
	                       req.onsuccess = hookedEventSuccessHandler(done);
	                   } else {
	                       //
	                       // Standard Bulk (no 'creating' hook to care about)
	                       //
	                       errorHandler = BulkErrorHandlerCatchAll(errorList);
	                       for (var i = 0, l = objects.length; i < l; ++i) {
	                           req = keys ? idbstore.add(objects[i], keys[i]) : idbstore.add(objects[i]);
	                           req.onerror = errorHandler;
	                       }
	                       // Only need to catch success or error on the last operation
	                       // according to the IDB spec.
	                       req.onerror = BulkErrorHandlerCatchAll(errorList, done);
	                       req.onsuccess = eventSuccessHandler(done);
	                   }
	               });
	           },
	           add: function (obj, key) {
	               /// <summary>
	               ///   Add an object to the database. In case an object with same primary key already exists, the object will not be added.
	               /// </summary>
	               /// <param name="obj" type="Object">A javascript object to insert</param>
	               /// <param name="key" optional="true">Primary key</param>
	               var creatingHook = this.hook.creating.fire;
	               return this._idbstore(READWRITE, function (resolve, reject, idbstore, trans) {
	                   var hookCtx = { onsuccess: null, onerror: null };
	                   if (creatingHook !== nop) {
	                       var effectiveKey = key != null ? key : idbstore.keyPath ? getByKeyPath(obj, idbstore.keyPath) : undefined;
	                       var keyToUse = creatingHook.call(hookCtx, effectiveKey, obj, trans); // Allow subscribers to when("creating") to generate the key.
	                       if (effectiveKey == null && keyToUse != null) {
	                           // Using "==" and "!=" to check for either null or undefined!
	                           if (idbstore.keyPath) setByKeyPath(obj, idbstore.keyPath, keyToUse);else key = keyToUse;
	                       }
	                   }
	                   try {
	                       var req = key != null ? idbstore.add(obj, key) : idbstore.add(obj);
	                       req._hookCtx = hookCtx;
	                       req.onerror = hookedEventRejectHandler(reject);
	                       req.onsuccess = hookedEventSuccessHandler(function (result) {
	                           // TODO: Remove these two lines in next major release (2.0?)
	                           // It's no good practice to have side effects on provided parameters
	                           var keyPath = idbstore.keyPath;
	                           if (keyPath) setByKeyPath(obj, keyPath, result);
	                           resolve(result);
	                       });
	                   } catch (e) {
	                       if (hookCtx.onerror) hookCtx.onerror(e);
	                       throw e;
	                   }
	               });
	           },
	
	           put: function (obj, key) {
	               /// <summary>
	               ///   Add an object to the database but in case an object with same primary key alread exists, the existing one will get updated.
	               /// </summary>
	               /// <param name="obj" type="Object">A javascript object to insert or update</param>
	               /// <param name="key" optional="true">Primary key</param>
	               var self = this,
	                   creatingHook = this.hook.creating.fire,
	                   updatingHook = this.hook.updating.fire;
	               if (creatingHook !== nop || updatingHook !== nop) {
	                   //
	                   // People listens to when("creating") or when("updating") events!
	                   // We must know whether the put operation results in an CREATE or UPDATE.
	                   //
	                   return this._trans(READWRITE, function (resolve, reject, trans) {
	                       // Since key is optional, make sure we get it from obj if not provided
	                       var effectiveKey = key !== undefined ? key : self.schema.primKey.keyPath && getByKeyPath(obj, self.schema.primKey.keyPath);
	                       if (effectiveKey == null) {
	                           // "== null" means checking for either null or undefined.
	                           // No primary key. Must use add().
	                           trans.tables[self.name].add(obj).then(resolve, reject);
	                       } else {
	                           // Primary key exist. Lock transaction and try modifying existing. If nothing modified, call add().
	                           trans._lock(); // Needed because operation is splitted into modify() and add().
	                           // clone obj before this async call. If caller modifies obj the line after put(), the IDB spec requires that it should not affect operation.
	                           obj = deepClone(obj);
	                           trans.tables[self.name].where(":id").equals(effectiveKey).modify(function () {
	                               // Replace extisting value with our object
	                               // CRUD event firing handled in WriteableCollection.modify()
	                               this.value = obj;
	                           }).then(function (count) {
	                               if (count === 0) {
	                                   // Object's key was not found. Add the object instead.
	                                   // CRUD event firing will be done in add()
	                                   return trans.tables[self.name].add(obj, key); // Resolving with another Promise. Returned Promise will then resolve with the new key.
	                               } else {
	                                       return effectiveKey; // Resolve with the provided key.
	                                   }
	                           }).finally(function () {
	                               trans._unlock();
	                           }).then(resolve, reject);
	                       }
	                   });
	               } else {
	                   // Use the standard IDB put() method.
	                   return this._idbstore(READWRITE, function (resolve, reject, idbstore) {
	                       var req = key !== undefined ? idbstore.put(obj, key) : idbstore.put(obj);
	                       req.onerror = eventRejectHandler(reject);
	                       req.onsuccess = function (ev) {
	                           var keyPath = idbstore.keyPath;
	                           if (keyPath) setByKeyPath(obj, keyPath, ev.target.result);
	                           resolve(req.result);
	                       };
	                   });
	               }
	           },
	
	           'delete': function (key) {
	               /// <param name="key">Primary key of the object to delete</param>
	               if (this.hook.deleting.subscribers.length) {
	                   // People listens to when("deleting") event. Must implement delete using WriteableCollection.delete() that will
	                   // call the CRUD event. Only WriteableCollection.delete() will know whether an object was actually deleted.
	                   return this.where(":id").equals(key).delete();
	               } else {
	                   // No one listens. Use standard IDB delete() method.
	                   return this._idbstore(READWRITE, function (resolve, reject, idbstore) {
	                       var req = idbstore.delete(key);
	                       req.onerror = eventRejectHandler(reject);
	                       req.onsuccess = function () {
	                           resolve(req.result);
	                       };
	                   });
	               }
	           },
	
	           clear: function () {
	               if (this.hook.deleting.subscribers.length) {
	                   // People listens to when("deleting") event. Must implement delete using WriteableCollection.delete() that will
	                   // call the CRUD event. Only WriteableCollection.delete() will knows which objects that are actually deleted.
	                   return this.toCollection().delete();
	               } else {
	                   return this._idbstore(READWRITE, function (resolve, reject, idbstore) {
	                       var req = idbstore.clear();
	                       req.onerror = eventRejectHandler(reject);
	                       req.onsuccess = function () {
	                           resolve(req.result);
	                       };
	                   });
	               }
	           },
	
	           update: function (keyOrObject, modifications) {
	               if (typeof modifications !== 'object' || isArray(modifications)) throw new exceptions.InvalidArgument("Modifications must be an object.");
	               if (typeof keyOrObject === 'object' && !isArray(keyOrObject)) {
	                   // object to modify. Also modify given object with the modifications:
	                   keys(modifications).forEach(function (keyPath) {
	                       setByKeyPath(keyOrObject, keyPath, modifications[keyPath]);
	                   });
	                   var key = getByKeyPath(keyOrObject, this.schema.primKey.keyPath);
	                   if (key === undefined) return rejection(new exceptions.InvalidArgument("Given object does not contain its primary key"), dbUncaught);
	                   return this.where(":id").equals(key).modify(modifications);
	               } else {
	                   // key to modify
	                   return this.where(":id").equals(keyOrObject).modify(modifications);
	               }
	           }
	       });
	
	       //
	       //
	       //
	       // Transaction Class
	       //
	       //
	       //
	       function Transaction(mode, storeNames, dbschema, parent) {
	           var _this2 = this;
	
	           /// <summary>
	           ///    Transaction class. Represents a database transaction. All operations on db goes through a Transaction.
	           /// </summary>
	           /// <param name="mode" type="String">Any of "readwrite" or "readonly"</param>
	           /// <param name="storeNames" type="Array">Array of table names to operate on</param>
	           this.db = db;
	           this.mode = mode;
	           this.storeNames = storeNames;
	           this.idbtrans = null;
	           this.on = Events(this, "complete", "error", "abort");
	           this.parent = parent || null;
	           this.active = true;
	           this._tables = null;
	           this._reculock = 0;
	           this._blockedFuncs = [];
	           this._psd = null;
	           this._dbschema = dbschema;
	           this._resolve = null;
	           this._reject = null;
	           this._completion = new Promise(function (resolve, reject) {
	               _this2._resolve = resolve;
	               _this2._reject = reject;
	           }).uncaught(dbUncaught);
	
	           this._completion.then(function () {
	               _this2.on.complete.fire();
	           }, function (e) {
	               _this2.on.error.fire(e);
	               _this2.parent ? _this2.parent._reject(e) : _this2.active && _this2.idbtrans && _this2.idbtrans.abort();
	               _this2.active = false;
	               return rejection(e); // Indicate we actually DO NOT catch this error.
	           });
	       }
	
	       props(Transaction.prototype, {
	           //
	           // Transaction Protected Methods (not required by API users, but needed internally and eventually by dexie extensions)
	           //
	           _lock: function () {
	               assert(!PSD.global); // Locking and unlocking reuires to be within a PSD scope.
	               // Temporary set all requests into a pending queue if they are called before database is ready.
	               ++this._reculock; // Recursive read/write lock pattern using PSD (Promise Specific Data) instead of TLS (Thread Local Storage)
	               if (this._reculock === 1 && !PSD.global) PSD.lockOwnerFor = this;
	               return this;
	           },
	           _unlock: function () {
	               assert(!PSD.global); // Locking and unlocking reuires to be within a PSD scope.
	               if (--this._reculock === 0) {
	                   if (!PSD.global) PSD.lockOwnerFor = null;
	                   while (this._blockedFuncs.length > 0 && !this._locked()) {
	                       var fn = this._blockedFuncs.shift();
	                       try {
	                           fn();
	                       } catch (e) {}
	                   }
	               }
	               return this;
	           },
	           _locked: function () {
	               // Checks if any write-lock is applied on this transaction.
	               // To simplify the Dexie API for extension implementations, we support recursive locks.
	               // This is accomplished by using "Promise Specific Data" (PSD).
	               // PSD data is bound to a Promise and any child Promise emitted through then() or resolve( new Promise() ).
	               // PSD is local to code executing on top of the call stacks of any of any code executed by Promise():
	               //         * callback given to the Promise() constructor  (function (resolve, reject){...})
	               //         * callbacks given to then()/catch()/finally() methods (function (value){...})
	               // If creating a new independant Promise instance from within a Promise call stack, the new Promise will derive the PSD from the call stack of the parent Promise.
	               // Derivation is done so that the inner PSD __proto__ points to the outer PSD.
	               // PSD.lockOwnerFor will point to current transaction object if the currently executing PSD scope owns the lock.
	               return this._reculock && PSD.lockOwnerFor !== this;
	           },
	           create: function (idbtrans) {
	               var _this3 = this;
	
	               assert(!this.idbtrans);
	               if (!idbtrans && !idbdb) {
	                   switch (dbOpenError && dbOpenError.name) {
	                       case "DatabaseClosedError":
	                           // Errors where it is no difference whether it was caused by the user operation or an earlier call to db.open()
	                           throw new exceptions.DatabaseClosed(dbOpenError);
	                       case "MissingAPIError":
	                           // Errors where it is no difference whether it was caused by the user operation or an earlier call to db.open()
	                           throw new exceptions.MissingAPI(dbOpenError.message, dbOpenError);
	                       default:
	                           // Make it clear that the user operation was not what caused the error - the error had occurred earlier on db.open()!
	                           throw new exceptions.OpenFailed(dbOpenError);
	                   }
	               }
	               if (!this.active) throw new exceptions.TransactionInactive();
	               assert(this._completion._state === null);
	
	               idbtrans = this.idbtrans = idbtrans || idbdb.transaction(safariMultiStoreFix(this.storeNames), this.mode);
	               idbtrans.onerror = wrap(function (ev) {
	                   preventDefault(ev); // Prohibit default bubbling to window.error
	                   _this3._reject(idbtrans.error);
	               });
	               idbtrans.onabort = wrap(function (ev) {
	                   preventDefault(ev);
	                   _this3.active && _this3._reject(new exceptions.Abort());
	                   _this3.active = false;
	                   _this3.on("abort").fire(ev);
	               });
	               idbtrans.oncomplete = wrap(function () {
	                   _this3.active = false;
	                   _this3._resolve();
	               });
	               return this;
	           },
	           _promise: function (mode, fn, bWriteLock) {
	               var self = this;
	               return newScope(function () {
	                   var p;
	                   // Read lock always
	                   if (!self._locked()) {
	                       p = self.active ? new Promise(function (resolve, reject) {
	                           if (mode === READWRITE && self.mode !== READWRITE) throw new exceptions.ReadOnly("Transaction is readonly");
	                           if (!self.idbtrans && mode) self.create();
	                           if (bWriteLock) self._lock(); // Write lock if write operation is requested
	                           fn(resolve, reject, self);
	                       }) : rejection(new exceptions.TransactionInactive());
	                       if (self.active && bWriteLock) p.finally(function () {
	                           self._unlock();
	                       });
	                   } else {
	                       // Transaction is write-locked. Wait for mutex.
	                       p = new Promise(function (resolve, reject) {
	                           self._blockedFuncs.push(function () {
	                               self._promise(mode, fn, bWriteLock).then(resolve, reject);
	                           });
	                       });
	                   }
	                   p._lib = true;
	                   return p.uncaught(dbUncaught);
	               });
	           },
	
	           //
	           // Transaction Public Properties and Methods
	           //
	           abort: function () {
	               this.active && this._reject(new exceptions.Abort());
	               this.active = false;
	           },
	
	           // Deprecate:
	           tables: {
	               get: function () {
	                   if (this._tables) return this._tables;
	                   return this._tables = arrayToObject(this.storeNames, function (name) {
	                       return [name, allTables[name]];
	                   });
	               }
	           },
	
	           // Deprecate:
	           complete: function (cb) {
	               return this.on("complete", cb);
	           },
	
	           // Deprecate:
	           error: function (cb) {
	               return this.on("error", cb);
	           },
	
	           // Deprecate
	           table: function (name) {
	               if (this.storeNames.indexOf(name) === -1) throw new exceptions.InvalidTable("Table " + name + " not in transaction");
	               return allTables[name];
	           }
	       });
	
	       //
	       //
	       //
	       // WhereClause
	       //
	       //
	       //
	       function WhereClause(table, index, orCollection) {
	           /// <param name="table" type="Table"></param>
	           /// <param name="index" type="String" optional="true"></param>
	           /// <param name="orCollection" type="Collection" optional="true"></param>
	           this._ctx = {
	               table: table,
	               index: index === ":id" ? null : index,
	               collClass: table._collClass,
	               or: orCollection
	           };
	       }
	
	       props(WhereClause.prototype, function () {
	
	           // WhereClause private methods
	
	           function fail(collectionOrWhereClause, err, T) {
	               var collection = collectionOrWhereClause instanceof WhereClause ? new collectionOrWhereClause._ctx.collClass(collectionOrWhereClause) : collectionOrWhereClause;
	
	               collection._ctx.error = T ? new T(err) : new TypeError(err);
	               return collection;
	           }
	
	           function emptyCollection(whereClause) {
	               return new whereClause._ctx.collClass(whereClause, function () {
	                   return IDBKeyRange.only("");
	               }).limit(0);
	           }
	
	           function upperFactory(dir) {
	               return dir === "next" ? function (s) {
	                   return s.toUpperCase();
	               } : function (s) {
	                   return s.toLowerCase();
	               };
	           }
	           function lowerFactory(dir) {
	               return dir === "next" ? function (s) {
	                   return s.toLowerCase();
	               } : function (s) {
	                   return s.toUpperCase();
	               };
	           }
	           function nextCasing(key, lowerKey, upperNeedle, lowerNeedle, cmp, dir) {
	               var length = Math.min(key.length, lowerNeedle.length);
	               var llp = -1;
	               for (var i = 0; i < length; ++i) {
	                   var lwrKeyChar = lowerKey[i];
	                   if (lwrKeyChar !== lowerNeedle[i]) {
	                       if (cmp(key[i], upperNeedle[i]) < 0) return key.substr(0, i) + upperNeedle[i] + upperNeedle.substr(i + 1);
	                       if (cmp(key[i], lowerNeedle[i]) < 0) return key.substr(0, i) + lowerNeedle[i] + upperNeedle.substr(i + 1);
	                       if (llp >= 0) return key.substr(0, llp) + lowerKey[llp] + upperNeedle.substr(llp + 1);
	                       return null;
	                   }
	                   if (cmp(key[i], lwrKeyChar) < 0) llp = i;
	               }
	               if (length < lowerNeedle.length && dir === "next") return key + upperNeedle.substr(key.length);
	               if (length < key.length && dir === "prev") return key.substr(0, upperNeedle.length);
	               return llp < 0 ? null : key.substr(0, llp) + lowerNeedle[llp] + upperNeedle.substr(llp + 1);
	           }
	
	           function addIgnoreCaseAlgorithm(whereClause, match, needles, suffix) {
	               /// <param name="needles" type="Array" elementType="String"></param>
	               var upper,
	                   lower,
	                   compare,
	                   upperNeedles,
	                   lowerNeedles,
	                   direction,
	                   nextKeySuffix,
	                   needlesLen = needles.length;
	               if (!needles.every(function (s) {
	                   return typeof s === 'string';
	               })) {
	                   return fail(whereClause, STRING_EXPECTED);
	               }
	               function initDirection(dir) {
	                   upper = upperFactory(dir);
	                   lower = lowerFactory(dir);
	                   compare = dir === "next" ? simpleCompare : simpleCompareReverse;
	                   var needleBounds = needles.map(function (needle) {
	                       return { lower: lower(needle), upper: upper(needle) };
	                   }).sort(function (a, b) {
	                       return compare(a.lower, b.lower);
	                   });
	                   upperNeedles = needleBounds.map(function (nb) {
	                       return nb.upper;
	                   });
	                   lowerNeedles = needleBounds.map(function (nb) {
	                       return nb.lower;
	                   });
	                   direction = dir;
	                   nextKeySuffix = dir === "next" ? "" : suffix;
	               }
	               initDirection("next");
	
	               var c = new whereClause._ctx.collClass(whereClause, function () {
	                   return IDBKeyRange.bound(upperNeedles[0], lowerNeedles[needlesLen - 1] + suffix);
	               });
	
	               c._ondirectionchange = function (direction) {
	                   // This event onlys occur before filter is called the first time.
	                   initDirection(direction);
	               };
	
	               var firstPossibleNeedle = 0;
	
	               c._addAlgorithm(function (cursor, advance, resolve) {
	                   /// <param name="cursor" type="IDBCursor"></param>
	                   /// <param name="advance" type="Function"></param>
	                   /// <param name="resolve" type="Function"></param>
	                   var key = cursor.key;
	                   if (typeof key !== 'string') return false;
	                   var lowerKey = lower(key);
	                   if (match(lowerKey, lowerNeedles, firstPossibleNeedle)) {
	                       return true;
	                   } else {
	                       var lowestPossibleCasing = null;
	                       for (var i = firstPossibleNeedle; i < needlesLen; ++i) {
	                           var casing = nextCasing(key, lowerKey, upperNeedles[i], lowerNeedles[i], compare, direction);
	                           if (casing === null && lowestPossibleCasing === null) firstPossibleNeedle = i + 1;else if (lowestPossibleCasing === null || compare(lowestPossibleCasing, casing) > 0) {
	                               lowestPossibleCasing = casing;
	                           }
	                       }
	                       if (lowestPossibleCasing !== null) {
	                           advance(function () {
	                               cursor.continue(lowestPossibleCasing + nextKeySuffix);
	                           });
	                       } else {
	                           advance(resolve);
	                       }
	                       return false;
	                   }
	               });
	               return c;
	           }
	
	           //
	           // WhereClause public methods
	           //
	           return {
	               between: function (lower, upper, includeLower, includeUpper) {
	                   /// <summary>
	                   ///     Filter out records whose where-field lays between given lower and upper values. Applies to Strings, Numbers and Dates.
	                   /// </summary>
	                   /// <param name="lower"></param>
	                   /// <param name="upper"></param>
	                   /// <param name="includeLower" optional="true">Whether items that equals lower should be included. Default true.</param>
	                   /// <param name="includeUpper" optional="true">Whether items that equals upper should be included. Default false.</param>
	                   /// <returns type="Collection"></returns>
	                   includeLower = includeLower !== false; // Default to true
	                   includeUpper = includeUpper === true; // Default to false
	                   try {
	                       if (cmp(lower, upper) > 0 || cmp(lower, upper) === 0 && (includeLower || includeUpper) && !(includeLower && includeUpper)) return emptyCollection(this); // Workaround for idiotic W3C Specification that DataError must be thrown if lower > upper. The natural result would be to return an empty collection.
	                       return new this._ctx.collClass(this, function () {
	                           return IDBKeyRange.bound(lower, upper, !includeLower, !includeUpper);
	                       });
	                   } catch (e) {
	                       return fail(this, INVALID_KEY_ARGUMENT);
	                   }
	               },
	               equals: function (value) {
	                   return new this._ctx.collClass(this, function () {
	                       return IDBKeyRange.only(value);
	                   });
	               },
	               above: function (value) {
	                   return new this._ctx.collClass(this, function () {
	                       return IDBKeyRange.lowerBound(value, true);
	                   });
	               },
	               aboveOrEqual: function (value) {
	                   return new this._ctx.collClass(this, function () {
	                       return IDBKeyRange.lowerBound(value);
	                   });
	               },
	               below: function (value) {
	                   return new this._ctx.collClass(this, function () {
	                       return IDBKeyRange.upperBound(value, true);
	                   });
	               },
	               belowOrEqual: function (value) {
	                   return new this._ctx.collClass(this, function () {
	                       return IDBKeyRange.upperBound(value);
	                   });
	               },
	               startsWith: function (str) {
	                   /// <param name="str" type="String"></param>
	                   if (typeof str !== 'string') return fail(this, STRING_EXPECTED);
	                   return this.between(str, str + maxString, true, true);
	               },
	               startsWithIgnoreCase: function (str) {
	                   /// <param name="str" type="String"></param>
	                   if (str === "") return this.startsWith(str);
	                   return addIgnoreCaseAlgorithm(this, function (x, a) {
	                       return x.indexOf(a[0]) === 0;
	                   }, [str], maxString);
	               },
	               equalsIgnoreCase: function (str) {
	                   /// <param name="str" type="String"></param>
	                   return addIgnoreCaseAlgorithm(this, function (x, a) {
	                       return x === a[0];
	                   }, [str], "");
	               },
	               anyOfIgnoreCase: function () {
	                   var set = getArrayOf.apply(NO_CHAR_ARRAY, arguments);
	                   if (set.length === 0) return emptyCollection(this);
	                   return addIgnoreCaseAlgorithm(this, function (x, a) {
	                       return a.indexOf(x) !== -1;
	                   }, set, "");
	               },
	               startsWithAnyOfIgnoreCase: function () {
	                   var set = getArrayOf.apply(NO_CHAR_ARRAY, arguments);
	                   if (set.length === 0) return emptyCollection(this);
	                   return addIgnoreCaseAlgorithm(this, function (x, a) {
	                       return a.some(function (n) {
	                           return x.indexOf(n) === 0;
	                       });
	                   }, set, maxString);
	               },
	               anyOf: function () {
	                   var set = getArrayOf.apply(NO_CHAR_ARRAY, arguments);
	                   var compare = ascending;
	                   try {
	                       set.sort(compare);
	                   } catch (e) {
	                       return fail(this, INVALID_KEY_ARGUMENT);
	                   }
	                   if (set.length === 0) return emptyCollection(this);
	                   var c = new this._ctx.collClass(this, function () {
	                       return IDBKeyRange.bound(set[0], set[set.length - 1]);
	                   });
	
	                   c._ondirectionchange = function (direction) {
	                       compare = direction === "next" ? ascending : descending;
	                       set.sort(compare);
	                   };
	                   var i = 0;
	                   c._addAlgorithm(function (cursor, advance, resolve) {
	                       var key = cursor.key;
	                       while (compare(key, set[i]) > 0) {
	                           // The cursor has passed beyond this key. Check next.
	                           ++i;
	                           if (i === set.length) {
	                               // There is no next. Stop searching.
	                               advance(resolve);
	                               return false;
	                           }
	                       }
	                       if (compare(key, set[i]) === 0) {
	                           // The current cursor value should be included and we should continue a single step in case next item has the same key or possibly our next key in set.
	                           return true;
	                       } else {
	                           // cursor.key not yet at set[i]. Forward cursor to the next key to hunt for.
	                           advance(function () {
	                               cursor.continue(set[i]);
	                           });
	                           return false;
	                       }
	                   });
	                   return c;
	               },
	
	               notEqual: function (value) {
	                   return this.inAnyRange([[-Infinity, value], [value, maxKey]], { includeLowers: false, includeUppers: false });
	               },
	
	               noneOf: function () {
	                   var set = getArrayOf.apply(NO_CHAR_ARRAY, arguments);
	                   if (set.length === 0) return new this._ctx.collClass(this); // Return entire collection.
	                   try {
	                       set.sort(ascending);
	                   } catch (e) {
	                       return fail(this, INVALID_KEY_ARGUMENT);
	                   }
	                   // Transform ["a","b","c"] to a set of ranges for between/above/below: [[-Infinity,"a"], ["a","b"], ["b","c"], ["c",maxKey]]
	                   var ranges = set.reduce(function (res, val) {
	                       return res ? res.concat([[res[res.length - 1][1], val]]) : [[-Infinity, val]];
	                   }, null);
	                   ranges.push([set[set.length - 1], maxKey]);
	                   return this.inAnyRange(ranges, { includeLowers: false, includeUppers: false });
	               },
	
	               /** Filter out values withing given set of ranges.
	               * Example, give children and elders a rebate of 50%:
	               *
	               *   db.friends.where('age').inAnyRange([[0,18],[65,Infinity]]).modify({Rebate: 1/2});
	               *
	               * @param {(string|number|Date|Array)[][]} ranges
	               * @param {{includeLowers: boolean, includeUppers: boolean}} options
	               */
	               inAnyRange: function (ranges, options) {
	                   var ctx = this._ctx;
	                   if (ranges.length === 0) return emptyCollection(this);
	                   if (!ranges.every(function (range) {
	                       return range[0] !== undefined && range[1] !== undefined && ascending(range[0], range[1]) <= 0;
	                   })) {
	                       return fail(this, "First argument to inAnyRange() must be an Array of two-value Arrays [lower,upper] where upper must not be lower than lower", exceptions.InvalidArgument);
	                   }
	                   var includeLowers = !options || options.includeLowers !== false; // Default to true
	                   var includeUppers = options && options.includeUppers === true; // Default to false
	
	                   function addRange(ranges, newRange) {
	                       for (var i = 0, l = ranges.length; i < l; ++i) {
	                           var range = ranges[i];
	                           if (cmp(newRange[0], range[1]) < 0 && cmp(newRange[1], range[0]) > 0) {
	                               range[0] = min(range[0], newRange[0]);
	                               range[1] = max(range[1], newRange[1]);
	                               break;
	                           }
	                       }
	                       if (i === l) ranges.push(newRange);
	                       return ranges;
	                   }
	
	                   var sortDirection = ascending;
	                   function rangeSorter(a, b) {
	                       return sortDirection(a[0], b[0]);
	                   }
	
	                   // Join overlapping ranges
	                   var set;
	                   try {
	                       set = ranges.reduce(addRange, []);
	                       set.sort(rangeSorter);
	                   } catch (ex) {
	                       return fail(this, INVALID_KEY_ARGUMENT);
	                   }
	
	                   var i = 0;
	                   var keyIsBeyondCurrentEntry = includeUppers ? function (key) {
	                       return ascending(key, set[i][1]) > 0;
	                   } : function (key) {
	                       return ascending(key, set[i][1]) >= 0;
	                   };
	
	                   var keyIsBeforeCurrentEntry = includeLowers ? function (key) {
	                       return descending(key, set[i][0]) > 0;
	                   } : function (key) {
	                       return descending(key, set[i][0]) >= 0;
	                   };
	
	                   function keyWithinCurrentRange(key) {
	                       return !keyIsBeyondCurrentEntry(key) && !keyIsBeforeCurrentEntry(key);
	                   }
	
	                   var checkKey = keyIsBeyondCurrentEntry;
	
	                   var c = new ctx.collClass(this, function () {
	                       return IDBKeyRange.bound(set[0][0], set[set.length - 1][1], !includeLowers, !includeUppers);
	                   });
	
	                   c._ondirectionchange = function (direction) {
	                       if (direction === "next") {
	                           checkKey = keyIsBeyondCurrentEntry;
	                           sortDirection = ascending;
	                       } else {
	                           checkKey = keyIsBeforeCurrentEntry;
	                           sortDirection = descending;
	                       }
	                       set.sort(rangeSorter);
	                   };
	
	                   c._addAlgorithm(function (cursor, advance, resolve) {
	                       var key = cursor.key;
	                       while (checkKey(key)) {
	                           // The cursor has passed beyond this key. Check next.
	                           ++i;
	                           if (i === set.length) {
	                               // There is no next. Stop searching.
	                               advance(resolve);
	                               return false;
	                           }
	                       }
	                       if (keyWithinCurrentRange(key)) {
	                           // The current cursor value should be included and we should continue a single step in case next item has the same key or possibly our next key in set.
	                           return true;
	                       } else if (cmp(key, set[i][1]) === 0 || cmp(key, set[i][0]) === 0) {
	                           // includeUpper or includeLower is false so keyWithinCurrentRange() returns false even though we are at range border.
	                           // Continue to next key but don't include this one.
	                           return false;
	                       } else {
	                           // cursor.key not yet at set[i]. Forward cursor to the next key to hunt for.
	                           advance(function () {
	                               if (sortDirection === ascending) cursor.continue(set[i][0]);else cursor.continue(set[i][1]);
	                           });
	                           return false;
	                       }
	                   });
	                   return c;
	               },
	               startsWithAnyOf: function () {
	                   var set = getArrayOf.apply(NO_CHAR_ARRAY, arguments);
	
	                   if (!set.every(function (s) {
	                       return typeof s === 'string';
	                   })) {
	                       return fail(this, "startsWithAnyOf() only works with strings");
	                   }
	                   if (set.length === 0) return emptyCollection(this);
	
	                   return this.inAnyRange(set.map(function (str) {
	                       return [str, str + maxString];
	                   }));
	               }
	           };
	       });
	
	       //
	       //
	       //
	       // Collection Class
	       //
	       //
	       //
	       function Collection(whereClause, keyRangeGenerator) {
	           /// <summary>
	           ///
	           /// </summary>
	           /// <param name="whereClause" type="WhereClause">Where clause instance</param>
	           /// <param name="keyRangeGenerator" value="function(){ return IDBKeyRange.bound(0,1);}" optional="true"></param>
	           var keyRange = null,
	               error = null;
	           if (keyRangeGenerator) try {
	               keyRange = keyRangeGenerator();
	           } catch (ex) {
	               error = ex;
	           }
	
	           var whereCtx = whereClause._ctx,
	               table = whereCtx.table;
	           this._ctx = {
	               table: table,
	               index: whereCtx.index,
	               isPrimKey: !whereCtx.index || table.schema.primKey.keyPath && whereCtx.index === table.schema.primKey.name,
	               range: keyRange,
	               keysOnly: false,
	               dir: "next",
	               unique: "",
	               algorithm: null,
	               filter: null,
	               replayFilter: null,
	               justLimit: true, // True if a replayFilter is just a filter that performs a "limit" operation (or none at all)
	               isMatch: null,
	               offset: 0,
	               limit: Infinity,
	               error: error, // If set, any promise must be rejected with this error
	               or: whereCtx.or,
	               valueMapper: table.hook.reading.fire
	           };
	       }
	
	       function isPlainKeyRange(ctx, ignoreLimitFilter) {
	           return !(ctx.filter || ctx.algorithm || ctx.or) && (ignoreLimitFilter ? ctx.justLimit : !ctx.replayFilter);
	       }
	
	       props(Collection.prototype, function () {
	
	           //
	           // Collection Private Functions
	           //
	
	           function addFilter(ctx, fn) {
	               ctx.filter = combine(ctx.filter, fn);
	           }
	
	           function addReplayFilter(ctx, factory, isLimitFilter) {
	               var curr = ctx.replayFilter;
	               ctx.replayFilter = curr ? function () {
	                   return combine(curr(), factory());
	               } : factory;
	               ctx.justLimit = isLimitFilter && !curr;
	           }
	
	           function addMatchFilter(ctx, fn) {
	               ctx.isMatch = combine(ctx.isMatch, fn);
	           }
	
	           /** @param ctx {
	            *      isPrimKey: boolean,
	            *      table: Table,
	            *      index: string
	            * }
	            * @param store IDBObjectStore
	            **/
	           function getIndexOrStore(ctx, store) {
	               if (ctx.isPrimKey) return store;
	               var indexSpec = ctx.table.schema.idxByName[ctx.index];
	               if (!indexSpec) throw new exceptions.Schema("KeyPath " + ctx.index + " on object store " + store.name + " is not indexed");
	               return store.index(indexSpec.name);
	           }
	
	           /** @param ctx {
	            *      isPrimKey: boolean,
	            *      table: Table,
	            *      index: string,
	            *      keysOnly: boolean,
	            *      range?: IDBKeyRange,
	            *      dir: "next" | "prev"
	            * }
	            */
	           function openCursor(ctx, store) {
	               var idxOrStore = getIndexOrStore(ctx, store);
	               return ctx.keysOnly && 'openKeyCursor' in idxOrStore ? idxOrStore.openKeyCursor(ctx.range || null, ctx.dir + ctx.unique) : idxOrStore.openCursor(ctx.range || null, ctx.dir + ctx.unique);
	           }
	
	           function iter(ctx, fn, resolve, reject, idbstore) {
	               var filter = ctx.replayFilter ? combine(ctx.filter, ctx.replayFilter()) : ctx.filter;
	               if (!ctx.or) {
	                   iterate(openCursor(ctx, idbstore), combine(ctx.algorithm, filter), fn, resolve, reject, !ctx.keysOnly && ctx.valueMapper);
	               } else (function () {
	                   var set = {};
	                   var resolved = 0;
	
	                   function resolveboth() {
	                       if (++resolved === 2) resolve(); // Seems like we just support or btwn max 2 expressions, but there are no limit because we do recursion.
	                   }
	
	                   function union(item, cursor, advance) {
	                       if (!filter || filter(cursor, advance, resolveboth, reject)) {
	                           var key = cursor.primaryKey.toString(); // Converts any Date to String, String to String, Number to String and Array to comma-separated string
	                           if (!hasOwn(set, key)) {
	                               set[key] = true;
	                               fn(item, cursor, advance);
	                           }
	                       }
	                   }
	
	                   ctx.or._iterate(union, resolveboth, reject, idbstore);
	                   iterate(openCursor(ctx, idbstore), ctx.algorithm, union, resolveboth, reject, !ctx.keysOnly && ctx.valueMapper);
	               })();
	           }
	           function getInstanceTemplate(ctx) {
	               return ctx.table.schema.instanceTemplate;
	           }
	
	           return {
	
	               //
	               // Collection Protected Functions
	               //
	
	               _read: function (fn, cb) {
	                   var ctx = this._ctx;
	                   if (ctx.error) return ctx.table._trans(null, function rejector(resolve, reject) {
	                       reject(ctx.error);
	                   });else return ctx.table._idbstore(READONLY, fn).then(cb);
	               },
	               _write: function (fn) {
	                   var ctx = this._ctx;
	                   if (ctx.error) return ctx.table._trans(null, function rejector(resolve, reject) {
	                       reject(ctx.error);
	                   });else return ctx.table._idbstore(READWRITE, fn, "locked"); // When doing write operations on collections, always lock the operation so that upcoming operations gets queued.
	               },
	               _addAlgorithm: function (fn) {
	                   var ctx = this._ctx;
	                   ctx.algorithm = combine(ctx.algorithm, fn);
	               },
	
	               _iterate: function (fn, resolve, reject, idbstore) {
	                   return iter(this._ctx, fn, resolve, reject, idbstore);
	               },
	
	               clone: function (props) {
	                   var rv = Object.create(this.constructor.prototype),
	                       ctx = Object.create(this._ctx);
	                   if (props) extend(ctx, props);
	                   rv._ctx = ctx;
	                   return rv;
	               },
	
	               raw: function () {
	                   this._ctx.valueMapper = null;
	                   return this;
	               },
	
	               //
	               // Collection Public methods
	               //
	
	               each: function (fn) {
	                   var ctx = this._ctx;
	
	                   if (fake) {
	                       var item = getInstanceTemplate(ctx),
	                           primKeyPath = ctx.table.schema.primKey.keyPath,
	                           key = getByKeyPath(item, ctx.index ? ctx.table.schema.idxByName[ctx.index].keyPath : primKeyPath),
	                           primaryKey = getByKeyPath(item, primKeyPath);
	                       fn(item, { key: key, primaryKey: primaryKey });
	                   }
	
	                   return this._read(function (resolve, reject, idbstore) {
	                       iter(ctx, fn, resolve, reject, idbstore);
	                   });
	               },
	
	               count: function (cb) {
	                   if (fake) return Promise.resolve(0).then(cb);
	                   var ctx = this._ctx;
	
	                   if (isPlainKeyRange(ctx, true)) {
	                       // This is a plain key range. We can use the count() method if the index.
	                       return this._read(function (resolve, reject, idbstore) {
	                           var idx = getIndexOrStore(ctx, idbstore);
	                           var req = ctx.range ? idx.count(ctx.range) : idx.count();
	                           req.onerror = eventRejectHandler(reject);
	                           req.onsuccess = function (e) {
	                               resolve(Math.min(e.target.result, ctx.limit));
	                           };
	                       }, cb);
	                   } else {
	                       // Algorithms, filters or expressions are applied. Need to count manually.
	                       var count = 0;
	                       return this._read(function (resolve, reject, idbstore) {
	                           iter(ctx, function () {
	                               ++count;return false;
	                           }, function () {
	                               resolve(count);
	                           }, reject, idbstore);
	                       }, cb);
	                   }
	               },
	
	               sortBy: function (keyPath, cb) {
	                   /// <param name="keyPath" type="String"></param>
	                   var parts = keyPath.split('.').reverse(),
	                       lastPart = parts[0],
	                       lastIndex = parts.length - 1;
	                   function getval(obj, i) {
	                       if (i) return getval(obj[parts[i]], i - 1);
	                       return obj[lastPart];
	                   }
	                   var order = this._ctx.dir === "next" ? 1 : -1;
	
	                   function sorter(a, b) {
	                       var aVal = getval(a, lastIndex),
	                           bVal = getval(b, lastIndex);
	                       return aVal < bVal ? -order : aVal > bVal ? order : 0;
	                   }
	                   return this.toArray(function (a) {
	                       return a.sort(sorter);
	                   }).then(cb);
	               },
	
	               toArray: function (cb) {
	                   var ctx = this._ctx;
	                   return this._read(function (resolve, reject, idbstore) {
	                       fake && resolve([getInstanceTemplate(ctx)]);
	                       if (hasGetAll && ctx.dir === 'next' && isPlainKeyRange(ctx, true) && ctx.limit > 0) {
	                           // Special optimation if we could use IDBObjectStore.getAll() or
	                           // IDBKeyRange.getAll():
	                           var readingHook = ctx.table.hook.reading.fire;
	                           var idxOrStore = getIndexOrStore(ctx, idbstore);
	                           var req = ctx.limit < Infinity ? idxOrStore.getAll(ctx.range, ctx.limit) : idxOrStore.getAll(ctx.range);
	                           req.onerror = eventRejectHandler(reject);
	                           req.onsuccess = readingHook === mirror ? eventSuccessHandler(resolve) : wrap(eventSuccessHandler(function (res) {
	                               resolve(res.map(readingHook));
	                           }));
	                       } else {
	                           // Getting array through a cursor.
	                           var a = [];
	                           iter(ctx, function (item) {
	                               a.push(item);
	                           }, function arrayComplete() {
	                               resolve(a);
	                           }, reject, idbstore);
	                       }
	                   }, cb);
	               },
	
	               offset: function (offset) {
	                   var ctx = this._ctx;
	                   if (offset <= 0) return this;
	                   ctx.offset += offset; // For count()
	                   if (isPlainKeyRange(ctx)) {
	                       addReplayFilter(ctx, function () {
	                           var offsetLeft = offset;
	                           return function (cursor, advance) {
	                               if (offsetLeft === 0) return true;
	                               if (offsetLeft === 1) {
	                                   --offsetLeft;return false;
	                               }
	                               advance(function () {
	                                   cursor.advance(offsetLeft);
	                                   offsetLeft = 0;
	                               });
	                               return false;
	                           };
	                       });
	                   } else {
	                       addReplayFilter(ctx, function () {
	                           var offsetLeft = offset;
	                           return function () {
	                               return --offsetLeft < 0;
	                           };
	                       });
	                   }
	                   return this;
	               },
	
	               limit: function (numRows) {
	                   this._ctx.limit = Math.min(this._ctx.limit, numRows); // For count()
	                   addReplayFilter(this._ctx, function () {
	                       var rowsLeft = numRows;
	                       return function (cursor, advance, resolve) {
	                           if (--rowsLeft <= 0) advance(resolve); // Stop after this item has been included
	                           return rowsLeft >= 0; // If numRows is already below 0, return false because then 0 was passed to numRows initially. Otherwise we wouldnt come here.
	                       };
	                   }, true);
	                   return this;
	               },
	
	               until: function (filterFunction, bIncludeStopEntry) {
	                   var ctx = this._ctx;
	                   fake && filterFunction(getInstanceTemplate(ctx));
	                   addFilter(this._ctx, function (cursor, advance, resolve) {
	                       if (filterFunction(cursor.value)) {
	                           advance(resolve);
	                           return bIncludeStopEntry;
	                       } else {
	                           return true;
	                       }
	                   });
	                   return this;
	               },
	
	               first: function (cb) {
	                   return this.limit(1).toArray(function (a) {
	                       return a[0];
	                   }).then(cb);
	               },
	
	               last: function (cb) {
	                   return this.reverse().first(cb);
	               },
	
	               filter: function (filterFunction) {
	                   /// <param name="jsFunctionFilter" type="Function">function(val){return true/false}</param>
	                   fake && filterFunction(getInstanceTemplate(this._ctx));
	                   addFilter(this._ctx, function (cursor) {
	                       return filterFunction(cursor.value);
	                   });
	                   // match filters not used in Dexie.js but can be used by 3rd part libraries to test a
	                   // collection for a match without querying DB. Used by Dexie.Observable.
	                   addMatchFilter(this._ctx, filterFunction);
	                   return this;
	               },
	
	               and: function (filterFunction) {
	                   return this.filter(filterFunction);
	               },
	
	               or: function (indexName) {
	                   return new WhereClause(this._ctx.table, indexName, this);
	               },
	
	               reverse: function () {
	                   this._ctx.dir = this._ctx.dir === "prev" ? "next" : "prev";
	                   if (this._ondirectionchange) this._ondirectionchange(this._ctx.dir);
	                   return this;
	               },
	
	               desc: function () {
	                   return this.reverse();
	               },
	
	               eachKey: function (cb) {
	                   var ctx = this._ctx;
	                   ctx.keysOnly = !ctx.isMatch;
	                   return this.each(function (val, cursor) {
	                       cb(cursor.key, cursor);
	                   });
	               },
	
	               eachUniqueKey: function (cb) {
	                   this._ctx.unique = "unique";
	                   return this.eachKey(cb);
	               },
	
	               eachPrimaryKey: function (cb) {
	                   var ctx = this._ctx;
	                   ctx.keysOnly = !ctx.isMatch;
	                   return this.each(function (val, cursor) {
	                       cb(cursor.primaryKey, cursor);
	                   });
	               },
	
	               keys: function (cb) {
	                   var ctx = this._ctx;
	                   ctx.keysOnly = !ctx.isMatch;
	                   var a = [];
	                   return this.each(function (item, cursor) {
	                       a.push(cursor.key);
	                   }).then(function () {
	                       return a;
	                   }).then(cb);
	               },
	
	               primaryKeys: function (cb) {
	                   var ctx = this._ctx;
	                   if (hasGetAll && ctx.dir === 'next' && isPlainKeyRange(ctx, true) && ctx.limit > 0) {
	                       // Special optimation if we could use IDBObjectStore.getAllKeys() or
	                       // IDBKeyRange.getAllKeys():
	                       return this._read(function (resolve, reject, idbstore) {
	                           var idxOrStore = getIndexOrStore(ctx, idbstore);
	                           var req = ctx.limit < Infinity ? idxOrStore.getAllKeys(ctx.range, ctx.limit) : idxOrStore.getAllKeys(ctx.range);
	                           req.onerror = eventRejectHandler(reject);
	                           req.onsuccess = eventSuccessHandler(resolve);
	                       }).then(cb);
	                   }
	                   ctx.keysOnly = !ctx.isMatch;
	                   var a = [];
	                   return this.each(function (item, cursor) {
	                       a.push(cursor.primaryKey);
	                   }).then(function () {
	                       return a;
	                   }).then(cb);
	               },
	
	               uniqueKeys: function (cb) {
	                   this._ctx.unique = "unique";
	                   return this.keys(cb);
	               },
	
	               firstKey: function (cb) {
	                   return this.limit(1).keys(function (a) {
	                       return a[0];
	                   }).then(cb);
	               },
	
	               lastKey: function (cb) {
	                   return this.reverse().firstKey(cb);
	               },
	
	               distinct: function () {
	                   var ctx = this._ctx,
	                       idx = ctx.index && ctx.table.schema.idxByName[ctx.index];
	                   if (!idx || !idx.multi) return this; // distinct() only makes differencies on multiEntry indexes.
	                   var set = {};
	                   addFilter(this._ctx, function (cursor) {
	                       var strKey = cursor.primaryKey.toString(); // Converts any Date to String, String to String, Number to String and Array to comma-separated string
	                       var found = hasOwn(set, strKey);
	                       set[strKey] = true;
	                       return !found;
	                   });
	                   return this;
	               }
	           };
	       });
	
	       //
	       //
	       // WriteableCollection Class
	       //
	       //
	       function WriteableCollection() {
	           Collection.apply(this, arguments);
	       }
	
	       derive(WriteableCollection).from(Collection).extend({
	
	           //
	           // WriteableCollection Public Methods
	           //
	
	           modify: function (changes) {
	               var self = this,
	                   ctx = this._ctx,
	                   hook = ctx.table.hook,
	                   updatingHook = hook.updating.fire,
	                   deletingHook = hook.deleting.fire;
	
	               fake && typeof changes === 'function' && changes.call({ value: ctx.table.schema.instanceTemplate }, ctx.table.schema.instanceTemplate);
	
	               return this._write(function (resolve, reject, idbstore, trans) {
	                   var modifyer;
	                   if (typeof changes === 'function') {
	                       // Changes is a function that may update, add or delete propterties or even require a deletion the object itself (delete this.item)
	                       if (updatingHook === nop && deletingHook === nop) {
	                           // Noone cares about what is being changed. Just let the modifier function be the given argument as is.
	                           modifyer = changes;
	                       } else {
	                           // People want to know exactly what is being modified or deleted.
	                           // Let modifyer be a proxy function that finds out what changes the caller is actually doing
	                           // and call the hooks accordingly!
	                           modifyer = function (item) {
	                               var origItem = deepClone(item); // Clone the item first so we can compare laters.
	                               if (changes.call(this, item, this) === false) return false; // Call the real modifyer function (If it returns false explicitely, it means it dont want to modify anyting on this object)
	                               if (!hasOwn(this, "value")) {
	                                   // The real modifyer function requests a deletion of the object. Inform the deletingHook that a deletion is taking place.
	                                   deletingHook.call(this, this.primKey, item, trans);
	                               } else {
	                                   // No deletion. Check what was changed
	                                   var objectDiff = getObjectDiff(origItem, this.value);
	                                   var additionalChanges = updatingHook.call(this, objectDiff, this.primKey, origItem, trans);
	                                   if (additionalChanges) {
	                                       // Hook want to apply additional modifications. Make sure to fullfill the will of the hook.
	                                       item = this.value;
	                                       keys(additionalChanges).forEach(function (keyPath) {
	                                           setByKeyPath(item, keyPath, additionalChanges[keyPath]); // Adding {keyPath: undefined} means that the keyPath should be deleted. Handled by setByKeyPath
	                                       });
	                                   }
	                               }
	                           };
	                       }
	                   } else if (updatingHook === nop) {
	                           // changes is a set of {keyPath: value} and no one is listening to the updating hook.
	                           var keyPaths = keys(changes);
	                           var numKeys = keyPaths.length;
	                           modifyer = function (item) {
	                               var anythingModified = false;
	                               for (var i = 0; i < numKeys; ++i) {
	                                   var keyPath = keyPaths[i],
	                                       val = changes[keyPath];
	                                   if (getByKeyPath(item, keyPath) !== val) {
	                                       setByKeyPath(item, keyPath, val); // Adding {keyPath: undefined} means that the keyPath should be deleted. Handled by setByKeyPath
	                                       anythingModified = true;
	                                   }
	                               }
	                               return anythingModified;
	                           };
	                       } else {
	                           // changes is a set of {keyPath: value} and people are listening to the updating hook so we need to call it and
	                           // allow it to add additional modifications to make.
	                           var origChanges = changes;
	                           changes = shallowClone(origChanges); // Let's work with a clone of the changes keyPath/value set so that we can restore it in case a hook extends it.
	                           modifyer = function (item) {
	                               var anythingModified = false;
	                               var additionalChanges = updatingHook.call(this, changes, this.primKey, deepClone(item), trans);
	                               if (additionalChanges) extend(changes, additionalChanges);
	                               keys(changes).forEach(function (keyPath) {
	                                   var val = changes[keyPath];
	                                   if (getByKeyPath(item, keyPath) !== val) {
	                                       setByKeyPath(item, keyPath, val);
	                                       anythingModified = true;
	                                   }
	                               });
	                               if (additionalChanges) changes = shallowClone(origChanges); // Restore original changes for next iteration
	                               return anythingModified;
	                           };
	                       }
	
	                   var count = 0;
	                   var successCount = 0;
	                   var iterationComplete = false;
	                   var failures = [];
	                   var failKeys = [];
	                   var currentKey = null;
	
	                   function modifyItem(item, cursor) {
	                       currentKey = cursor.primaryKey;
	                       var thisContext = {
	                           primKey: cursor.primaryKey,
	                           value: item,
	                           onsuccess: null,
	                           onerror: null
	                       };
	
	                       function onerror(e) {
	                           failures.push(e);
	                           failKeys.push(thisContext.primKey);
	                           checkFinished();
	                           return true; // Catch these errors and let a final rejection decide whether or not to abort entire transaction
	                       }
	
	                       if (modifyer.call(thisContext, item, thisContext) !== false) {
	                           // If a callback explicitely returns false, do not perform the update!
	                           var bDelete = !hasOwn(thisContext, "value");
	                           ++count;
	                           tryCatch(function () {
	                               var req = bDelete ? cursor.delete() : cursor.update(thisContext.value);
	                               req._hookCtx = thisContext;
	                               req.onerror = hookedEventRejectHandler(onerror);
	                               req.onsuccess = hookedEventSuccessHandler(function () {
	                                   ++successCount;
	                                   checkFinished();
	                               });
	                           }, onerror);
	                       } else if (thisContext.onsuccess) {
	                           // Hook will expect either onerror or onsuccess to always be called!
	                           thisContext.onsuccess(thisContext.value);
	                       }
	                   }
	
	                   function doReject(e) {
	                       if (e) {
	                           failures.push(e);
	                           failKeys.push(currentKey);
	                       }
	                       return reject(new ModifyError("Error modifying one or more objects", failures, successCount, failKeys));
	                   }
	
	                   function checkFinished() {
	                       if (iterationComplete && successCount + failures.length === count) {
	                           if (failures.length > 0) doReject();else resolve(successCount);
	                       }
	                   }
	                   self.clone().raw()._iterate(modifyItem, function () {
	                       iterationComplete = true;
	                       checkFinished();
	                   }, doReject, idbstore);
	               });
	           },
	
	           'delete': function () {
	               var _this4 = this;
	
	               var ctx = this._ctx,
	                   range = ctx.range,
	                   deletingHook = ctx.table.hook.deleting.fire,
	                   hasDeleteHook = deletingHook !== nop;
	               if (!hasDeleteHook && isPlainKeyRange(ctx) && (ctx.isPrimKey && !hangsOnDeleteLargeKeyRange || !range)) // if no range, we'll use clear().
	                   {
	                       // May use IDBObjectStore.delete(IDBKeyRange) in this case (Issue #208)
	                       // For chromium, this is the way most optimized version.
	                       // For IE/Edge, this could hang the indexedDB engine and make operating system instable
	                       // (https://gist.github.com/dfahlander/5a39328f029de18222cf2125d56c38f7)
	                       return this._write(function (resolve, reject, idbstore) {
	                           // Our API contract is to return a count of deleted items, so we have to count() before delete().
	                           var onerror = eventRejectHandler(reject),
	                               countReq = range ? idbstore.count(range) : idbstore.count();
	                           countReq.onerror = onerror;
	                           countReq.onsuccess = function () {
	                               var count = countReq.result;
	                               tryCatch(function () {
	                                   var delReq = range ? idbstore.delete(range) : idbstore.clear();
	                                   delReq.onerror = onerror;
	                                   delReq.onsuccess = function () {
	                                       return resolve(count);
	                                   };
	                               }, function (err) {
	                                   return reject(err);
	                               });
	                           };
	                       });
	                   }
	
	               // Default version to use when collection is not a vanilla IDBKeyRange on the primary key.
	               // Divide into chunks to not starve RAM.
	               // If has delete hook, we will have to collect not just keys but also objects, so it will use
	               // more memory and need lower chunk size.
	               var CHUNKSIZE = hasDeleteHook ? 2000 : 10000;
	
	               return this._write(function (resolve, reject, idbstore, trans) {
	                   var totalCount = 0;
	                   // Clone collection and change its table and set a limit of CHUNKSIZE on the cloned Collection instance.
	                   var collection = _this4.clone({
	                       keysOnly: !ctx.isMatch && !hasDeleteHook }) // load just keys (unless filter() or and() or deleteHook has subscribers)
	                   .distinct() // In case multiEntry is used, never delete same key twice because resulting count
	                   // would become larger than actual delete count.
	                   .limit(CHUNKSIZE).raw(); // Don't filter through reading-hooks (like mapped classes etc)
	
	                   var keysOrTuples = [];
	
	                   // We're gonna do things on as many chunks that are needed.
	                   // Use recursion of nextChunk function:
	                   var nextChunk = function () {
	                       return collection.each(hasDeleteHook ? function (val, cursor) {
	                           // Somebody subscribes to hook('deleting'). Collect all primary keys and their values,
	                           // so that the hook can be called with its values in bulkDelete().
	                           keysOrTuples.push([cursor.primaryKey, cursor.value]);
	                       } : function (val, cursor) {
	                           // No one subscribes to hook('deleting'). Collect only primary keys:
	                           keysOrTuples.push(cursor.primaryKey);
	                       }).then(function () {
	                           // Chromium deletes faster when doing it in sort order.
	                           hasDeleteHook ? keysOrTuples.sort(function (a, b) {
	                               return ascending(a[0], b[0]);
	                           }) : keysOrTuples.sort(ascending);
	                           return bulkDelete(idbstore, trans, keysOrTuples, hasDeleteHook, deletingHook);
	                       }).then(function () {
	                           var count = keysOrTuples.length;
	                           totalCount += count;
	                           keysOrTuples = [];
	                           return count < CHUNKSIZE ? totalCount : nextChunk();
	                       });
	                   };
	
	                   resolve(nextChunk());
	               });
	           }
	       });
	
	       //
	       //
	       //
	       // ------------------------- Help functions ---------------------------
	       //
	       //
	       //
	
	       function lowerVersionFirst(a, b) {
	           return a._cfg.version - b._cfg.version;
	       }
	
	       function setApiOnPlace(objs, tableNames, mode, dbschema) {
	           tableNames.forEach(function (tableName) {
	               var tableInstance = db._tableFactory(mode, dbschema[tableName]);
	               objs.forEach(function (obj) {
	                   tableName in obj || (obj[tableName] = tableInstance);
	               });
	           });
	       }
	
	       function removeTablesApi(objs) {
	           objs.forEach(function (obj) {
	               for (var key in obj) {
	                   if (obj[key] instanceof Table) delete obj[key];
	               }
	           });
	       }
	
	       function iterate(req, filter, fn, resolve, reject, valueMapper) {
	
	           // Apply valueMapper (hook('reading') or mappped class)
	           var mappedFn = valueMapper ? function (x, c, a) {
	               return fn(valueMapper(x), c, a);
	           } : fn;
	           // Wrap fn with PSD and microtick stuff from Promise.
	           var wrappedFn = wrap(mappedFn, reject);
	
	           if (!req.onerror) req.onerror = eventRejectHandler(reject);
	           if (filter) {
	               req.onsuccess = trycatcher(function filter_record() {
	                   var cursor = req.result;
	                   if (cursor) {
	                       var c = function () {
	                           cursor.continue();
	                       };
	                       if (filter(cursor, function (advancer) {
	                           c = advancer;
	                       }, resolve, reject)) wrappedFn(cursor.value, cursor, function (advancer) {
	                           c = advancer;
	                       });
	                       c();
	                   } else {
	                       resolve();
	                   }
	               }, reject);
	           } else {
	               req.onsuccess = trycatcher(function filter_record() {
	                   var cursor = req.result;
	                   if (cursor) {
	                       var c = function () {
	                           cursor.continue();
	                       };
	                       wrappedFn(cursor.value, cursor, function (advancer) {
	                           c = advancer;
	                       });
	                       c();
	                   } else {
	                       resolve();
	                   }
	               }, reject);
	           }
	       }
	
	       function parseIndexSyntax(indexes) {
	           /// <param name="indexes" type="String"></param>
	           /// <returns type="Array" elementType="IndexSpec"></returns>
	           var rv = [];
	           indexes.split(',').forEach(function (index) {
	               index = index.trim();
	               var name = index.replace(/([&*]|\+\+)/g, ""); // Remove "&", "++" and "*"
	               // Let keyPath of "[a+b]" be ["a","b"]:
	               var keyPath = /^\[/.test(name) ? name.match(/^\[(.*)\]$/)[1].split('+') : name;
	
	               rv.push(new IndexSpec(name, keyPath || null, /\&/.test(index), /\*/.test(index), /\+\+/.test(index), isArray(keyPath), /\./.test(index)));
	           });
	           return rv;
	       }
	
	       function cmp(key1, key2) {
	           return indexedDB.cmp(key1, key2);
	       }
	
	       function min(a, b) {
	           return cmp(a, b) < 0 ? a : b;
	       }
	
	       function max(a, b) {
	           return cmp(a, b) > 0 ? a : b;
	       }
	
	       function ascending(a, b) {
	           return indexedDB.cmp(a, b);
	       }
	
	       function descending(a, b) {
	           return indexedDB.cmp(b, a);
	       }
	
	       function simpleCompare(a, b) {
	           return a < b ? -1 : a === b ? 0 : 1;
	       }
	
	       function simpleCompareReverse(a, b) {
	           return a > b ? -1 : a === b ? 0 : 1;
	       }
	
	       function combine(filter1, filter2) {
	           return filter1 ? filter2 ? function () {
	               return filter1.apply(this, arguments) && filter2.apply(this, arguments);
	           } : filter1 : filter2;
	       }
	
	       function readGlobalSchema() {
	           db.verno = idbdb.version / 10;
	           db._dbSchema = globalSchema = {};
	           dbStoreNames = slice(idbdb.objectStoreNames, 0);
	           if (dbStoreNames.length === 0) return; // Database contains no stores.
	           var trans = idbdb.transaction(safariMultiStoreFix(dbStoreNames), 'readonly');
	           dbStoreNames.forEach(function (storeName) {
	               var store = trans.objectStore(storeName),
	                   keyPath = store.keyPath,
	                   dotted = keyPath && typeof keyPath === 'string' && keyPath.indexOf('.') !== -1;
	               var primKey = new IndexSpec(keyPath, keyPath || "", false, false, !!store.autoIncrement, keyPath && typeof keyPath !== 'string', dotted);
	               var indexes = [];
	               for (var j = 0; j < store.indexNames.length; ++j) {
	                   var idbindex = store.index(store.indexNames[j]);
	                   keyPath = idbindex.keyPath;
	                   dotted = keyPath && typeof keyPath === 'string' && keyPath.indexOf('.') !== -1;
	                   var index = new IndexSpec(idbindex.name, keyPath, !!idbindex.unique, !!idbindex.multiEntry, false, keyPath && typeof keyPath !== 'string', dotted);
	                   indexes.push(index);
	               }
	               globalSchema[storeName] = new TableSchema(storeName, primKey, indexes, {});
	           });
	           setApiOnPlace([allTables, Transaction.prototype], keys(globalSchema), READWRITE, globalSchema);
	       }
	
	       function adjustToExistingIndexNames(schema, idbtrans) {
	           /// <summary>
	           /// Issue #30 Problem with existing db - adjust to existing index names when migrating from non-dexie db
	           /// </summary>
	           /// <param name="schema" type="Object">Map between name and TableSchema</param>
	           /// <param name="idbtrans" type="IDBTransaction"></param>
	           var storeNames = idbtrans.db.objectStoreNames;
	           for (var i = 0; i < storeNames.length; ++i) {
	               var storeName = storeNames[i];
	               var store = idbtrans.objectStore(storeName);
	               hasGetAll = 'getAll' in store;
	               for (var j = 0; j < store.indexNames.length; ++j) {
	                   var indexName = store.indexNames[j];
	                   var keyPath = store.index(indexName).keyPath;
	                   var dexieName = typeof keyPath === 'string' ? keyPath : "[" + slice(keyPath).join('+') + "]";
	                   if (schema[storeName]) {
	                       var indexSpec = schema[storeName].idxByName[dexieName];
	                       if (indexSpec) indexSpec.name = indexName;
	                   }
	               }
	           }
	       }
	
	       function fireOnBlocked(ev) {
	           db.on("blocked").fire(ev);
	           // Workaround (not fully*) for missing "versionchange" event in IE,Edge and Safari:
	           connections.filter(function (c) {
	               return c.name === db.name && c !== db && !c._vcFired;
	           }).map(function (c) {
	               return c.on("versionchange").fire(ev);
	           });
	       }
	
	       extend(this, {
	           Collection: Collection,
	           Table: Table,
	           Transaction: Transaction,
	           Version: Version,
	           WhereClause: WhereClause,
	           WriteableCollection: WriteableCollection,
	           WriteableTable: WriteableTable
	       });
	
	       init();
	
	       addons.forEach(function (fn) {
	           fn(db);
	       });
	   }
	
	   var fakeAutoComplete = function () {}; // Will never be changed. We just fake for the IDE that we change it (see doFakeAutoComplete())
	   var fake = false; // Will never be changed. We just fake for the IDE that we change it (see doFakeAutoComplete())
	
	   function parseType(type) {
	       if (typeof type === 'function') {
	           return new type();
	       } else if (isArray(type)) {
	           return [parseType(type[0])];
	       } else if (type && typeof type === 'object') {
	           var rv = {};
	           applyStructure(rv, type);
	           return rv;
	       } else {
	           return type;
	       }
	   }
	
	   function applyStructure(obj, structure) {
	       keys(structure).forEach(function (member) {
	           var value = parseType(structure[member]);
	           obj[member] = value;
	       });
	       return obj;
	   }
	
	   function eventSuccessHandler(done) {
	       return function (ev) {
	           done(ev.target.result);
	       };
	   }
	
	   function hookedEventSuccessHandler(resolve) {
	       // wrap() is needed when calling hooks because the rare scenario of:
	       //  * hook does a db operation that fails immediately (IDB throws exception)
	       //    For calling db operations on correct transaction, wrap makes sure to set PSD correctly.
	       //    wrap() will also execute in a virtual tick.
	       //  * If not wrapped in a virtual tick, direct exception will launch a new physical tick.
	       //  * If this was the last event in the bulk, the promise will resolve after a physical tick
	       //    and the transaction will have committed already.
	       // If no hook, the virtual tick will be executed in the reject()/resolve of the final promise,
	       // because it is always marked with _lib = true when created using Transaction._promise().
	       return wrap(function (event) {
	           var req = event.target,
	               result = req.result,
	               ctx = req._hookCtx,
	               // Contains the hook error handler. Put here instead of closure to boost performance.
	           hookSuccessHandler = ctx && ctx.onsuccess;
	           hookSuccessHandler && hookSuccessHandler(result);
	           resolve && resolve(result);
	       }, resolve);
	   }
	
	   function eventRejectHandler(reject) {
	       return function (event) {
	           preventDefault(event);
	           reject(event.target.error);
	           return false;
	       };
	   }
	
	   function hookedEventRejectHandler(reject) {
	       return wrap(function (event) {
	           // See comment on hookedEventSuccessHandler() why wrap() is needed only when supporting hooks.
	
	           var req = event.target,
	               err = req.error,
	               ctx = req._hookCtx,
	               // Contains the hook error handler. Put here instead of closure to boost performance.
	           hookErrorHandler = ctx && ctx.onerror;
	           hookErrorHandler && hookErrorHandler(err);
	           preventDefault(event);
	           reject(err);
	           return false;
	       });
	   }
	
	   function preventDefault(event) {
	       if (event.stopPropagation) // IndexedDBShim doesnt support this on Safari 8 and below.
	           event.stopPropagation();
	       if (event.preventDefault) // IndexedDBShim doesnt support this on Safari 8 and below.
	           event.preventDefault();
	   }
	
	   function globalDatabaseList(cb) {
	       var val,
	           localStorage = Dexie.dependencies.localStorage;
	       if (!localStorage) return cb([]); // Envs without localStorage support
	       try {
	           val = JSON.parse(localStorage.getItem('Dexie.DatabaseNames') || "[]");
	       } catch (e) {
	           val = [];
	       }
	       if (cb(val)) {
	           localStorage.setItem('Dexie.DatabaseNames', JSON.stringify(val));
	       }
	   }
	
	   function awaitIterator(iterator) {
	       var callNext = function (result) {
	           return iterator.next(result);
	       },
	           doThrow = function (error) {
	           return iterator.throw(error);
	       },
	           onSuccess = step(callNext),
	           onError = step(doThrow);
	
	       function step(getNext) {
	           return function (val) {
	               var next = getNext(val),
	                   value = next.value;
	
	               return next.done ? value : !value || typeof value.then !== 'function' ? isArray(value) ? Promise.all(value).then(onSuccess, onError) : onSuccess(value) : value.then(onSuccess, onError);
	           };
	       }
	
	       return step(callNext)();
	   }
	
	   //
	   // IndexSpec struct
	   //
	   function IndexSpec(name, keyPath, unique, multi, auto, compound, dotted) {
	       /// <param name="name" type="String"></param>
	       /// <param name="keyPath" type="String"></param>
	       /// <param name="unique" type="Boolean"></param>
	       /// <param name="multi" type="Boolean"></param>
	       /// <param name="auto" type="Boolean"></param>
	       /// <param name="compound" type="Boolean"></param>
	       /// <param name="dotted" type="Boolean"></param>
	       this.name = name;
	       this.keyPath = keyPath;
	       this.unique = unique;
	       this.multi = multi;
	       this.auto = auto;
	       this.compound = compound;
	       this.dotted = dotted;
	       var keyPathSrc = typeof keyPath === 'string' ? keyPath : keyPath && '[' + [].join.call(keyPath, '+') + ']';
	       this.src = (unique ? '&' : '') + (multi ? '*' : '') + (auto ? "++" : "") + keyPathSrc;
	   }
	
	   //
	   // TableSchema struct
	   //
	   function TableSchema(name, primKey, indexes, instanceTemplate) {
	       /// <param name="name" type="String"></param>
	       /// <param name="primKey" type="IndexSpec"></param>
	       /// <param name="indexes" type="Array" elementType="IndexSpec"></param>
	       /// <param name="instanceTemplate" type="Object"></param>
	       this.name = name;
	       this.primKey = primKey || new IndexSpec();
	       this.indexes = indexes || [new IndexSpec()];
	       this.instanceTemplate = instanceTemplate;
	       this.mappedClass = null;
	       this.idxByName = arrayToObject(indexes, function (index) {
	           return [index.name, index];
	       });
	   }
	
	   // Used in when defining dependencies later...
	   // (If IndexedDBShim is loaded, prefer it before standard indexedDB)
	   var idbshim = _global.idbModules && _global.idbModules.shimIndexedDB ? _global.idbModules : {};
	
	   function safariMultiStoreFix(storeNames) {
	       return storeNames.length === 1 ? storeNames[0] : storeNames;
	   }
	
	   function getNativeGetDatabaseNamesFn(indexedDB) {
	       var fn = indexedDB && (indexedDB.getDatabaseNames || indexedDB.webkitGetDatabaseNames);
	       return fn && fn.bind(indexedDB);
	   }
	
	   // Export Error classes
	   props(Dexie, fullNameExceptions); // Dexie.XXXError = class XXXError {...};
	
	   //
	   // Static methods and properties
	   //
	   props(Dexie, {
	
	       //
	       // Static delete() method.
	       //
	       delete: function (databaseName) {
	           var db = new Dexie(databaseName),
	               promise = db.delete();
	           promise.onblocked = function (fn) {
	               db.on("blocked", fn);
	               return this;
	           };
	           return promise;
	       },
	
	       //
	       // Static exists() method.
	       //
	       exists: function (name) {
	           return new Dexie(name).open().then(function (db) {
	               db.close();
	               return true;
	           }).catch(Dexie.NoSuchDatabaseError, function () {
	               return false;
	           });
	       },
	
	       //
	       // Static method for retrieving a list of all existing databases at current host.
	       //
	       getDatabaseNames: function (cb) {
	           return new Promise(function (resolve, reject) {
	               var getDatabaseNames = getNativeGetDatabaseNamesFn(indexedDB);
	               if (getDatabaseNames) {
	                   // In case getDatabaseNames() becomes standard, let's prepare to support it:
	                   var req = getDatabaseNames();
	                   req.onsuccess = function (event) {
	                       resolve(slice(event.target.result, 0)); // Converst DOMStringList to Array<String>
	                   };
	                   req.onerror = eventRejectHandler(reject);
	               } else {
	                   globalDatabaseList(function (val) {
	                       resolve(val);
	                       return false;
	                   });
	               }
	           }).then(cb);
	       },
	
	       defineClass: function (structure) {
	           /// <summary>
	           ///     Create a javascript constructor based on given template for which properties to expect in the class.
	           ///     Any property that is a constructor function will act as a type. So {name: String} will be equal to {name: new String()}.
	           /// </summary>
	           /// <param name="structure">Helps IDE code completion by knowing the members that objects contain and not just the indexes. Also
	           /// know what type each member has. Example: {name: String, emailAddresses: [String], properties: {shoeSize: Number}}</param>
	
	           // Default constructor able to copy given properties into this object.
	           function Class(properties) {
	               /// <param name="properties" type="Object" optional="true">Properties to initialize object with.
	               /// </param>
	               properties ? extend(this, properties) : fake && applyStructure(this, structure);
	           }
	           return Class;
	       },
	
	       applyStructure: applyStructure,
	
	       ignoreTransaction: function (scopeFunc) {
	           // In case caller is within a transaction but needs to create a separate transaction.
	           // Example of usage:
	           //
	           // Let's say we have a logger function in our app. Other application-logic should be unaware of the
	           // logger function and not need to include the 'logentries' table in all transaction it performs.
	           // The logging should always be done in a separate transaction and not be dependant on the current
	           // running transaction context. Then you could use Dexie.ignoreTransaction() to run code that starts a new transaction.
	           //
	           //     Dexie.ignoreTransaction(function() {
	           //         db.logentries.add(newLogEntry);
	           //     });
	           //
	           // Unless using Dexie.ignoreTransaction(), the above example would try to reuse the current transaction
	           // in current Promise-scope.
	           //
	           // An alternative to Dexie.ignoreTransaction() would be setImmediate() or setTimeout(). The reason we still provide an
	           // API for this because
	           //  1) The intention of writing the statement could be unclear if using setImmediate() or setTimeout().
	           //  2) setTimeout() would wait unnescessary until firing. This is however not the case with setImmediate().
	           //  3) setImmediate() is not supported in the ES standard.
	           //  4) You might want to keep other PSD state that was set in a parent PSD, such as PSD.letThrough.
	           return PSD.trans ? usePSD(PSD.transless, scopeFunc) : // Use the closest parent that was non-transactional.
	           scopeFunc(); // No need to change scope because there is no ongoing transaction.
	       },
	
	       vip: function (fn) {
	           // To be used by subscribers to the on('ready') event.
	           // This will let caller through to access DB even when it is blocked while the db.ready() subscribers are firing.
	           // This would have worked automatically if we were certain that the Provider was using Dexie.Promise for all asyncronic operations. The promise PSD
	           // from the provider.connect() call would then be derived all the way to when provider would call localDatabase.applyChanges(). But since
	           // the provider more likely is using non-promise async APIs or other thenable implementations, we cannot assume that.
	           // Note that this method is only useful for on('ready') subscribers that is returning a Promise from the event. If not using vip()
	           // the database could deadlock since it wont open until the returned Promise is resolved, and any non-VIPed operation started by
	           // the caller will not resolve until database is opened.
	           return newScope(function () {
	               PSD.letThrough = true; // Make sure we are let through if still blocking db due to onready is firing.
	               return fn();
	           });
	       },
	
	       async: function (generatorFn) {
	           return function () {
	               try {
	                   var rv = awaitIterator(generatorFn.apply(this, arguments));
	                   if (!rv || typeof rv.then !== 'function') return Promise.resolve(rv);
	                   return rv;
	               } catch (e) {
	                   return rejection(e);
	               }
	           };
	       },
	
	       spawn: function (generatorFn, args, thiz) {
	           try {
	               var rv = awaitIterator(generatorFn.apply(thiz, args || []));
	               if (!rv || typeof rv.then !== 'function') return Promise.resolve(rv);
	               return rv;
	           } catch (e) {
	               return rejection(e);
	           }
	       },
	
	       // Dexie.currentTransaction property
	       currentTransaction: {
	           get: function () {
	               return PSD.trans || null;
	           }
	       },
	
	       // Export our Promise implementation since it can be handy as a standalone Promise implementation
	       Promise: Promise,
	
	       // Dexie.debug proptery:
	       // Dexie.debug = false
	       // Dexie.debug = true
	       // Dexie.debug = "dexie" - don't hide dexie's stack frames.
	       debug: {
	           get: function () {
	               return debug;
	           },
	           set: function (value) {
	               setDebug(value, value === 'dexie' ? function () {
	                   return true;
	               } : dexieStackFrameFilter);
	           }
	       },
	
	       // Export our derive/extend/override methodology
	       derive: derive,
	       extend: extend,
	       props: props,
	       override: override,
	       // Export our Events() function - can be handy as a toolkit
	       Events: Events,
	       events: Events, // Backward compatible lowercase version. Deprecate.
	       // Utilities
	       getByKeyPath: getByKeyPath,
	       setByKeyPath: setByKeyPath,
	       delByKeyPath: delByKeyPath,
	       shallowClone: shallowClone,
	       deepClone: deepClone,
	       getObjectDiff: getObjectDiff,
	       asap: asap,
	       maxKey: maxKey,
	       // Addon registry
	       addons: [],
	       // Global DB connection list
	       connections: connections,
	
	       MultiModifyError: exceptions.Modify, // Backward compatibility 0.9.8. Deprecate.
	       errnames: errnames,
	
	       // Export other static classes
	       IndexSpec: IndexSpec,
	       TableSchema: TableSchema,
	
	       //
	       // Dependencies
	       //
	       // These will automatically work in browsers with indexedDB support, or where an indexedDB polyfill has been included.
	       //
	       // In node.js, however, these properties must be set "manually" before instansiating a new Dexie().
	       // For node.js, you need to require indexeddb-js or similar and then set these deps.
	       //
	       dependencies: {
	           // Required:
	           indexedDB: idbshim.shimIndexedDB || _global.indexedDB || _global.mozIndexedDB || _global.webkitIndexedDB || _global.msIndexedDB,
	           IDBKeyRange: idbshim.IDBKeyRange || _global.IDBKeyRange || _global.webkitIDBKeyRange
	       },
	
	       // API Version Number: Type Number, make sure to always set a version number that can be comparable correctly. Example: 0.9, 0.91, 0.92, 1.0, 1.01, 1.1, 1.2, 1.21, etc.
	       semVer: DEXIE_VERSION,
	       version: DEXIE_VERSION.split('.').map(function (n) {
	           return parseInt(n);
	       }).reduce(function (p, c, i) {
	           return p + c / Math.pow(10, i * 2);
	       }),
	       fakeAutoComplete: fakeAutoComplete,
	
	       // https://github.com/dfahlander/Dexie.js/issues/186
	       // typescript compiler tsc in mode ts-->es5 & commonJS, will expect require() to return
	       // x.default. Workaround: Set Dexie.default = Dexie.
	       default: Dexie
	   });
	
	   tryCatch(function () {
	       // Optional dependencies
	       // localStorage
	       Dexie.dependencies.localStorage = (typeof chrome !== "undefined" && chrome !== null ? chrome.storage : void 0) != null ? null : _global.localStorage;
	   });
	
	   // Map DOMErrors and DOMExceptions to corresponding Dexie errors. May change in Dexie v2.0.
	   Promise.rejectionMapper = mapError;
	
	   // Fool IDE to improve autocomplete. Tested with Visual Studio 2013 and 2015.
	   doFakeAutoComplete(function () {
	       Dexie.fakeAutoComplete = fakeAutoComplete = doFakeAutoComplete;
	       Dexie.fake = fake = true;
	   });
	
	   return Dexie;
	
	}));
	//# sourceMappingURL=dexie.js.map
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }()), __webpack_require__(7).setImmediate))

/***/ },
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(setImmediate, clearImmediate) {var nextTick = __webpack_require__(8).nextTick;
	var apply = Function.prototype.apply;
	var slice = Array.prototype.slice;
	var immediateIds = {};
	var nextImmediateId = 0;
	
	// DOM APIs, for completeness
	
	exports.setTimeout = function() {
	  return new Timeout(apply.call(setTimeout, window, arguments), clearTimeout);
	};
	exports.setInterval = function() {
	  return new Timeout(apply.call(setInterval, window, arguments), clearInterval);
	};
	exports.clearTimeout =
	exports.clearInterval = function(timeout) { timeout.close(); };
	
	function Timeout(id, clearFn) {
	  this._id = id;
	  this._clearFn = clearFn;
	}
	Timeout.prototype.unref = Timeout.prototype.ref = function() {};
	Timeout.prototype.close = function() {
	  this._clearFn.call(window, this._id);
	};
	
	// Does not start the time, just sets up the members needed.
	exports.enroll = function(item, msecs) {
	  clearTimeout(item._idleTimeoutId);
	  item._idleTimeout = msecs;
	};
	
	exports.unenroll = function(item) {
	  clearTimeout(item._idleTimeoutId);
	  item._idleTimeout = -1;
	};
	
	exports._unrefActive = exports.active = function(item) {
	  clearTimeout(item._idleTimeoutId);
	
	  var msecs = item._idleTimeout;
	  if (msecs >= 0) {
	    item._idleTimeoutId = setTimeout(function onTimeout() {
	      if (item._onTimeout)
	        item._onTimeout();
	    }, msecs);
	  }
	};
	
	// That's not how node.js implements it but the exposed api is the same.
	exports.setImmediate = typeof setImmediate === "function" ? setImmediate : function(fn) {
	  var id = nextImmediateId++;
	  var args = arguments.length < 2 ? false : slice.call(arguments, 1);
	
	  immediateIds[id] = true;
	
	  nextTick(function onNextTick() {
	    if (immediateIds[id]) {
	      // fn.call() is faster so we optimize for the common use-case
	      // @see http://jsperf.com/call-apply-segu
	      if (args) {
	        fn.apply(null, args);
	      } else {
	        fn.call(null);
	      }
	      // Prevent ids from leaking
	      exports.clearImmediate(id);
	    }
	  });
	
	  return id;
	};
	
	exports.clearImmediate = typeof clearImmediate === "function" ? clearImmediate : function(id) {
	  delete immediateIds[id];
	};
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(7).setImmediate, __webpack_require__(7).clearImmediate))

/***/ },
/* 8 */
/***/ function(module, exports) {

	// shim for using process in browser
	
	var process = module.exports = {};
	
	// cached from whatever global is present so that test runners that stub it
	// don't break things.  But we need to wrap it in a try catch in case it is
	// wrapped in strict mode code which doesn't define any globals.  It's inside a
	// function because try/catches deoptimize in certain engines.
	
	var cachedSetTimeout;
	var cachedClearTimeout;
	
	(function () {
	  try {
	    cachedSetTimeout = setTimeout;
	  } catch (e) {
	    cachedSetTimeout = function () {
	      throw new Error('setTimeout is not defined');
	    }
	  }
	  try {
	    cachedClearTimeout = clearTimeout;
	  } catch (e) {
	    cachedClearTimeout = function () {
	      throw new Error('clearTimeout is not defined');
	    }
	  }
	} ())
	var queue = [];
	var draining = false;
	var currentQueue;
	var queueIndex = -1;
	
	function cleanUpNextTick() {
	    if (!draining || !currentQueue) {
	        return;
	    }
	    draining = false;
	    if (currentQueue.length) {
	        queue = currentQueue.concat(queue);
	    } else {
	        queueIndex = -1;
	    }
	    if (queue.length) {
	        drainQueue();
	    }
	}
	
	function drainQueue() {
	    if (draining) {
	        return;
	    }
	    var timeout = cachedSetTimeout(cleanUpNextTick);
	    draining = true;
	
	    var len = queue.length;
	    while(len) {
	        currentQueue = queue;
	        queue = [];
	        while (++queueIndex < len) {
	            if (currentQueue) {
	                currentQueue[queueIndex].run();
	            }
	        }
	        queueIndex = -1;
	        len = queue.length;
	    }
	    currentQueue = null;
	    draining = false;
	    cachedClearTimeout(timeout);
	}
	
	process.nextTick = function (fun) {
	    var args = new Array(arguments.length - 1);
	    if (arguments.length > 1) {
	        for (var i = 1; i < arguments.length; i++) {
	            args[i - 1] = arguments[i];
	        }
	    }
	    queue.push(new Item(fun, args));
	    if (queue.length === 1 && !draining) {
	        cachedSetTimeout(drainQueue, 0);
	    }
	};
	
	// v8 likes predictible objects
	function Item(fun, array) {
	    this.fun = fun;
	    this.array = array;
	}
	Item.prototype.run = function () {
	    this.fun.apply(null, this.array);
	};
	process.title = 'browser';
	process.browser = true;
	process.env = {};
	process.argv = [];
	process.version = ''; // empty string to avoid regexp issues
	process.versions = {};
	
	function noop() {}
	
	process.on = noop;
	process.addListener = noop;
	process.once = noop;
	process.off = noop;
	process.removeListener = noop;
	process.removeAllListeners = noop;
	process.emit = noop;
	
	process.binding = function (name) {
	    throw new Error('process.binding is not supported');
	};
	
	process.cwd = function () { return '/' };
	process.chdir = function (dir) {
	    throw new Error('process.chdir is not supported');
	};
	process.umask = function() { return 0; };


/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*
	 * Anime v1.1.0
	 * http://anime-js.com
	 * JavaScript animation engine
	 * Copyright (c) 2016 Julian Garnier
	 * http://juliangarnier.com
	 * Released under the MIT license
	 */
	
	(function (root, factory) {
	  if (true) {
	    // AMD. Register as an anonymous module.
	    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory), __WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ? (__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	  } else if (typeof module === 'object' && module.exports) {
	    // Node. Does not work with strict CommonJS, but
	    // only CommonJS-like environments that support module.exports,
	    // like Node.
	    module.exports = factory();
	  } else {
	    // Browser globals (root is window)
	    root.anime = factory();
	  }
	}(this, function () {
	
	  var version = '1.1.0';
	
	  // Defaults
	
	  var defaultSettings = {
	    duration: 1000,
	    delay: 0,
	    loop: false,
	    autoplay: true,
	    direction: 'normal',
	    easing: 'easeOutElastic',
	    elasticity: 400,
	    round: false,
	    begin: undefined,
	    update: undefined,
	    complete: undefined
	  }
	
	  // Transforms
	
	  var validTransforms = ['translateX', 'translateY', 'translateZ', 'rotate', 'rotateX', 'rotateY', 'rotateZ', 'scale', 'scaleX', 'scaleY', 'scaleZ', 'skewX', 'skewY'];
	  var transform, transformStr = 'transform';
	
	  // Utils
	
	  var is = (function() {
	    return {
	      array:  function(a) { return Array.isArray(a) },
	      object: function(a) { return Object.prototype.toString.call(a).indexOf('Object') > -1 },
	      svg:    function(a) { return a instanceof SVGElement },
	      dom:    function(a) { return a.nodeType || is.svg(a) },
	      number: function(a) { return !isNaN(parseInt(a)) },
	      string: function(a) { return typeof a === 'string' },
	      func:   function(a) { return typeof a === 'function' },
	      undef:  function(a) { return typeof a === 'undefined' },
	      null:   function(a) { return typeof a === 'null' },
	      hex:    function(a) { return /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(a) },
	      rgb:    function(a) { return /^rgb/.test(a) },
	      rgba:   function(a) { return /^rgba/.test(a) },
	      hsl:    function(a) { return /^hsl/.test(a) },
	      color:  function(a) { return (is.hex(a) || is.rgb(a) || is.rgba(a) || is.hsl(a))}
	    }
	  })();
	
	  // Easings functions adapted from http://jqueryui.com/
	
	  var easings = (function() {
	    var eases = {};
	    var names = ['Quad', 'Cubic', 'Quart', 'Quint', 'Expo'];
	    var functions = {
	      Sine: function(t) { return 1 - Math.cos( t * Math.PI / 2 ); },
	      Circ: function(t) { return 1 - Math.sqrt( 1 - t * t ); },
	      Elastic: function(t, m) {
	        if( t === 0 || t === 1 ) return t;
	        var p = (1 - Math.min(m, 998) / 1000), st = t / 1, st1 = st - 1, s = p / ( 2 * Math.PI ) * Math.asin( 1 );
	        return -( Math.pow( 2, 10 * st1 ) * Math.sin( ( st1 - s ) * ( 2 * Math.PI ) / p ) );
	      },
	      Back: function(t) { return t * t * ( 3 * t - 2 ); },
	      Bounce: function(t) {
	        var pow2, bounce = 4;
	        while ( t < ( ( pow2 = Math.pow( 2, --bounce ) ) - 1 ) / 11 ) {}
	        return 1 / Math.pow( 4, 3 - bounce ) - 7.5625 * Math.pow( ( pow2 * 3 - 2 ) / 22 - t, 2 );
	      }
	    }
	    names.forEach(function(name, i) {
	      functions[name] = function(t) {
	        return Math.pow( t, i + 2 );
	      }
	    });
	    Object.keys(functions).forEach(function(name) {
	      var easeIn = functions[name];
	      eases['easeIn' + name] = easeIn;
	      eases['easeOut' + name] = function(t, m) { return 1 - easeIn(1 - t, m); };
	      eases['easeInOut' + name] = function(t, m) { return t < 0.5 ? easeIn(t * 2, m) / 2 : 1 - easeIn(t * -2 + 2, m) / 2; };
	    });
	    eases.linear = function(t) { return t; };
	    return eases;
	  })();
	
	  // Strings
	
	  var numberToString = function(val) {
	    return (is.string(val)) ? val : val + '';
	  }
	
	  var stringToHyphens = function(str) {
	    return str.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
	  }
	
	  var selectString = function(str) {
	    if (is.color(str)) return false;
	    try {
	      var nodes = document.querySelectorAll(str);
	      return nodes;
	    } catch(e) {
	      return false;
	    }
	  }
	
	  // Numbers
	
	  var random = function(min, max) {
	    return Math.floor(Math.random() * (max - min + 1)) + min;
	  }
	
	  // Arrays
	
	  var flattenArray = function(arr) {
	    return arr.reduce(function(a, b) {
	      return a.concat(is.array(b) ? flattenArray(b) : b);
	    }, []);
	  }
	
	  var toArray = function(o) {
	    if (is.array(o)) return o;
	    if (is.string(o)) o = selectString(o) || o;
	    if (o instanceof NodeList || o instanceof HTMLCollection) return [].slice.call(o);
	    return [o];
	  }
	
	  var arrayContains = function(arr, val) {
	    return arr.some(function(a) { return a === val; });
	  }
	
	  var groupArrayByProps = function(arr, propsArr) {
	    var groups = {};
	    arr.forEach(function(o) {
	      var group = JSON.stringify(propsArr.map(function(p) { return o[p]; }));
	      groups[group] = groups[group] || [];
	      groups[group].push(o);
	    });
	    return Object.keys(groups).map(function(group) {
	      return groups[group];
	    });
	  }
	
	  var removeArrayDuplicates = function(arr) {
	    return arr.filter(function(item, pos, self) {
	      return self.indexOf(item) === pos;
	    });
	  }
	
	  // Objects
	
	  var cloneObject = function(o) {
	    var newObject = {};
	    for (var p in o) newObject[p] = o[p];
	    return newObject;
	  }
	
	  var mergeObjects = function(o1, o2) {
	    for (var p in o2) o1[p] = !is.undef(o1[p]) ? o1[p] : o2[p];
	    return o1;
	  }
	
	  // Colors
	
	  var hexToRgb = function(hex) {
	    var rgx = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
	    var hex = hex.replace(rgx, function(m, r, g, b) { return r + r + g + g + b + b; });
	    var rgb = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
	    var r = parseInt(rgb[1], 16);
	    var g = parseInt(rgb[2], 16);
	    var b = parseInt(rgb[3], 16);
	    return 'rgb(' + r + ',' + g + ',' + b + ')';
	  }
	
	  var hslToRgb = function(hsl) {
	    var hsl = /hsl\((\d+),\s*([\d.]+)%,\s*([\d.]+)%\)/g.exec(hsl);
	    var h = parseInt(hsl[1]) / 360;
	    var s = parseInt(hsl[2]) / 100;
	    var l = parseInt(hsl[3]) / 100;
	    var hue2rgb = function(p, q, t) {
	      if (t < 0) t += 1;
	      if (t > 1) t -= 1;
	      if (t < 1/6) return p + (q - p) * 6 * t;
	      if (t < 1/2) return q;
	      if (t < 2/3) return p + (q - p) * (2/3 - t) * 6;
	      return p;
	    }
	    var r, g, b;
	    if (s == 0) {
	      r = g = b = l;
	    } else {
	      var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
	      var p = 2 * l - q;
	      r = hue2rgb(p, q, h + 1/3);
	      g = hue2rgb(p, q, h);
	      b = hue2rgb(p, q, h - 1/3);
	    }
	    return 'rgb(' + r * 255 + ',' + g * 255 + ',' + b * 255 + ')';
	  }
	
	  var colorToRgb = function(val) {
	    if (is.rgb(val) || is.rgba(val)) return val;
	    if (is.hex(val)) return hexToRgb(val);
	    if (is.hsl(val)) return hslToRgb(val);
	  }
	
	  // Units
	
	  var getUnit = function(val) {
	    return /([\+\-]?[0-9|auto\.]+)(%|px|pt|em|rem|in|cm|mm|ex|pc|vw|vh|deg)?/.exec(val)[2];
	  }
	
	  var addDefaultTransformUnit = function(prop, val, intialVal) {
	    if (getUnit(val)) return val;
	    if (prop.indexOf('translate') > -1) return getUnit(intialVal) ? val + getUnit(intialVal) : val + 'px';
	    if (prop.indexOf('rotate') > -1 || prop.indexOf('skew') > -1) return val + 'deg';
	    return val;
	  }
	
	  // Values
	
	  var getCSSValue = function(el, prop) {
	    // First check if prop is a valid CSS property
	    if (prop in el.style) {
	      // Then return the property value or fallback to '0' when getPropertyValue fails
	      return getComputedStyle(el).getPropertyValue(stringToHyphens(prop)) || '0';
	    }
	  }
	
	  var getTransformValue = function(el, prop) {
	    var defaultVal = prop.indexOf('scale') > -1 ? 1 : 0;
	    var str = el.style.transform;
	    if (!str) return defaultVal;
	    var rgx = /(\w+)\((.+?)\)/g;
	    var match = [];
	    var props = [];
	    var values = [];
	    while (match = rgx.exec(str)) {
	      props.push(match[1]);
	      values.push(match[2]);
	    }
	    var val = values.filter(function(f, i) { return props[i] === prop; });
	    return val.length ? val[0] : defaultVal;
	  }
	
	  var getAnimationType = function(el, prop) {
	    if ( is.dom(el) && arrayContains(validTransforms, prop)) return 'transform';
	    if ( is.dom(el) && (prop !== 'transform' && getCSSValue(el, prop))) return 'css';
	    if ( is.dom(el) && (el.getAttribute(prop) || (is.svg(el) && el[prop]))) return 'attribute';
	    if (!is.null(el[prop]) && !is.undef(el[prop])) return 'object';
	  }
	
	  var getInitialTargetValue = function(target, prop) {
	    switch (getAnimationType(target, prop)) {
	      case 'transform': return getTransformValue(target, prop);
	      case 'css': return getCSSValue(target, prop);
	      case 'attribute': return target.getAttribute(prop);
	    }
	    return target[prop] || 0;
	  }
	
	  var getValidValue = function(values, val, originalCSS) {
	    if (is.color(val)) return colorToRgb(val);
	    if (getUnit(val)) return val;
	    var unit = getUnit(values.to) ? getUnit(values.to) : getUnit(values.from);
	    if (!unit && originalCSS) unit = getUnit(originalCSS);
	    return unit ? val + unit : val;
	  }
	
	  var decomposeValue = function(val) {
	    var rgx = /-?\d*\.?\d+/g;
	    return {
	      original: val,
	      numbers: numberToString(val).match(rgx) ? numberToString(val).match(rgx).map(Number) : [0],
	      strings: numberToString(val).split(rgx)
	    }
	  }
	
	  var recomposeValue = function(numbers, strings, initialStrings) {
	    return strings.reduce(function(a, b, i) {
	      var b = (b ? b : initialStrings[i - 1]);
	      return a + numbers[i - 1] + b;
	    });
	  }
	
	  // Animatables
	
	  var getAnimatables = function(targets) {
	    var targets = targets ? (flattenArray(is.array(targets) ? targets.map(toArray) : toArray(targets))) : [];
	    return targets.map(function(t, i) {
	      return { target: t, id: i };
	    });
	  }
	
	  // Properties
	
	  var getProperties = function(params, settings) {
	    var props = [];
	    for (var p in params) {
	      if (!defaultSettings.hasOwnProperty(p) && p !== 'targets') {
	        var prop = is.object(params[p]) ? cloneObject(params[p]) : {value: params[p]};
	        prop.name = p;
	        props.push(mergeObjects(prop, settings));
	      }
	    }
	    return props;
	  }
	
	  var getPropertiesValues = function(target, prop, value, i) {
	    var values = toArray( is.func(value) ? value(target, i) : value);
	    return {
	      from: (values.length > 1) ? values[0] : getInitialTargetValue(target, prop),
	      to: (values.length > 1) ? values[1] : values[0]
	    }
	  }
	
	  // Tweens
	
	  var getTweenValues = function(prop, values, type, target) {
	    var valid = {};
	    if (type === 'transform') {
	      valid.from = prop + '(' + addDefaultTransformUnit(prop, values.from, values.to) + ')';
	      valid.to = prop + '(' + addDefaultTransformUnit(prop, values.to) + ')';
	    } else {
	      var originalCSS = (type === 'css') ? getCSSValue(target, prop) : undefined;
	      valid.from = getValidValue(values, values.from, originalCSS);
	      valid.to = getValidValue(values, values.to, originalCSS);
	    }
	    return { from: decomposeValue(valid.from), to: decomposeValue(valid.to) };
	  }
	
	  var getTweensProps = function(animatables, props) {
	    var tweensProps = [];
	    animatables.forEach(function(animatable, i) {
	      var target = animatable.target;
	      return props.forEach(function(prop) {
	        var animType = getAnimationType(target, prop.name);
	        if (animType) {
	          var values = getPropertiesValues(target, prop.name, prop.value, i);
	          var tween = cloneObject(prop);
	          tween.animatables = animatable;
	          tween.type = animType;
	          tween.from = getTweenValues(prop.name, values, tween.type, target).from;
	          tween.to = getTweenValues(prop.name, values, tween.type, target).to;
	          tween.round = (is.color(values.from) || tween.round) ? 1 : 0;
	          tween.delay = (is.func(tween.delay) ? tween.delay(target, i, animatables.length) : tween.delay) / animation.speed;
	          tween.duration = (is.func(tween.duration) ? tween.duration(target, i, animatables.length) : tween.duration) / animation.speed;
	          tweensProps.push(tween);
	        }
	      });
	    });
	    return tweensProps;
	  }
	
	  var getTweens = function(animatables, props) {
	    var tweensProps = getTweensProps(animatables, props);
	    var splittedProps = groupArrayByProps(tweensProps, ['name', 'from', 'to', 'delay', 'duration']);
	    return splittedProps.map(function(tweenProps) {
	      var tween = cloneObject(tweenProps[0]);
	      tween.animatables = tweenProps.map(function(p) { return p.animatables });
	      tween.totalDuration = tween.delay + tween.duration;
	      return tween;
	    });
	  }
	
	  var reverseTweens = function(anim, delays) {
	    anim.tweens.forEach(function(tween) {
	      var toVal = tween.to;
	      var fromVal = tween.from;
	      var delayVal = anim.duration - (tween.delay + tween.duration);
	      tween.from = toVal;
	      tween.to = fromVal;
	      if (delays) tween.delay = delayVal;
	    });
	    anim.reversed = anim.reversed ? false : true;
	  }
	
	  var getTweensDuration = function(tweens) {
	    if (tweens.length) return Math.max.apply(Math, tweens.map(function(tween){ return tween.totalDuration; }));
	  }
	
	  // will-change
	
	  var getWillChange = function(anim) {
	    var props = [];
	    var els = [];
	    anim.tweens.forEach(function(tween) {
	      if (tween.type === 'css' || tween.type === 'transform' ) {
	        props.push(tween.type === 'css' ? stringToHyphens(tween.name) : 'transform');
	        tween.animatables.forEach(function(animatable) { els.push(animatable.target); });
	      }
	    });
	    return {
	      properties: removeArrayDuplicates(props).join(', '),
	      elements: removeArrayDuplicates(els)
	    }
	  }
	
	  var setWillChange = function(anim) {
	    var willChange = getWillChange(anim);
	    willChange.elements.forEach(function(element) {
	      element.style.willChange = willChange.properties;
	    });
	  }
	
	  var removeWillChange = function(anim) {
	    var willChange = getWillChange(anim);
	    willChange.elements.forEach(function(element) {
	      element.style.removeProperty('will-change');
	    });
	  }
	
	  /* Svg path */
	
	  var getPathProps = function(path) {
	    var el = is.string(path) ? selectString(path)[0] : path;
	    return {
	      path: el,
	      value: el.getTotalLength()
	    }
	  }
	
	  var snapProgressToPath = function(tween, progress) {
	    var pathEl = tween.path;
	    var pathProgress = tween.value * progress;
	    var point = function(offset) {
	      var o = offset || 0;
	      var p = progress > 1 ? tween.value + o : pathProgress + o;
	      return pathEl.getPointAtLength(p);
	    }
	    var p = point();
	    var p0 = point(-1);
	    var p1 = point(+1);
	    switch (tween.name) {
	      case 'translateX': return p.x;
	      case 'translateY': return p.y;
	      case 'rotate': return Math.atan2(p1.y - p0.y, p1.x - p0.x) * 180 / Math.PI;
	    }
	  }
	
	  // Progress
	
	  var getTweenProgress = function(tween, time) {
	    var elapsed = Math.min(Math.max(time - tween.delay, 0), tween.duration);
	    var percent = elapsed / tween.duration;
	    var progress = tween.to.numbers.map(function(number, p) {
	      var start = tween.from.numbers[p];
	      var eased = easings[tween.easing](percent, tween.elasticity);
	      var val = tween.path ? snapProgressToPath(tween, eased) : start + eased * (number - start);
	      val = tween.round ? Math.round(val * tween.round) / tween.round : val;
	      return val;
	    });
	    return recomposeValue(progress, tween.to.strings, tween.from.strings);
	  }
	
	  var setAnimationProgress = function(anim, time) {
	    var transforms;
	    anim.currentTime = time;
	    anim.progress = (time / anim.duration) * 100;
	    for (var t = 0; t < anim.tweens.length; t++) {
	      var tween = anim.tweens[t];
	      tween.currentValue = getTweenProgress(tween, time);
	      var progress = tween.currentValue;
	      for (var a = 0; a < tween.animatables.length; a++) {
	        var animatable = tween.animatables[a];
	        var id = animatable.id;
	        var target = animatable.target;
	        var name = tween.name;
	        switch (tween.type) {
	          case 'css': target.style[name] = progress; break;
	          case 'attribute': target.setAttribute(name, progress); break;
	          case 'object': target[name] = progress; break;
	          case 'transform':
	          if (!transforms) transforms = {};
	          if (!transforms[id]) transforms[id] = [];
	          transforms[id].push(progress);
	          break;
	        }
	      }
	    }
	    if (transforms) {
	      if (!transform) transform = (getCSSValue(document.body, transformStr) ? '' : '-webkit-') + transformStr;
	      for (var t in transforms) {
	        anim.animatables[t].target.style[transform] = transforms[t].join(' ');
	      }
	    }
	    if (anim.settings.update) anim.settings.update(anim);
	  }
	
	  // Animation
	
	  var createAnimation = function(params) {
	    var anim = {};
	    anim.animatables = getAnimatables(params.targets);
	    anim.settings = mergeObjects(params, defaultSettings);
	    anim.properties = getProperties(params, anim.settings);
	    anim.tweens = getTweens(anim.animatables, anim.properties);
	    anim.duration = getTweensDuration(anim.tweens) || params.duration;
	    anim.currentTime = 0;
	    anim.progress = 0;
	    anim.ended = false;
	    return anim;
	  }
	
	  // Public
	
	  var animations = [];
	  var raf = 0;
	
	  var engine = (function() {
	    var play = function() { raf = requestAnimationFrame(step); };
	    var step = function(t) {
	      if (animations.length) {
	        for (var i = 0; i < animations.length; i++) animations[i].tick(t);
	        play();
	      } else {
	        cancelAnimationFrame(raf);
	        raf = 0;
	      }
	    }
	    return play;
	  })();
	
	  var animation = function(params) {
	
	    var anim = createAnimation(params);
	    var time = {};
	
	    anim.tick = function(now) {
	      anim.ended = false;
	      if (!time.start) time.start = now;
	      time.current = Math.min(Math.max(time.last + now - time.start, 0), anim.duration);
	      setAnimationProgress(anim, time.current);
	      var s = anim.settings;
	      if (s.begin && time.current >= s.delay) { s.begin(anim); s.begin = undefined; };
	      if (time.current >= anim.duration) {
	        if (s.loop) {
	          time.start = now;
	          if (s.direction === 'alternate') reverseTweens(anim, true);
	          if (is.number(s.loop)) s.loop--;
	        } else {
	          anim.ended = true;
	          anim.pause();
	          if (s.complete) s.complete(anim);
	        }
	        time.last = 0;
	      }
	    }
	
	    anim.seek = function(progress) {
	      setAnimationProgress(anim, (progress / 100) * anim.duration);
	    }
	
	    anim.pause = function() {
	      removeWillChange(anim);
	      var i = animations.indexOf(anim);
	      if (i > -1) animations.splice(i, 1);
	    }
	
	    anim.play = function(params) {
	      anim.pause();
	      if (params) anim = mergeObjects(createAnimation(mergeObjects(params, anim.settings)), anim);
	      time.start = 0;
	      time.last = anim.ended ? 0 : anim.currentTime;
	      var s = anim.settings;
	      if (s.direction === 'reverse') reverseTweens(anim);
	      if (s.direction === 'alternate' && !s.loop) s.loop = 1;
	      setWillChange(anim);
	      animations.push(anim);
	      if (!raf) engine();
	    }
	
	    anim.restart = function() {
	      if (anim.reversed) reverseTweens(anim);
	      anim.pause();
	      anim.seek(0);
	      anim.play();
	    }
	
	    if (anim.settings.autoplay) anim.play();
	
	    return anim;
	
	  }
	
	  // Remove one or multiple targets from all active animations.
	
	  var remove = function(elements) {
	    var targets = flattenArray(is.array(elements) ? elements.map(toArray) : toArray(elements));
	    for (var i = animations.length-1; i >= 0; i--) {
	      var animation = animations[i];
	      var tweens = animation.tweens;
	      for (var t = tweens.length-1; t >= 0; t--) {
	        var animatables = tweens[t].animatables;
	        for (var a = animatables.length-1; a >= 0; a--) {
	          if (arrayContains(targets, animatables[a].target)) {
	            animatables.splice(a, 1);
	            if (!animatables.length) tweens.splice(t, 1);
	            if (!tweens.length) animation.pause();
	          }
	        }
	      }
	    }
	  }
	
	  animation.version = version;
	  animation.speed = 1;
	  animation.list = animations;
	  animation.remove = remove;
	  animation.easings = easings;
	  animation.getValue = getInitialTargetValue;
	  animation.path = getPathProps;
	  animation.random = random;
	
	  return animation;
	
	}));


/***/ }
/******/ ]);
//# sourceMappingURL=main.js.map