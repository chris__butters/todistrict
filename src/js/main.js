import Monkberry from 'monkberry';
import 'monkberry-events';
import Template from './templates/todo.monk';
import Dexie from 'dexie';
import anime from 'animejs';

// TODO: Quit
// TODO: random messages for task

var db = new Dexie('data');
db.version(1).stores({
  todo:'++id, text, complete'
});
db.open();

var state = {
  todos: []
};

let view = Monkberry.render(Template, document.body);
view.update(state);

db.todo.toArray().then(function(response){
  state.todos = response;
  view.update(state);  
});


view.on('submit', 'form', function (event) {
  event.preventDefault();
  let input = view.querySelector('input[type="text"]');
  if (input.value == ''){
    alert('Cannot add a blank item');
    return;
  }
  db.todo.put({text: input.value, complete: false}).then(function(res){
    return db.todo.toArray();
  }).then(function(db){
    state.todos = db;
    view.update(state);
    input.value = '';
  });
});

view.on('click', 'input[type="checkbox"]', function (event) {
  let i = parseInt(event.target.dataset.index);
  db.todo.where('id').equals(i).modify(function(item){
    item.complete = !item.complete;
  }).then(function(response){
    db.todo.toArray().then(function(response){
      return response;
    }).then(function(response){
      state.todos = response;
      view.update(state);
    })
  });
});

view.on('click', '.delete' ,function(event) {
  event.preventDefault();
  let i =  parseInt(event.target.parentNode.dataset.index);
  
  db.todo.where('id').equals(i).delete().then(function(response){
    db.todo.toArray().then(function(response){
      return response;
    }).then(function(response){
      state.todos = response;
      view.update(state);
    })
  });
});

view.on('click', '.delete *' ,function(event) {
  event.stopPropogation();
});

view.on('mouseover', '.label', function(event){;
  let el = this;
  let true_width = (function(){
    // jQuery code to rewrite as vanilla
    let tempobj = el.cloneNode(true);
    tempobj.style.left = '-1000px';
    tempobj.style.position = 'absolute';
    document.body.appendChild(tempobj);
    let result = tempobj.offsetWidth;
    tempobj.parentNode.removeChild(tempobj);

    return result;
  })();

  if (true_width > el.offsetWidth){
    let shift_distance = true_width - el.offsetWidth; // how far to move
    anime({
      targets: el.querySelector('*'),
      left: shift_distance * -1 + 'px',
      duration: 1500,
      loop: false,
      easing: 'linear'
    })
  }
});
view.on('mouseout', '.label', function(event){
  let el = this;
  let anim = anime({
    targets: el.querySelector('*'),
    left: '0px',
    duration: 1500,
    loop: false,
    easing: 'linear'
  });
});


document.querySelector('.quit button').addEventListener('click',function(){
  window.close();
})


