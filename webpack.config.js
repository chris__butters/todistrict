const webpack = require('webpack');
const path = require('path');

const BUILD_DIR = path.resolve(__dirname, 'app/scripts');
const APP_DIR = path.resolve(__dirname, 'src/js');

let config = {
  entry: APP_DIR + '/main.js',
  devtool: 'source-map',
  output: {
    path: BUILD_DIR,
    filename: 'main.js'
  },
  module : {
    loaders : [
      {
        test : /\.js?/,
        include : APP_DIR,
        exclude: /(node_modules|bower_components)/,
        loader : 'babel'
      },
      {
        test: /\.monk$/, 
        loader: 'monkberry-loader'
      }
    ]
  }
};

module.exports = config;